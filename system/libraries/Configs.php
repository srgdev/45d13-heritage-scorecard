<?php

/*
 * @author : Paul Radich
 * 
 * Added to system core for the SRG CMS
 * 
 * 
 */
 
class Configs {
	
	var $CI;
	var $badUrl;
	var $goodUrl;
	
	function Configs(){
		$this->CI =& get_instance();
	}
	
	function set($key,$val){
		$query = $this->CI->db->get_where('CMS_config', array('key' => $key));
		$num = $query->num_rows(); 
		if($num != 0){
			$row = $query->row();
			$id = $row->id;
			$data = array(
               'key' => $key,
               'value' => $val
            );
			$this->CI->db->where('id', $id);
			$this->CI->db->update('CMS_config', $data);
		}else{
			$data = array(
			   'key' => $key,
               'value' => $val
			);
			$this->CI->db->insert('CMS_config', $data); 
		}
	}
	
	function get($key){
		// if($this->CI->session->userdata($key)){
			// return $this->CI->session->userdata($key);;
		// }else{
			// $query = $this->CI->db->get_where('config', array('key'=> $key));
			// $row = $query->row();
			// $this->CI->session->set_userdata($key, $row->value);
			// return $row->value;
		// }
		
		$query = $this->CI->db->get_where('CMS_config', array('key'=> $key));
		$row = $query->row();
		$val = $row->value;
		if(!$val){$val = "";};
		return $val;
	}
	
	function getAll(){
		$query = $this->CI->db->get('CMS_config');
		return $query->result();
	}
	
// 	
// CREATE TABLE `config` (
  // `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  // `key` varchar(255) NOT NULL,
  // `value` varchar(255) NOT NULL,
  // PRIMARY KEY (`id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
// 	
}