<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


/*
 * @author : Paul Radich
 * 
 * Added to system core for the SRG CMS
 * 
 * 
 */

function date_to_human($date , $format = ''){
	
	if(!$format){$format = "M jS Y   g:ia";}
	$newDate = date($format, strtotime($date));
	return $newDate; 
	
	
}
