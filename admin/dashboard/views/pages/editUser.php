<?php

?>

<h1>Edit User</h1>

<?php if($change == "yes") { ?>
	<div class=" alert-success well">
   		<i class="icon-ok "></i> User was Successfully <?php echo $message ?> <br/><br/>
   		<?php echo anchor("adminusers/allusers", "<i class='icon-arrow-left icon-white'></i> All Users", 'class="btn btn-primary btn-mini "'); ?> 
    </div>
	 
<?php } ?>


<?php 
	
	$attributes = array('class' => 'form-horizontal well');
	echo form_open('adminusers/upadteUser', $attributes);
	foreach($user as $use) {
?>	

<div class="container-fluid">
		<div class="row-fluid">
			<div id="mainLeft" class="span8">
				<input type="hidden" name="id" id="id" value="<?php echo $use->id; ?>" />
				<input type="hidden" name="username" id="username" value="<?php echo $use->username; ?>" />
				<div class="control-group">
					<label class="control-label" for="username">Username</label>
					<div class="controls">
						<span class="input-xlarge uneditable-input"><?php echo $use->username; ?></span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="name">Name</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="name" name="name" value="<?php echo $use->name; ?>">
					</div>
				</div>
				
				
				
				<div class="control-group">
					<label class="control-label" for="password">Change Password</label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="pass" name="pass" onfocus="this.type='password';" />
						<p class="help-block">Users Password will be changed to this value</p>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="email">Email</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,custom[email]]" id="email" name="email" value="<?php echo $use->email; ?>">
					</div>
				</div>
				<br/>
				<input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
	<?php echo anchor("adminusers/allusers", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
			</div>
			
			
			
			<div id="mainRight" class="span4">
				
					
					
				<div class="sideBox">
					<div class="sideBoxTitle">Accout Permissions</div>
						<?php foreach($acl as $access){?>
							
								<input type="radio" id="radio<?php echo $access->id; ?>" class="validate[required] " name="acl" value="<?php echo $access->id; ?>" <?php if($use->acl == $access->id){echo "checked"; }?>  /> <?php echo $access->acl_name; ?><br />
													 <?php } ?>	
				</div>	
				<br/>
				<?php if($this->configs->get('useGroups') == "true"){?>
				<div class="sideBox">
					<div class="sideBoxTitle">Group(s)</div>
					
						<?php 
						if($use->acg != "all" || $use->acg != ""){
							$values = explode(',', $use->acg); 
						}else{
							$values = array();	
						}	
						?>
						<?php $perms = explode(',',$permissions); ?>
						<?php foreach($acg as $group){?>
							<label class="checkbox">
								<input name="acg[]" type="checkbox" value="<?php echo $group->acg; ?>" <?php echo set_checkbox('acg',$group->acg,in_array($group->acg, $values) );?>  /> <?php echo $group->acg; ?>
							</label>
						 <?php } ?>
						
					
				</div>
				<?php } ?>
				
	
	</div>
	</div>

	
	
	
<?php	

	}
?>

