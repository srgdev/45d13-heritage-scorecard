<h1 class="left"><span class="ifont">B </span>Event Logs</h1>
<?php echo anchor("admin/exportLogs", "<i class='icon-download'></i> Download Logs", 'class="btn right"'); ?>
<br class="clear"/>
Logged Events are currently kept for <?php echo $this->configs->get('logKeepDays');?> days
<br/><br/>
<table  class="table table-striped">
	<thead>
	<tr>
		<th>Username</th>
		<th>Event</th>
		<th>Description</th>
		<th>Date/Time</th>
	</tr>
	</thead>
	<?php
	foreach($logData as $logLine){ ?> 
		
		<tr>
			<td><?php echo $logLine->user ?></td>
			<td><?php echo $logLine->action ?></td>
			<td><?php echo $logLine->desc ?></td>
			<td><?php echo date_to_human($logLine->date) ?></td>
		</tr>	
	<?php } ?>

</table>
<div class="pagination">
	<ul>
<?php echo $links ?>
</ul>
</div>