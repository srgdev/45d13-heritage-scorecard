<h1><?php echo $title; ?></h1>

<?php 
	
	$attributes = array('class' => 'form-horizontal well');
	echo form_open('', $attributes);
?>
<?php echo $fields; ?>

<input type="submit" name="mysubmit" value="Save" class="btn btn-primary" />

<?php if(isset($cancel)){ echo anchor($cancel, "<i class='icon-remove'></i> Cancel", 'class="btn "'); } ?>