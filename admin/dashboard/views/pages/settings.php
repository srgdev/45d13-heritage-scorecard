<h1><span class="ifont">( </span>System Settings</h1>

<?php if($this->session->flashdata('flash')) { ?>
<div class="alert alert-info">
    	<?php echo $this->session->flashdata('flash'); ?>
</div>
<?php } ?>

<?php 
$attributes = array('class' => 'form-horizontal well');
echo form_open('settings/update', $attributes); ?>
	<h3>Scorecard Settings</h3><br />
	<div class="control-group">
        <label class="control-label" for="congress">Admin Congress</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="congress" name="congress" value="<?php echo $this->configs->get('congress'); ?>">
            <p class="help-block">Default Session of Congress (for admin panel)</p>
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="sitecongress">Site Congress</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="sitecongress" name="sitecongress" value="<?php echo $this->configs->get('sitecongress'); ?>">
            <p class="help-block">Default Session of Congress (for Live Site)</p>
        </div>
    </div>
	
	<h3>System Logging</h3><br/>
	
	
	
	<div class="control-group">
		<label class="control-label" for="logging">Log All System Events</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="logging" name="logging" value="false" />
				<input name="logging" id="logging" type="checkbox" value="true" <?php if($this->configs->get('logging') == "true"){ echo "checked=\"checked\"";} ?> />
			</label> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="logKeepDays">Logged Events Expire</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="logKeepDays" name="logKeepDays" value="<?php echo $this->configs->get('logKeepDays'); ?>">
			<p class="help-block">Number of days to keep the logged events</p>
		</div>
	</div>
	
	<h3>Analytics</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="googleAnalytics">Include Analytics</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="googleAnalytics" name="googleAnalytics" value="false" />
				<input name="googleAnalytics" id="googleAnalytics" type="checkbox" value="true" <?php if($this->configs->get('googleAnalytics') == "true"){ echo "checked=\"checked\"";} ?> />  (google analytics)
			</label>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="analyticsID">Analytics ID</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="analyticsID" name="analyticsID" value="<?php echo $this->configs->get('analyticsID'); ?>">
		</div>
	</div>
	
	<h3>Page Versioning</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="versioning">Enable Page Versioning</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="versioning" name="versioning" value="false" />
				<input name="versioning" id="versioning" type="checkbox" value="true" <?php if($this->configs->get('versioning') == "true"){ echo "checked=\"checked\"";} ?> />  
			</label>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="version_limit">Versions Limit</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="version_limit" name="version_limit" value="<?php echo $this->configs->get('version_limit'); ?>">
		</div>
	</div>
	
	<h3>Database Backups</h3><br/>
	
	<div class="control-group">
        <label class="control-label" for="emailbackups">Email DB Backups</label>
        <div class="controls">
            <label class="checkbox">
                <input type="hidden" id="emailbackups" name="emailbackups" value="false" />
                <input name="emailbackups" id="emailbackups" type="checkbox" value="true" <?php if($this->configs->get('emailbackups') == "true"){ echo "checked=\"checked\"";} ?> />  
            </label>
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="dbemailaddress">Email Backups to</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="dbemailaddress" name="dbemailaddress" value="<?php echo $this->configs->get('dbemailaddress'); ?>">
        </div>
    </div>
	
	<!-- <h3>Mail</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="mes_from">Emails From Address</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="mes_from" name="mes_from" value="<?php echo $this->configs->get('mes_from'); ?>">
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="use_smtp">Use SMTP</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="use_smtp" name="use_smtp" value="false" />
				<input name="use_smtp" id="use_smtp" type="checkbox" value="true" <?php if($this->configs->get('use_smtp') == "true"){ echo "checked=\"checked\"";} ?> />  
			</label>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="SMTP">SMTP</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="SMTP" name="SMTP" value="<?php echo $this->configs->get('SMTP'); ?>">
		</div>
	</div> -->
	
	<!-- <h3>Moderation</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="moderation">Use Post Moderation</label>
		<div class="controls">
			<input type="hidden" id="moderation" name="moderation" value="false" />
			<input name="moderation" id="modiration" type="checkbox" value="true" <?php if($this->configs->get('moderation') == "true"){ echo "checked=\"checked\"";} ?> /> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="globalModeration">Use Global Moderation</label>
		<div class="controls">
			<input type="hidden" id="globalModeration" name="globalModeration" value="false" />
			<input name="globalModeration" id="globalModeration" type="checkbox" value="true" <?php if($this->configs->get('globalModeration') == "true"){ echo "checked=\"checked\"";} ?> /> 
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="groupModeration">Use Goup Moderation</label>
		<div class="controls">
			<input type="hidden" id="groupModeration" name="groupModeration" value="false" />
			<input name="groupModeration" id="groupModeration" type="checkbox" value="true" <?php if($this->configs->get('groupModeration') == "true"){ echo "checked=\"checked\"";} ?> /> 
		</div>
	</div> -->
	
	<input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />