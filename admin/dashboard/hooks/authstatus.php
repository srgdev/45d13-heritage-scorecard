<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class authstatus {
	
	 private $CI;

    public function __construct()
    {
       $this->CI =& get_instance();

       $this->CI->load->library('auth');
    }
	
	function checkAuth(){
		$this->CI =& get_instance();
		if ($this->CI->uri->segment(2) != "login" && $this->CI->uri->segment(1) != 'cfimports' && $this->CI->uri->segment(1) != 'backup' && $this->CI->uri->segment(1) != 'heritage_data')
		{
		    $this->CI->auth->checkStatus();
			 
		}
		
	}
	
	
}
