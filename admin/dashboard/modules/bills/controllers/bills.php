<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bills extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "bills";
    }
    

    
    public function index(){
        $this->load->model('billsModel');
        $load = $this->billsModel->loadBills();
        $this->template->title($this->cat)->build('listBills.php');
    }
    

    
    public function edit(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('bills');
        $bills = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->template->set('bills', $bills); 
        $this->template->title(ucfirst($this->cat).'/ Edit Vote')->build('edit');
    }
    
    
    public function update(){
        $id = $_REQUEST['id'];
        $data = array(
            'id' => $id,
            'description' => $_REQUEST['description'],
            'short_title' => $_REQUEST['title'],
            'url' => $_REQUEST['keyvoteurl'],
            'issue' => $_REQUEST['issue'],
            'sponsor_position' => $_REQUEST['position'],
            'position_txt' => $_REQUEST['positionTxt']
        );
        $this->crud->use_table('bills');
        $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
        $this->logevents->logEvent('Bill updated', $_REQUEST['billid']);
        setMessage('success', 'Bill was saved');
        redirect('/bills/edit/'.$id, 'location');
          
    }

    public function add(){
        $this->template->title(ucfirst($this->cat).'/ Add Vote')->build('add');
    }
    
    public function insert(){
        $this->load->model('billsModel');
        $id = $this->billsModel->addBill();
        $this->logevents->logEvent('Bill added', $_REQUEST['bill']);
        setMessage('success', 'Bill has been added');
        redirect('/bills/edit/'.$id, 'location');
    }
    
    
    public function delete(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('bills');
        $query = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
        $this->crud->use_table('sponsors');
        $this->crud->delete(array('bill_id' => $query->bill_id), 0, 0, array('id' => 'DESC'));
        $this->logevents->logEvent('Bill Deleted', $query->bill_id);
        setMessage('success', 'Bill has been Deleted');
        redirect('/bills', 'location');
    } 
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Bills', 'Mange bills', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Bill', 'Add Bill', $this->cat.'/add', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}