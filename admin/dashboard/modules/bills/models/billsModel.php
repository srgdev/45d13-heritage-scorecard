<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class billsModel extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
  
  public function  loadBills(){
    $congress = current_congress();
    $edit = $this->buildtable->buildEdits("bills/edit/##id", "bills/delete/##id" ); 
    $this->crud->use_table('bills');
    if(isset($_REQUEST['filter'])){
        $sort = $_REQUEST['filter'];
    }else{
        $sort = 'id';
    }
    if(!isset($_REQUEST['term'])){
        $rows = $this->crud->retrieve( array('session' => $congress),'', 0, 0, array($sort => 'ASC'));
        $this->template->set('search', 'no');
    }else{
        $this->template->set('term', $_REQUEST['term']); 
        $term = $_REQUEST['term'];
        $query = $this->db->query("SELECT * FROM Votes WHERE congressNum = '$congress' AND lName LIKE '%$term%' or fName LIKE '$term' ");
        $rows = $query->result(); 
        $this->template->set('search', 'yes');
    }
    $table = $this->buildtable->build(array('Bill ID', 'Short Title', 'Congress', 'Bill Type'), array('bill_id', 'short_title', 'session', 'billType'), $rows,$edit); 
    $this->template->set('table', $table); 
    
  }
  
  
  public function addBill(){
    $bill_id = $_REQUEST['bill'];
    $this->crud->use_table('bills');
    $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
    $url = $this->config->item('sunlight_base').'bills?bill_id='.$bill_id.'&fields='.$fields.'&apikey='.$this->config->item('SUNLIGHT_LABS_API_KEY');
    
    $bill = file_get_contents($url);
    $bill = json_decode($bill, true);
    
    $billData = $bill['results'][0];
   
    
    $data = array(
        'bill_id' => $bill_id,
        'billNumber' => $billData['number'],
        'session' => $billData['congress'],
        'billType' => $billData['bill_type'],
        'short_title' => $billData['short_title'],
        'official_title' => $billData['official_title'],
        'introduced_date' => $billData['introduced_on']
    );
    
    
    $row = $this->crud->create($data);
   
    $this->crud->use_table('sponsors');
    foreach($billData['cosponsor_ids'] as $s){
        $data = array(
            'bill_id' => $bill_id,
            'member' => $s
        );
        $this->crud->create($data);
    }
  
    // Add Sponsor ID
    $data = array(
        'bill_id' => $bill_id,
        'member' => $billData['sponsor_id']
    );
    $this->crud->create($data);
   
    return $row;
  }

}