<h1>Edit Bill</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('bills/update', $attributes);
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
                <input type="hidden" name="id" id="id" value="<?php echo $bills->id; ?>" />
                <input type="hidden" name="billid" id="billid" value="<?php echo $bills->bill_id; ?>" />
                <div class="control-group">
                    <label class="control-label" for="issue">Bill Number</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $bills->bill_id; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="issue">Session</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $bills->session; ?></span>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="title">Title</label>
                    <div class="controls">
                        <input type="text" style="width:70%" class="input-xlarge validate[required]" id="title" name="title" value="<?php echo $bills->short_title; ?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="keyvoteurl">Key Vote URL</label>
                    <div class="controls">
                        <input type="text" style="width:70%" class="input-xlarge validate[required]" id="keyvoteurl" name="keyvoteurl" value="<?php echo $bills->url; ?>">
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="issue">Issue</label>
                    <div class="controls">
                        <select name='issue'>
                            <option value="First Principles" <?php if($bills->issue == 'First Principles'){ ?> selected="selected" <?php } ?>>First Principles</option>
                            <option value="Energy and Environment" <?php if($bills->issue == 'Energy and Environment'){ ?> selected="selected" <?php } ?>>Energy & Environment</option>
                            <option value="Entitlements" <?php if($bills->issue == 'Entitlements'){ ?> selected="selected" <?php } ?>>Entitlements</option>
                            <option value="Enterprise and Free Markets" <?php if($bills->issue == 'Enterprise and Free Markets'){ ?> selected="selected" <?php } ?>>Enterprise & Free Markets</option>
                            <option value="Family/Religion" <?php if($bills->issue == 'Family/Religion'){ ?> selected="selected" <?php } ?>>Family & Religion</option>
                            <option value="American Leadership" <?php if($bills->issue == 'American Leadership'){ ?> selected="selected" <?php } ?>>American Leadership</option>
                            <option value="Health Care" <?php if($bills->issue == 'Health Care'){ ?> selected="selected" <?php } ?>>Health Care</option>
                            <option value="Rule of Law" <?php if($bills->issue == 'Rule of Law'){ ?> selected="selected" <?php } ?>>Rule of Law</option>
                            <option value="Education" <?php if($bills->issue == 'Education'){ ?> selected="selected" <?php } ?>>Education</option>
                            <option value="Protect America" <?php if($bills->issue == 'Protect America'){ ?> selected="selected" <?php } ?>>Protect America</option>
                        </select>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="position">Position</label>
                    <div class="controls">
                        <select name="position" id="position">
                            <option value="Yes" <?php if($bills->sponsor_position == 'Yes'){ ?> selected="selected" <?php } ?>>Yes</option>
                            <option value="No" <?php if($bills->sponsor_position == 'No'){ ?> selected="selected" <?php } ?>>No</option>
                        </select>
                    </div>
                </div>
                
               <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea class="input-xlarge" style="width:70%; height:200px" id="description" name="description"><?php echo stripcslashes($bills->description) ?></textarea>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="positionTxt">HA Position</label>
                    <div class="controls">
                        <textarea class="input-xlarge" style="width:70%; height:200px" id="positionTxt" name="positionTxt"><?php echo stripcslashes($bills->position_txt) ?></textarea>
                    </div>
                </div>
                
                
                <input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
    <?php echo anchor("bills", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
            </div>
            
            
          
    
    </div>
    </div>

    
    
    


