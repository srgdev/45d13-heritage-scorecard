<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class memberModal extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
  
  public function  loadMember(){
    $congress = current_congress();
    $edit = $this->buildtable->buildEdits("members/edit/##id", "" ); 
    $this->crud->use_table('Members');
    if(isset($_REQUEST['filter'])){
        $sort = $_REQUEST['filter'];
    }else{
        $sort = 'lName';
    }
    if(!isset($_REQUEST['term'])){
        $rows = $this->crud->retrieve( array('congressNum' => $congress),'', 0, 0, array($sort => 'ASC'));
        $this->template->set('search', 'no');
    }else{
        $this->template->set('term', $_REQUEST['term']); 
        $term = $_REQUEST['term'];
        $query = $this->db->query("SELECT * FROM Members WHERE congressNum = '$congress' AND lName LIKE '%$term%' or fName LIKE '$term' ");
        $rows = $query->result(); 
        $this->template->set('search', 'yes');
    }
    $table = $this->buildtable->build(array('First Name', 'Last Name', 'Congress ID', 'Party', 'State', 'Chamber', 'congress'), array('fName', 'lName', 'congID', 'party', 'state', 'chamber', 'congressNum'), $rows,$edit); 
    $this->template->set('table', $table); 
    
  }
    

}