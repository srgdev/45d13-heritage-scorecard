<h1>Edit Member</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('members/update', $attributes);
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
                <input type="hidden" name="id" value="<?php echo $members->id ?>" />
                <h2><?php echo $members->title; ?>. <?php echo $members->fName; ?>  <?php echo $members->lName; ?> (<?php echo $members->party; ?>)</h2>
                <br />
                
                <div class="control-group">
                    <label class="control-label" for="fName">First Name</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge validate[required]" id="fName" name="fName" value="<?php echo $members->fName; ?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="lName">Last Name</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge validate[required]" id="lName" name="lName" value="<?php echo $members->lName; ?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="title">State</label>
                    <div class="controls">
                         <span class="input-xlarge uneditable-input"><?php echo $members->state; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="d">District</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $members->district; ?></span>
                    </div>
                </div>
                
                <input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
                <?php echo anchor("members", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
                </form>
                <br /><br />
                
                
                <?php 
                    echo form_open_multipart('members/uploadImg');
                ?>  


                <h4><strong>Member Image</strong></h4>
                <br />
                <input type="hidden" name="id" value="<?php echo $members->id ?>" />
                <input type="hidden" name="first" value="<?php echo $members->fName ?>" />
                <input type="hidden" name="last" value="<?php echo $members->lName ?>" />
                <div class="control-group">
                    <label class="control-label" for="d"><img src="http://localhost:8888/45D13_heritage_scorecard_new/admin/memImgs/<?php echo $members->image_path ?>" /></label>
                    <div class="controls">
                        <input type="file" name="userfile" value="" id="userfile"/><br /><br />
                        <input type="submit" name="mysubmit" value="Change Image" class="btn btn-primary" />
                    </div>
                </div>
                
                

            </div>
            
            
          
    
    </div>
    </div>

    
    

