<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "members";
    }
    

    
    public function index(){
        $this->load->model('memberModal');
        $load = $this->memberModal->loadMember();
        $this->template->title($this->cat)->build('listMembers.php');
    }
    

    
    public function edit(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('Members');
        $members = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->template->set('members', $members); 
        $this->template->title(ucfirst($this->cat).'/ Edit Member')->build('edit');
    }

    public function update(){
        $id = $_REQUEST['id'];
        $data = array(
            'id' => $id,
            'fName' => $_REQUEST['fName'],
            'lName' => $_REQUEST['lName']
        );
        $this->crud->use_table('Members');
        $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
        $name = $_REQUEST['fName'].' '.$_REQUEST['lName'];
        $this->logevents->logEvent('Member Name Changed', $name);
        setMessage('success', 'Member was saved');
        redirect('/members/edit/'.$id, 'location');
          
    }
    
    
    public function uploadImg(){
        $config['upload_path'] = './memImgs/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '100';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $newName = time();
        $config['file_name']  = $newName;
        
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());

            print_r($error);
        }
        else
        {
            $id = $_REQUEST['id'];  
            $ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION); 
            $data = array(
                'id' => $id,
                'image_path' => $newName.'.'.$ext
            );
            $this->crud->use_table('Members');
            $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
            $name = $_REQUEST['first'].' '.$_REQUEST['last'];
            $this->logevents->logEvent('Member Image Changed', $name);
            setMessage('success', 'Member Image was saved');
            redirect('/members/edit/'.$id, 'location');
            

          
        }
    }
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Members', 'Mange members', $this->cat.'/index', 'user', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}