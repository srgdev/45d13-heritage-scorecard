<h1>Edit Vote</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('votes/update', $attributes);
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
                <input type="hidden" name="id" id="id" value="<?php echo $votes->id; ?>" />
               <input type="hidden" name="roll" id="roll" value="<?php echo $votes->roll_id; ?>" />
                <div class="control-group">
                    <label class="control-label" for="issue">Roll Call #</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->roll_id; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="title">Title</label>
                    <div class="controls">
                        <input type="text" style="width:70%" class="input-xlarge validate[required]" id="title" name="title" value="<?php echo $votes->question; ?>">
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="issue">Session</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->session; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="issue">Chamber</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->chamber; ?></span>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="issue">Result</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->result; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="issue">Vote Date</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->vote_date; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="issue">Bill</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $votes->bill_id; ?></span>
                    </div>
                </div>
                
                
                
                <div class="control-group">
                    <label class="control-label" for="keyvoteurl">Key Vote URL</label>
                    <div class="controls">
                        <input type="text" style="width:70%" class="input-xlarge validate[required]" id="keyvoteurl" name="keyvoteurl" value="<?php echo $votes->url; ?>">
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="issue">Issue</label>
                    <div class="controls">
                        <select name='issue'>
                            <option value="First Principles" <?php if($votes->issue == 'First Principles'){ ?> selected="selected" <?php } ?>>First Principles</option>
                            <option value="Energy and Environment" <?php if($votes->issue == 'Energy and Environment'){ ?> selected="selected" <?php } ?>>Energy & Environment</option>
                            <option value="Entitlements" <?php if($votes->issue == 'Entitlements'){ ?> selected="selected" <?php } ?>>Entitlements</option>
                            <option value="Enterprise and Free Markets" <?php if($votes->issue == 'Enterprise and Free Markets'){ ?> selected="selected" <?php } ?>>Enterprise & Free Markets</option>
                            <option value="Family/Religion" <?php if($votes->issue == 'Family/Religion'){ ?> selected="selected" <?php } ?>>Family & Religion</option>
                            <option value="American Leadership" <?php if($votes->issue == 'American Leadership'){ ?> selected="selected" <?php } ?>>American Leadership</option>
                            <option value="Health Care" <?php if($votes->issue == 'Health Care'){ ?> selected="selected" <?php } ?>>Health Care</option>
                            <option value="Rule of Law" <?php if($votes->issue == 'Rule of Law'){ ?> selected="selected" <?php } ?>>Rule of Law</option>
                            <option value="Education" <?php if($votes->issue == 'Education'){ ?> selected="selected" <?php } ?>>Education</option>
                            <option value="Protect America" <?php if($votes->issue == 'Protect America'){ ?> selected="selected" <?php } ?>>Protect America</option>
                        </select>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="prefVote">Preferred Vote</label>
                    <div class="controls">
                        <select name="prefVote" id="prefVote">
                            <option value="Yes" <?php if($votes->perferred_vote == 'Yes'){ ?> selected="selected" <?php } ?>>Yes</option>
                            <option value="No" <?php if($votes->perferred_vote == 'No'){ ?> selected="selected" <?php } ?>>No</option>
                        </select>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea class="input-xlarge" style="width:70%; height:100px" id="description" name="description"><?php echo stripcslashes($votes->description) ?></textarea>
                    </div>
                </div>
                
                
                <div class="control-group">
                    <label class="control-label" for="position">HA Position</label>
                    <div class="controls">
                        <textarea class="input-xlarge" style="width:70%; height:200px" id="position" name="position"><?php echo stripcslashes($votes->ha_position) ?></textarea>
                    </div>
                </div>
                
                <input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
    <?php echo anchor("votes", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
            </div>
            
            
          
    
    </div>
    </div>

    
    
    

