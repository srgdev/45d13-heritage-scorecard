<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class votesModel extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
  
  public function  loadVote(){
    $congress = current_congress();
    $edit = $this->buildtable->buildEdits("votes/edit/##id", "votes/delete/##id" ); 
    $this->crud->use_table('voteTable');
    if(isset($_REQUEST['filter'])){
        $sort = $_REQUEST['filter'];
    }else{
        $sort = 'id';
    }
    if(!isset($_REQUEST['term'])){
        $rows = $this->crud->retrieve( array('session' => $congress),'', 0, 0, array($sort => 'ASC'));
        $this->template->set('search', 'no');
    }else{
        $this->template->set('term', $_REQUEST['term']); 
        $term = $_REQUEST['term'];
        $query = $this->db->query("SELECT * FROM Votes WHERE congressNum = '$congress' AND lName LIKE '%$term%' or fName LIKE '$term' ");
        $rows = $query->result(); 
        $this->template->set('search', 'yes');
    }
    $table = $this->buildtable->build(array('Roll Call', 'Title', 'Congress', 'Chamber'), array('roll_id', 'question', 'session', 'chamber'), $rows,$edit); 
    $this->template->set('table', $table); 
    
  }
    

  public function addVote(){
      $rollCall = $_REQUEST['rollCall'];
      $this->crud->use_table('voteTable');
      $query = $this->crud->retrieve(array('roll_id' => $rollCall), '', 0, 0, array('id' => 'DESC'));
      if(count($query) != 0 ){ $row = 'dup'; }else{
            $fields = 'voters,result,number,chamber,year,congress,voted_at,question,bill_id';
            $url = $this->config->item('sunlight_base').'votes?roll_id='.$rollCall.'&fields='.$fields.'&apikey='.$this->config->item('SUNLIGHT_LABS_API_KEY');
            // echo $url;
            $votes = file_get_contents($url);
            $votes = json_decode($votes, true);
            $voteData = $votes['results'];
            // print_r($voteData[0]);
            // echo $voteData[0]['result'];
            $billid = (isset($voteData[0]['bill_id'])) ? $voteData[0]['bill_id'] : '';
            $data = array(
                'roll_id' => $rollCall,
                'bill_id' => $billid,
                'number' => $voteData[0]['number'],
                'year' => $voteData[0]['year'],
                'chamber' => $voteData[0]['chamber'],
                'session' => $voteData[0]['congress'],
                'result' => $voteData[0]['result'],
                'vote_date' => $voteData[0]['voted_at'],
                'question' => $voteData[0]['question']
            );
            
            $row = $this->crud->create($data);
            $congress = $voteData[0]['congress'];
            $this->crud->use_table('votes');
            foreach($voteData[0]['voters'] as $v) {
                $voter = $v['voter']['bioguide_id'];
                $vote = $v['vote'];
                if($vote == "Yea") {$vote = "Yes";}
                if($vote == "Nay") {$vote = "No";}
                    
                 $data1 = array(
                    'vote_id' => $rollCall,
                    'member' => $voter,
                    'vote' => $vote,
                    'session' => $congress
                );   
                
                $this->crud->create($data1);
            }
            
            return $row;
      }    
            
// 
                
            // }
            // $this->cacheClear();
            // return "added";
        
  }
}