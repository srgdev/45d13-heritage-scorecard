<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class votes extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "votes";
    }
    

    
    public function index(){
        $this->load->model('votesModel');
        $load = $this->votesModel->loadVote();
        $this->template->title($this->cat)->build('listVotes.php');
    }
    

    
    public function edit(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('voteTable');
        $votes = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->template->set('votes', $votes); 
        $this->template->title(ucfirst($this->cat).'/ Edit Vote')->build('edit');
    }

    public function update(){
        $id = $_REQUEST['id'];
        $data = array(
            'id' => $id,
            'description' => $_REQUEST['description'],
            'question' => $_REQUEST['title'],
            'url' => $_REQUEST['keyvoteurl'],
            'issue' => $_REQUEST['issue'],
            'perferred_vote' => $_REQUEST['prefVote'],
            'ha_position' => $_REQUEST['position']
        );
        $this->crud->use_table('voteTable');
        $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
        $this->logevents->logEvent('Voted updated', $_REQUEST['roll']);
        setMessage('success', 'Vote was saved');
        redirect('/votes/edit/'.$id, 'location');
          
    }
    
    public function add(){
        $this->template->title(ucfirst($this->cat).'/ Add Vote')->build('add');
    }
    
    public function insert(){
        $this->load->model('votesModel');
        $id = $this->votesModel->addVote();
        $this->logevents->logEvent('Voted added', $_REQUEST['rollCall']);
        setMessage('success', 'Vote has been added');
        redirect('/votes/edit/'.$id, 'location');
    }
    
    public function delete(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('voteTable');
        $query = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
        $this->crud->use_table('votes');
        $this->crud->delete(array('vote_id' => $query->roll_id), 0, 0, array('id' => 'DESC'));
        $this->logevents->logEvent('Voted Deleted', $query->roll_id);
        setMessage('success', 'Vote has been Deleted');
        redirect('/votes', 'location');
    } 
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Votes', 'Mange votes', $this->cat.'/index', 'check', 'index', '2', '5', $this->cat);
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Vote', 'Add Vote', $this->cat.'/add', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}