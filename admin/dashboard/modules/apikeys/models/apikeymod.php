<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class apikeymod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
   function genKey(){
       $length = 20; // 16 Chars long
        $key = "";
        for ($i=1;$i<=$length;$i++) {
          // Alphabetical range
          $alph_from = 65;
          $alph_to = 90;
        
          // Numeric
          $num_from = 48;
          $num_to = 57;
        
          // Add a random num/alpha character
          $chr = rand(0,1)?(chr(rand($alph_from,$alph_to))):(chr(rand($num_from,$num_to)));
          if (rand(0,1)) $chr = strtolower($chr);
          $key.=$chr;
          }
        return $key;
   }
}