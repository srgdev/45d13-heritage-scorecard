<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class apikeys extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "apikeys";
        $this->load->model('apikeymod');
    }
    
    public function index(){
        $this->crud->use_table('api_keys');
        $rows =   $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $edit = $this->buildtable->buildEdits("apikeys/edit/##id", "apikeys/delete/##id" );
        $table = $this->buildtable->build(array('Company', 'Name', 'Email', 'API Key'), array('company', 'name', 'email', 'key'), $rows,$edit); 
        $this->template->set('table', $table); 
        $this->template->title($this->cat)->build('list.php');
    }
    
    public function add(){
        $this->template->title(ucfirst($this->cat).'/ Add API key')->build('add');
    }
    
    public function insert(){
        $this->crud->use_table('api_keys');
        $data = array(
            'company' => $_REQUEST['company'],
            'name' => $_REQUEST['name'],
            'email' => $_REQUEST['email'],
            'key' => $this->apikeymod->genKey()
        );
        $id = $this->crud->create($data);
        $this->logevents->logEvent('API Key created', $_REQUEST['company']);
        setMessage('success', 'API Key Created');
        redirect('/apikeys/edit/'.$id, 'location'); 
    }
    
    public function edit(){
        $id = $this->uri->segment(3);
        $this->crud->use_table('api_keys');
        $row = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->template->set('row', $row); 
        $this->template->title(ucfirst($this->cat).'/ Edit API key')->build('edit');
    }

    public function update(){
        $id = $_REQUEST['id'];
        $key = (isset($_REQUEST['newkey'])) ? $this->apikeymod->genKey() : $_REQUEST['key'];
        $data = array(
            'company' => $_REQUEST['company'],
            'name' => $_REQUEST['name'],
            'email' => $_REQUEST['email'],
            'key' => $key
        );
       $this->crud->use_table('api_keys');
       $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
       
       if(isset($_REQUEST['newkey'])){
           $this->logevents->logEvent('API Key Changed', $_REQUEST['company']);
           setMessage('success', 'API Key was saved with new Key');
       }else{
           $this->logevents->logEvent('API Key info updated', $_REQUEST['company']);
           setMessage('success', 'API Key was saved');
       }
       
       redirect('/apikeys/edit/'.$id, 'location'); 
    }
    
    public function delete(){
        $id = $this->uri->segment(3);
        $this->crud->use_table('api_keys');
        $row = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
        $this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
        $this->logevents->logEvent('API Key deleted', $row->company);
        setMessage('success', 'API Key was deleted');
        redirect('/apikeys', 'location'); 
    }
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Api Keys', 'Mange Api Keys', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
       
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}