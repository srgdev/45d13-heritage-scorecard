<h1>Edit API Key</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('apikeys/update', $attributes);
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
               
               <input type="hidden" name="id" value="<?php echo $row->id ?>" />
               <input type="hidden" name="key" value="<?php echo $row->key ?>" />
                <div class="control-group">
                    <label class="control-label" for="company">Company</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge validate[required]" id="company" name="company" value="<?php echo $row->company ?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="name">Contact Name</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="name" name="name" value="<?php echo $row->name ?>">
                    </div>
                </div>
                
               <div class="control-group">
                    <label class="control-label" for="email">Contact Email</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="email" name="email" value="<?php echo $row->email ?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="key">API Key</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $row->key ?></span> 
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="newkey">Generate New Key</label>
                    <div class="controls">
                        <input type="checkbox" name="newkey" id="newkey" />
                    </div>
                </div>
                <input type="submit" name="mysubmit" value="Save" class="btn btn-primary" />
                <?php echo anchor("apikeys", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
       </div>
            
            
          
    
    </div>
    </div>

    
    
    

