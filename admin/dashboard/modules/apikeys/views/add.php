<h1>Add API Key</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('apikeys/insert', $attributes);
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
               
                <div class="control-group">
                    <label class="control-label" for="company">Company</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge validate[required]" id="company" name="company" value="">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="name">Contact Name</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="name" name="name" value="">
                    </div>
                </div>
                
               <div class="control-group">
                    <label class="control-label" for="email">Contact Email</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="email" name="email" value="">
                    </div>
                </div>
                <input type="submit" name="mysubmit" value="Generate API Key" class="btn btn-primary" />
                <?php echo anchor("apikeys", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
       </div>
            
            
          
    
    </div>
    </div>

    
    
    

