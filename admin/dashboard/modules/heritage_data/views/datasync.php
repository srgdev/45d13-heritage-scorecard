<h1>Data Sync</h1>

<script>
    $(function(){
        var urler = ' + <?php echo base_url().index_page()."/" ?> + ';
        $('.scoreBtn').click(function(){
            $(this).button('loading')
            var cong = $('.sessions').val();
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/heritage_data/calcScores/',
               cache: false,
               success: function(data){
                   $('.scoreBtn').button('reset');
               }
            });
        });
        
        
        $('.memBtn').click(function(){
            $(this).button('loading')
             var cong = $('.sessions').val();
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/heritage_data/syncMem/members/',
               cache: false,
               success: function(data){
                   $('.memBtn').button('reset');
               }
            });
        });
        
       
        
        
    })
</script>

<h3>Sync Members Database</h3>
<p>This can take upto 5 min</p>
<br />
<button type="button" class="btn btn-primary loading memBtn" data-loading-text="Loading...">Sync Members</button>
<br /><br />
<h3>Calculate Scores</h3>
<p>This can take upto 5 min</p>
<br />
<button type="button" class="btn btn-primary loading scoreBtn" data-loading-text="Loading...">Calculate Scores</button>
<br /><br />
<h3>Download Member Spreadsheets</h3>
<br />
<a href="<?php echo base_url().index_page() ?>/heritage_data/downloadMembers/house"><button type="button" class="btn btn-primary loading downloadBtn" rel="house" data-loading-text="Loading...">Download House</button></a>
<br /><br />
<a href="<?php echo base_url().index_page() ?>/heritage_data/downloadMembers/senate"><button type="button" class="btn btn-primary loading downloadBtn" rel="house" data-loading-text="Loading...">Download Senate</button></a>
