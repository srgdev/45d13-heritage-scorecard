<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class download_members extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
  public function download($chamber){
        //ported from old build
        
        // Get data records from table.

        $congress = current_congress();
        $name = $chamber."_".$congress;
        $this->crud->use_table('Members');
        $result = $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => $congress ), '', 0, 0, array('id' => 'DESC'));
        
        
        // Functions for export to excel.
        function xlsBOF() {
        echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
        return;
        }
        function xlsEOF() {
        echo pack("ss", 0x0A, 0x00);
        return;
        }
        function xlsWriteNumber($Row, $Col, $Value) {
        echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
        echo pack("d", $Value);
        return;
        }
        function xlsWriteLabel($Row, $Col, $Value ) {
        $L = strlen($Value);
        echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
        echo $Value;
        return;
        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=".$name.".xls ");
        header("Content-Transfer-Encoding: binary ");
        
        xlsBOF();
        
        /*
        Make a top line on your excel sheet at line 1 (starting at 0).
        The first number is the row number and the second number is the column, both are start at '0'
        */
        
        xlsWriteLabel(0,0,$chamber." MC Scores");
        
        // Make column labels. (at line 3)
        xlsWriteLabel(2,0,"First Name");
        xlsWriteLabel(2,1,"Last Name");
        xlsWriteLabel(2,2,"State");
        xlsWriteLabel(2,3,"District ");
        xlsWriteLabel(2,4,"Score");
        xlsWriteLabel(2,5,"Sponsorship %");
        xlsWriteLabel(2,6,"Vote %");
        xlsWriteLabel(2,7,"Session %");
        
        $xlsRow = 3;
        
        // Put data records from mysql by while loop.
        
        foreach($result as $row){
        
            xlsWriteLabel($xlsRow,0,$row->fName);
            xlsWriteLabel($xlsRow,1,$row->lName);
            xlsWriteLabel($xlsRow,2,$row->state);
            xlsWriteLabel($xlsRow,3,$row->district);
            xlsWriteLabel($xlsRow,4,$row->score."%");
            xlsWriteLabel($xlsRow,5,$row->SponsorScore."%");
            xlsWriteLabel($xlsRow,6,$row->voteScore."%");
            xlsWriteLabel($xlsRow,7,$row->congressNum);
            
            
            
            $xlsRow++;
        }
        xlsEOF();
        return 'good';
  } 
  
}