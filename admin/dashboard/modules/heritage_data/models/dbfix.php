<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class dbfix extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
    
   public function fixvotes(){
        $this->db->query("CREATE TABLE IF NOT EXISTS `api_keys` (
              `id` mediumint(9) NOT NULL AUTO_INCREMENT,
              `key` varchar(255) NOT NULL,
              `name` varchar(5) NOT NULL,
              `email` varchar(255) NOT NULL,
              `company` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
       
        $this->db->query('ALTER TABLE votes 
           ADD session varchar(255)');
       
        $this->crud->use_table('voteTable');
        $votes =  $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        foreach($votes as $v){
            $session = $v->session;
            $this->crud->use_table('votes');
            $this->crud->update(array('vote_id' => $v->roll_id),array('session' => $session), 0, 0, array('id' => 'DESC'));
        }
   }
  
  public function fixImages(){
      $this->crud->use_table('Members');
      $members = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
      $i = 0;
      echo count($members);
      foreach($members as $m){          
          $img = $m->image_path;
          echo $img;          
          if(strpos($img,'http') !== false){
              $check = $this->members->checkImgUrl($img);
              if(!$check){
                  $this->crud->update(array('id' => $m->id),array('image_path' => 'bigPic.jpg'), 0, 0, array('id' => 'DESC'));
                  $i++;
                  echo 'image fixed '.$m->title.' '.$m->lName;
              }else{
                  echo 'image good '.$m->title.' '.$m->lName;
              }
              echo '<br/>';
          }
      }
      // $this->logevents->logEvent(' Member images fix ran', $i.' images fixed');
  }
}