<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class scores extends CI_Model
{
  
  function __construct(){
    parent::__construct();
  }
  
  public function calcScores(){
      $this->updateBills();
  } 
  
  
  
  public function updateBills() {
        
        $this->crud->use_table('bills');
        echo '<br/>';
        $query =  $this->crud->retrieve(array('session' => current_congress()), '', 0, 0, array('id' => 'DESC'));
        foreach($query as $b){
            echo $b->bill_id;
            echo '<br/>';
            $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
            $url = $this->config->item('sunlight_base').'bills?bill_id='.$b->bill_id.'&fields='.$fields.'&apikey='.$this->config->item('SUNLIGHT_LABS_API_KEY');
            
            $bill = file_get_contents($url);
            $bill = json_decode($bill, true);
            echo count($bill);
            echo '<br/>';
            $billData = $bill['results'][0];
           
            
            $data = array(
                'bill_id' => $b->bill_id,
                'billNumber' => $billData['number'],
                'session' => $billData['congress'],
                'billType' => $billData['bill_type'],
                'short_title' => $billData['short_title'],
                'official_title' => $billData['official_title'],
                'introduced_date' => $billData['introduced_on']
            );
            
            $row = $this->crud->update(array('bill_id' => $b->bill_id),$data, 0, 0, array('id' => 'DESC'));
           
            $this->crud->use_table('sponsors');
            foreach($billData['cosponsor_ids'] as $s){
                $data = array(
                    'bill_id' => $b->bill_id,
                    'member' => $s
                );
                $query =  $this->crud->retrieve(array('bill_id' =>  $b->bill_id, 'member' => $s), '', 0, 0, array('id' => 'DESC'));
                if(count($query) == 0){
                    $this->crud->create($data);
                }
            }

            // Add Sponsor ID
            $data = array(
                'bill_id' => $b->bill_id,
                'member' => $billData['sponsor_id']
            );
            $query =  $this->crud->retrieve(array('bill_id' =>  $b->bill_id, 'member' => $billData['sponsor_id']), '', 0, 0, array('id' => 'DESC'));
            if(count($query) == 0){
                $this->crud->create($data);
            }
        }
        echo "<br/><br/>";
        $upDate = date('m/d/y');
        $this->crud->use_table('updated');
        $this->crud->update(array('id' => '1'),array('updated' => $upDate), 0, 0, array('id' => 'DESC'));
        $session = current_congress();
        $this->all($session);
    }// End add


    public function all($session) {
            $i=1;
            $this->crud->use_table('Members');
            $members =  $this->crud->retrieve(array('congressNum' => current_congress()), '', 0, 0, array('id' => 'DESC'));
            foreach($members as $row){
                $member = $row->congID;
                $state = $row->state; 
                $chamber = $row->chamber;
                echo "chamber: ".$chamber;
                $score = $this->calcScore($row->congID, $chamber); 
                   
                echo $i. " : ".$member." : ".$score."<br/><br/><br/>";
                $congress = current_congress();
                $this->crud->use_table('Members');
                $this->crud->update(array('congID' => $member, 'congressNum' => $congress),array('score' => $score), 0, 0, array('id' => 'DESC'));
                $i++;
            }
    }
    
    public function calcScore($memberID, $chamber) {
        $voteScore = '';
        $sponsorScore = '';
        $this->crud->use_table('votes');
        $voteQuery =  $this->crud->retrieve(array('member' => $memberID, 'session' => current_congress()), '', 0, 0, array('id' => 'DESC'));
        $votesCast = count($voteQuery);
        
        $this->crud->use_table('voteTable');
        $votesQuery = $this->crud->retrieve(array('session' => current_congress()), '', 0, 0, array('id' => 'DESC'));
        $v = 0;
        $i = 0;
        
        foreach($votesQuery as $row){
            $id = $row->roll_id;
            //echo '<br/>'.$memberID.'<br/>'.$id.'<br/>';
            $pref = $row->perferred_vote;
            
            $this->crud->use_table('votes');
            $comp =  $this->crud->retrieve(array('vote_id' => $id, 'member' => $memberID), 'row', 0, 0, array('id' => 'DESC'));
            //echo count($comp);
            if(count($comp) > 0){
                $vote = $comp->vote;
                if($pref == $vote ) { $i++; }
                if($pref == null || $pref == "" || $vote == "Not Voting" || $vote == "Present" || $vote == "present") {if($votesCast) {$votesCast = $votesCast-1;}}
                //if($vote == "Not Voting") {if($votesCast) {$votesCast = $votesCast-1;}}
                $v++;
            }
        }
        $votesRight =  $i;

        if($votesCast != 0 && $votesCast != "") {
            echo $votesRight.' / '.$votesCast;
            $rawVoteScore =  $votesRight / $votesCast;
        
    //      echo "raw vote score = ".$rawVoteScore;
    //      echo "<br/>";
            
    //      echo "vote Score = ".$voteScore;
    //      echo "<br/>";
            echo $rawVoteScore;
        }else{
            $voteScore = "no"; 
        } 
        if($chamber == "house") {
            $this->crud->use_table('bills');
            $billQuery = $this->db->query("SELECT * FROM bills WHERE (billType = 'hr' or billType = 'hltr') AND session = '".current_congress()."'");
        }elseif($chamber == "senate") {
            $billQuery = $this->db->query("SELECT * FROM bills WHERE (billType = 's' or billType = 'sltr') AND session = '".current_congress()."'");
        }
        $sponsorOp = count($billQuery->result());

        $j = 0;
        foreach($billQuery->result() as $row){
            $id = $row->bill_id;
            $pref1 = $row->sponsor_position;
            
            $votecomp = $this->db->query("SELECT * FROM sponsors WHERE bill_id = '$id' AND member = '$memberID' ");
            $comp2 = count($votecomp->result());
            if($comp2 != 0 && $pref1 == "Yes") { $j++; }
            //if($pref1 == null || $pref1 == "" || $comp2 == null || $comp2 == "" || !$comp2){if($sponsorOp) { $sponsorOp = $sponsorOp - 1;}}
        }
        $sonposrRight = $j;
        
        
        if($sponsorOp != 0 && $sponsorOp != "") {
            $rawSponsor =  $sonposrRight/$sponsorOp;
    //      echo "raw sponsor = ".$rawSponsor;
    //      echo "<br/>";
    //      echo "sponsor Score = ".$sponsorScore;
    //      echo "<br/>";
        }else{ 
            $sponsorScore = "no"; 
        }
        
        $sponsOnly = 0;
        $voteOnly = 0; 
        if($voteScore == "no" && $sponsorScore != "no") {
            $sponsorScore =  round($rawSponsor*100);
            $voteScore = "--";
            $score = $sponsorScore;
        }elseif($sponsorScore == "no" && $voteScore != "no") {
            $voteScore = round($rawVoteScore*100);
            $sponsorScore = "--";
            $score = $voteScore;
        }elseif($voteScore != "no" && $sponsorScore != "no"){
            $sponsPercent = $sponsorOp;
            $votePercent = 100 - $sponsPercent;
            echo "#of Spons: ".$sponsorOp."<br/>"."votePercent: ".$votePercent."<br/>";
            $sponsorScore =  round($rawSponsor*$sponsPercent);
            $voteScore = round($rawVoteScore*$votePercent);
            $sponsOnly = round($rawSponsor*100);
            $voteOnly = round($rawVoteScore*100); 
            $score = $voteScore + $sponsorScore;
        }elseif($voteScore == "no" && $sponsorScore == "no"){
            $sponsorScore = "--";
            $voteScore = "--";
            $score = "--";
        }
        
        
//      echo "final score = ".$score."%";
        echo "memberid= ".$memberID." vote  = ".$voteOnly." spons = ".$sponsOnly." score = ".$score.'<br/>';
        //echo " ".$voteScore."  ".$sponsorScore."  ".$score;
        $congress = current_congress();
        if($memberID == "B000589") {
            $this->db->query("UPDATE Members SET  voteScore = 'NA' WHERE congID = '$memberID' and congressNum = '$congress'");
            $this->db->query("UPDATE Members SET  SponsorScore = 'NA' WHERE congID = '$memberID' and congressNum = '$congress'");
            $score = "NA";
            return $score;          
        }else{
            $this->db->query("UPDATE Members SET  voteScore = '$voteOnly' WHERE congID = '$memberID' and congressNum = '$congress'");
            $this->db->query("UPDATE Members SET  SponsorScore = '$sponsOnly' WHERE congID = '$memberID' and congressNum = '$congress'");
            return $score;
        }
    }   


  public function cacheClear(){
      
        $cache_path = $_SERVER['DOCUMENT_ROOT']."/api/api_1_1/cache/";
        echo $cache_path;
        foreach(glob($cache_path.'*.*') as $v){
            echo "here";
           unlink($v);
        }
    }


    
}