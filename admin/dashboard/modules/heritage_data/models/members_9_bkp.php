<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class members extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->ny_key = "e06ccbd8f28eb3edc212fb864b937517:15:64541977";
        $this->nyApiLocation = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/";
        $this->slApiLocation = "http://congress.api.sunlightfoundation.com/legislators?apikey=";
        $this->session = $this->config->item('CONG_SESSION');
    }
    
    // Sync each member of congress to the DB by chamber 
    //
    // SRC: NY Times API
    public function syncMembers($chamber){
        $congress = current_congress();
        $this->session = $congress;
        
        echo $this->slApiLocation.$this->config->item('SUNLIGHT_LABS_API_KEY')."&bioguide_id=";
        $data = json_decode(file_get_contents($this->buildNYPath($chamber, $congress)),true);
        $results = $data['results'][0]['members'];
        $session = $data['results'][0]['congress'];
        foreach($results as $m){
            $state = $m['state'];
            
            //Filter out no included states
            if($state != "MP" &&$state != "GU" && $state != "AS" && $state != "PR" && $state != "VI" && $state != "FM" && $state != "MH" && $state != "PW" && $state != "CZ" && $state != "PI" && $state != "TT" && $state != "CM" ) {
               
                // Get Member Data from Sulight Labs
                $memberData = json_decode(file_get_contents($this->slApiLocation.$this->config->item('SUNLIGHT_LABS_API_KEY')."&bioguide_id=".$m['id']), true);
                $memberData = $memberData['results'];
                echo '<br/><br/>'.count($memberData).'<br/><br/>';
                $govtrack_id = "";
                $image_path = "bigPic.jpg";
                
                if(isset($memberData[0])) {
                    $memberResults = $memberData[0];
                    print_r($memberResults);
                    if($memberResults){
                        $govtrack_id = $memberResults['govtrack_id'];
                        //Get The Image
                        $image_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                        
                    }
                }
                
                
                
                
    
                // $g_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                // if(getimagesize($g_path)) {
                // copy($g_path , '../../img/'.$govtrack_id.'-200px.jpeg');
                // $image_path = $govtrack_id.'-200px.jpeg';
                // }else{
                    // echo "no IMgae";
                // $image_path = "bigPic.jpg";
                // }
                
                // Scrap the bio from bioguide.congress.gov
                // $this->load->helper('htmldom');
                // if($html = file_get_html('http://bioguide.congress.gov/scripts/biodisplay.pl?index='.$m['id'])) {
                // foreach($html->find('p') as $e) { $bio = $e;}
                // }else{ $bio = "";}
                
                $district = (isset($m['district'])) ? $m['district'] : "";
                $title = ($chamber = 'house') ? 'Rep' : 'Sen';
                //Build the Data Array
                $data = array(
                    'fName' => $m['first_name'],
                    'mname' => $m['middle_name'],
                    'lName' => $m['last_name'],
                    'congID' => $m['id'],
                    'govTrackID' => $govtrack_id,
                    'congressNum' => $session,
                    'state' => $m['state'],
                    'party' => $m['party'],
                    'district' => $district,
                    'chamber' => $chamber,
                    'title' => $title,
                    'image_path' => $image_path
                );
                
                $this->crud->use_table('Members');
                //Check if record exist
                $check =  $this->crud->retrieve(array('congID' => $m['id'], 'congressNum' => $session, 'chamber' => $chamber), '', 0, 0, array('id' => 'DESC'));
                if(count($check) > 0 ){
                    //$this->crud->update(array('congID' => $m['id'], 'congress' => $session),$data, 0, 0, array('id' => 'DESC'));
                }else{
                    $this->crud->create($data);
                }
                
                echo $m['id'].'<br/>';
           }    
        }
    }
    
    private function buildNYPath($chamber, $congress){
        return $this->nyApiLocation.$congress."/".$chamber."/members.json?api-key=".$this->config->item('NY_TIMES_API_KEY');
    }
}