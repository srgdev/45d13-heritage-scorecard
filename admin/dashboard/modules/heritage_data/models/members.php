<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class members extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->ny_key = "e06ccbd8f28eb3edc212fb864b937517:15:64541977";
        $this->nyApiLocation = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/";
        $this->slApiLocation = "http://congress.api.sunlightfoundation.com/legislators?apikey=";
        $this->session = $this->config->item('CONG_SESSION');
        $this->debug = false;
    }
    
    // Sync each member of congress to the DB by chamber 
    //
    // SRC: NY Times API
    public function syncMembers($chamber){
        $congress = current_congress();
        $this->session = $congress;
        
        $data = json_decode(file_get_contents($this->buildNYPath($chamber, $congress)),true);
        $results = $data['results'][0]['members'];
        $session = $data['results'][0]['congress'];
        foreach($results as $m){
            $state = $m['state'];
            
            //Filter out no included states
            if($state != "MP" &&$state != "GU" && $state != "AS" && $state != "PR" && $state != "VI" && $state != "FM" && $state != "MH" && $state != "PW" && $state != "CZ" && $state != "PI" && $state != "TT" && $state != "CM" ) {
               
                // Get Member Data from Sulight Labs
                $memberData = json_decode(file_get_contents($this->slApiLocation.$this->config->item('SUNLIGHT_LABS_API_KEY')."&bioguide_id=".$m['id']), true);
                $memberData = $memberData['results'];
                $this->doDebug('<br/><br/>'.count($memberData).'<br/><br/>', 'echo');
                $govtrack_id = "";
                
                if(isset($memberData[0])) {
                    $memberResults = $memberData[0];
                    $this->doDebug($memberResults, 'print');
                    if($memberResults){
                        $govtrack_id = $memberResults['govtrack_id'];
                        //Get The Image
                        $image_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                        
                    }
                }
                
                // 9/16/13 Removed old commented code and add a url check to each image
                  // This will add significant load to the proccess
                  // Paul Radich SRG --
                $g_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                $image_path = ($this->checkImgUrl($g_path)) ? $g_path : "bigPic.jpg";
                
                $district = (isset($m['district'])) ? $m['district'] : "";
                
                //Changed 9/16/13
                    //old : $title = ($chamber = 'house') ? 'Rep' : 'Sen';
                    //add == 
                    // Paul Radich SRG --
                $title = ($chamber == 'house') ? 'Rep' : 'Sen';
                
                // temp fix for null middle names
                $m['middle_name'] = ($m['middle_name']==NULL) ? '' : $m['middle_name'];
                
                //Build the Data Array
                $data = array(
                    'fName' => $m['first_name'],
                    'mname' => $m['middle_name'],
                    'lName' => $m['last_name'],
                    'congID' => $m['id'],
                    'govTrackID' => $govtrack_id,
                    'congressNum' => $session,
                    'state' => $m['state'],
                    'party' => $m['party'],
                    'district' => $district,
                    'chamber' => $chamber,
                    'title' => $title,
                    'image_path' => $image_path
                );
                
                $this->crud->use_table('Members');
                //Check if record exist
                $check =  $this->crud->retrieve(array('congID' => $m['id'], 'congressNum' => $session, 'chamber' => $chamber), '', 0, 0, array('id' => 'DESC'));
                if(count($check) > 0 ){
                    // ***** We should think about this. It will over wright any data modified w the CMS
                    // $this->crud->update(array('congID' => $m['id'], 'congressNum' => $session),$data, 0, 0, array('id' => 'DESC'));
                }else{
                    $this->crud->create($data);
                }
                
                $this->doDebug($m['id'].'<br/>', 'echo');
           }    
        }
        
        // Add system Logging Paul Radich SRG --
        $this->logevents->logEvent('Members Synced', $chamber);
    }
    
    private function buildNYPath($chamber, $congress){
        return $this->nyApiLocation.$congress."/".$chamber."/members.json?api-key=".$this->config->item('NY_TIMES_API_KEY');
    }
    
    // Added 9/16/13 Paul Radich SRG--
    // Checks if a given url returns a 404
    public function checkImgUrl($url){
        // Open $url
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        
        //Get the HTML for $url. 
        $response = curl_exec($handle);
        
        // Check for 404 (file not found). 
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if($httpCode == 404) {
             return false;
        }else{
            return true;
        }
        curl_close($handle);
    }
    
    //Added 9/16/13 to help debug but easily turn off echos for performance Paul Radich SRG --
    private function doDebug($message, $type){
        if($this->debug){
            if($type == 'echo'){
                echo $message;
            }else{
                print_r($message);
            }
        }
    }
    
    //Added 9/16/13 to post to heritageaction.com/update-member-scores Paul Radich SRG --
    public function callHeritageAction(){
        $url = 'http://heritageaction.com/update-member-scores/ ';
        
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        
        $response = curl_exec( $ch );
        curl_close($ch);
        $this->logevents->logEvent('heritageaction.com/update-member-scores', $response);
    }
}