<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class heritage_data extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        // $this->load->model('legislation');
        // $this->legislation->syncLegislation();
        $this->cat = "heritage_data";
    }
    
    function index() {
        
        $this->template->title('Data Sync')->build('datasync');
    }
    
   
    
    function syncMem(){
        $this->load->model('members');
        $this->backupDB();
        $this->members->syncMembers('house');        
        $this->members->syncMembers('senate'); 
        $this->callback();
        return 'good';
    }
    
    function calcScores(){
        $this->load->model('scores');
        $this->scores->calcScores();
        return 'good';
    }
    
    function downloadMembers(){
        $this->load->model('download_members');
        $chamber = $this->uri->segment(3);
        return $this->download_members->download($chamber);
    }
    
    public function fixdata(){
        $this->load->model('dbfix');
        $this->dbfix->fixvotes();
    }
    
    public function fixImages(){
        $this->load->model('dbfix');
        $this->load->model('members');
        $this->dbfix->fixImages();
    }
    
    public function backupDB(){
        $this->load->model('backupdb');
        $file = $this->backupdb->backup();
        if( $this->configs->get('emailbackups') == 'true' ){
            $this->cmsemail->sendMail($this->configs->get('dbemailaddress'),'','Scorecard Database Backup','The HA Scorecard DB has been backed up','', $file);
        }
        $date = date('m-j-y');
        $this->logevents->logEvent('DB Backup Created', $date);
    }
    
    public function callabck(){
        $this->load->model('members');
        $this->members->callHeritageAction();
    }
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        $parent = "Data Sync";
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $parent, 'Data Sync', 'Sync Data', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    
}