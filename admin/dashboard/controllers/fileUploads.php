<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class fileUploads extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
		
    }
	
	function insertFile (){
		$cat = $this->session->userdata('section');
		//copy($_FILES['file']['tmp_name'], './files/'.$_FILES['file']['name']);
		$file_element_name = 'file';		
		$config['upload_path'] = './files/';
		$config['allowed_types'] = 'eps|pdf|doc|txt|docx|jpg|jpeg|gif|ai|xls|xlsx|png';
		$config['max_size']	= '30000';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);
		$this->upload->do_upload($file_element_name);

		// $array = array(
		// 'filelink' => '/files/'.$_FILES['file']['name'],
		// 'filename' => $_FILES['file']['name']
		// );
// 		  
		//print_r($this->upload->data());
		$data = $this->upload->data();
		$fileName =  $data['client_name'];
		$fullPath = $data['full_path'];
		
		$fileName = str_replace(" ", "_", $fileName);
		
		$this->crud->use_table('CMS_files');
		$this->crud->create(array('file'=>$fileName, 'section' => $cat));
		
		$url = '/admin/files/'.$fileName;
		
		$ouput = '<a href="'.$url.'">'.$fileName.'</a>';
		echo $ouput;   
		// echo stripslashes(json_encode($array));	
		
		$array = array(
			'filelink' => $url,
			'filename' => $fileName
		);
		
		echo stripslashes(json_encode($array));
	}
	
	function insertImage (){
		$cat = $this->session->userdata('section');
		$_FILES['file']['type'] = strtolower($_FILES['file']['type']);
 
		if ($_FILES['file']['type'] == 'image/png' 
		|| $_FILES['file']['type'] == 'image/jpg' 
		|| $_FILES['file']['type'] == 'image/gif' 
		|| $_FILES['file']['type'] == 'image/jpeg'
		|| $_FILES['file']['type'] == 'image/pjpeg')
		{
			$base = base_url()."admin/";	
			$ext = str_replace("image/", "",$_FILES['file']['type']);

		    // setting file's mysterious name
		    $file = './uploads/'.md5(date('YmdHis')).'.'.$ext;
		
		    // copying
		    copy($_FILES['file']['tmp_name'], $file);
			
			$nf = md5(date('YmdHis')).'.'.$ext;
			ini_set("gd.jpeg_ignore_warning", 1);
			
			if( $_FILES['file']['type'] == 'image/jpg' || $_FILES['file']['type'] == 'image/jpeg') {  
	       	    $im = imagecreatefromjpeg($base.'uploads/'.$nf);  
		    } else if ($_FILES['file']['type'] == 'image/gif' ) {  
		        $im = imagecreatefromgif($base.'uploads/'.$nf);  
		    } else if ($_FILES['file']['type'] == 'image/png' ) {  
		        $im = imagecreatefrompng($base.'uploads/'.$nf);  
		    }
			
			$upload_image = $base.'uploads/'.$nf;
			$thumbnail = './uploads/'.md5(date('YmdHis')).'thumb.'.$ext;
			$dbThumbnail = md5(date('YmdHis')).'thumb.'.$ext;
			// Get new sizes
			list($width, $height) = getimagesize($upload_image);
			$newwidth = 150; // This can be a set value or a percentage of original size ($width)
			$newheight = 150; // This can be a set value or a percentage of original size ($height)
			
			$min_side = min($width, $height);
			
			
			// Load the images
			$thumb = imagecreatetruecolor($newwidth, $newheight);
			$source = $im;
			
			
			
			// Resize the $thumb image.
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $min_side, $min_side);
			
			// Save the new file to the location specified by $thumbnail
			imagejpeg($thumb, $thumbnail, 100);
			$this->crud->use_table('CMS_images');
			$this->crud->create(array('image'=>$nf, 'thumb' => $dbThumbnail, 'section' => $cat));
			 
		    // displaying file
		    //echo '<img src="'.$base.'uploads/'.$nf.'" />';
			
			// displaying file    
			$array = array(
				'filelink' => $base.'uploads/'.$nf
			);
			
			echo stripslashes(json_encode($array)); 
		}
	}

	function imagejson(){
		$cat = $this->session->userdata('section');
		$this->crud->use_table('CMS_images');
		$results = $this->crud->retrieve(array('section' => $cat), '', 0, 0, array('id' => 'DESC'));
		
		$images = array();
		$i = 0;
		foreach($results as $result){
			$images[$i]['image'] = "/admin/uploads/".$result->image;	
			$images[$i]['thumb'] = "/admin/uploads/".$result->thumb;
			$i++;
		}
		
		echo json_encode($images);
	}
	
	function filejson(){
		$cat = $this->session->userdata('section');
		$this->crud->use_table('CMS_files');
		$results = $this->crud->retrieve(array('section' => $cat), '', 0, 0, array('id' => 'DESC'));
		
		$files = array();
		$i = 0;
		foreach($results as $result){
			$files[$i]['file'] = $result->file;
			$files[$i]['link'] = "/admin/files/".$result->file;
			$i++;
		}
		
		echo json_encode($files);
	}
	
}