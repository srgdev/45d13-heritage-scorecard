<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class settings extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function index(){
		
		$data = $this->configs->getAll();
		$this->template->set('values', $data);
		
		$this->template->title('Settings')->build('pages/settings');
	}
	
	public function update(){
		foreach($_POST as $key => $value){
			if($key != "mysubmit"){
				$this->configs->set($key, $value);
			}
		}
		$this->session->set_flashdata('flash', 'Settings Saved'); 
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}
	
	
	
}
	
