<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class menus extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
    }
	
	function addPage(){
		$module = $_REQUEST['cat'];
		$pageID = $_REQUEST['page'];
		$parent = $_REQUEST['parent'];
		$this->crud->use_table('CMS_pages');
		$page =  $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC'));
		
		$this->crud->use_table('CMS_menus');
		$this->crud->create(array('module' => $module, 'name' => $page->title, 'link' => $page->slug, 'parent' => $parent, 'isParent' => 'false', 'isChild' => 'true', 'published' => 'false'));
		
		setMessage('info', 'Link '.$page->title.' Added to Menu');
		$this->logevents->logEvent('Link Added to Menu', 'Menu Item '.$page->title.' Added');
		
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}
	
	function external(){
		$module = $_REQUEST['cat'];
		$title = $_REQUEST['name'];
		$link = $_REQUEST['link'];
		$parent = $_REQUEST['parent'];
		
		if (!preg_match("~^(?:f|ht)tps?://~i", $link)) {
			if($link != "#"){
       		 	$link = "http://" . $link;
			}
   		 }
		
		$this->crud->use_table('CMS_menus');
		$this->crud->create(array('module' => $module, 'name' => $title, 'link' => $link, 'parent' => $parent, 'isParent' => 'false', 'isChild' => 'true', 'published' => 'false'));
		
		setMessage('info', 'External Link '.$title.' Added');
		$this->logevents->logEvent('External Link Added', 'Menu Item '.$title.' Added');
		
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}

	function addParent(){
		$module = $_REQUEST['cat'];
		$title = $_REQUEST['title'];
		
		$this->crud->use_table('CMS_menus');
		$this->crud->create(array('module' => $module, 'name' => $title, 'link' => '#', 'isParent' => 'true', 'isChild' => 'false', 'published' => 'false'));
		
		setMessage('info', 'Menu Parent '.$title.' Added');
		$this->logevents->logEvent('Menu Parent Added', 'Menu Parent '.$title.' Added');
		
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}

	function edit(){
		$name = $_REQUEST['modalName'];
		$id =$_REQUEST['id'];
		$this->crud->use_table('CMS_menus');
		$this->crud->update(array('id' => $id),array('name' => $name), 0, 0, array('id' => 'DESC'));
		setMessage('info', 'Menu Item Updated');
		$this->logevents->logEvent('Menu Item Updated', 'Menu Item '.$name.' Updated');
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}

	function delete(){
		$id = $this->uri->segment(3);
		$name = $this->uri->segment(4);
		$this->crud->use_table('CMS_menus');
		$this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
		setMessage('info', 'Menu Item '.$name.' Deleted');
		$this->logevents->logEvent('Menu Item Deleted', 'Menu Item '.$name.' Deleted');
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}
	
	public function saveMenu(){
		$this->crud->use_table('CMS_menus');
		$data =  json_decode(stripslashes($_REQUEST['data']), true);
		for($i = 0; $i < count($data); $i++){
			//echo "here";
			$order = $i + 1;
			$id = $data[$i]['id'];
			$elem = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
			$isParent = $elem->isParent;
			echo $isParent;
			if (array_key_exists('children', $data[$i])) {
				
				for($j = 0; $j < count($data[$i]['children']); $j++){
						$childID = 	$data[$i]['children'][$j]['id'];
						$childOrder = $j + 1; 
				     	echo $childOrder;
						$this->crud->update(array('id' => $childID),array('order' => $childOrder, 'parent' => $id, 'isChild' => 'true', 'isParent' => 'false'), 0, 0, array('id' => 'DESC'));
				}
				if($isParent == "true"){
					$this->crud->update(array('id' => $id),array('order' => $order, 'parent' => '', 'isParent' => 'true', 'isChild' => 'false'), 0, 0, array('id' => 'DESC'));
				}
			}else{
				if($isParent == "true"){
					$this->crud->update(array('id' => $id),array('order' => $order, 'parent' => '', 'isParent' => 'true', 'isChild' => 'false'), 0, 0, array('id' => 'DESC'));
				}	
			}
			
			
		}
	}
	
	
}