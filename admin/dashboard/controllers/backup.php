<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class backup extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $this->load->model('backupdb');
        $file = $this->backupdb->backup();
        if( $this->configs->get('emailbackups') == 'true' ){
            $this->cmsemail->sendMail($this->configs->get('dbemailaddress'),'','Scorecard Database Backup','The HA Scorecard DB has been backed up','', $file);
        }
        $date = date('m-j-y');
        $this->logevents->logEvent('DB Backup Created', $date);

    }
    
    
}