<?php
	$level = $this->auth->getLevel();
	$acg = $this->auth->getACG();
	$acg = str_replace(' ', '', $acg);
	$acg = explode(',',$acg);
	$date = date('Y-m-d');
?>

	
	<?php echo getMessage();  ?>
    <?php if($level == 0 || $level == 1) {?>
		<div class="cmsBlock">
		
			<h3>Recent System Events</h3>
			<div class="innerblock">
			<table  class="table table-striped">
				<thead>
				<tr>
					<th>Username</th>
					<th>Event</th>
					<th>Date/Time</th>
				</tr>
				</thead>
				<?php
				foreach($logData as $logLine){ ?> 
					<tr class="popParent"  >
						<td><?php echo $logLine->user ?></td>
						<td class="pop" data-content="<?php echo $logLine->desc ?>" rel="popover"><?php echo $logLine->action ?></td>
						<td><?php echo date_to_human($logLine->date) ?></td>
					</tr>	
				<?php } ?>
			
			</table>
		</div></div>
	<?php } ?>
