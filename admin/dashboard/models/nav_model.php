<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Nav Model 
 * 
 * @package Nav Model   for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
 
class Nav_model extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
		$this->crud->use_table('CMS_navigation');
    }
	
  function makeNav(){
  	$this->db->group_by("parent"); 
	$this->db->select('parent,acl,acg');
	$query = $this->db->get('CMS_navigation');
	$nav = $this->get_sub_nav($query->result());
	return $nav;
  }
  
  function get_sub_nav($parents){
  	$nav = "";
  	$i = 0;
	foreach($parents as $parent){
		$build = FALSE;
		$query = $this->db->get_where('CMS_navigation', array('parent' => $parent->parent));
		$children = $query->result();
		foreach($children as $child){
			if($this->auth->checkLevel($child->acl)){
				if($this->auth->checkACG($child->acg)){
					$build = TRUE;	
				}
			}
		}
		
		if($build){
				$nav[$i]['parent'] = $parent->parent;
				$j = 0;
				foreach($children as $child){
					if($this->auth->checkLevel($child->acl)){
						if($this->auth->checkACG($child->acg)){
							$nav[$i]['children'][$j]['title'] = $child->title;
							$nav[$i]['children'][$j]['helpText'] = $child->helpText;
							$nav[$i]['children'][$j]['link'] = $child->link;
							$nav[$i]['children'][$j]['icon'] = $child->icon;
							$nav[$i]['children'][$j]['uri'] = $child->uri;
							$nav[$i]['children'][$j]['active'] = $child->active;
							$j++;
						}
					}
				}
			$i++;
		}
	}
	
	return $nav;
  }
}
?>