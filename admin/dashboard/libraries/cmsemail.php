<?php

class cmsemail {


	function cmsemail(){
		$this->CI =& get_instance();
		$this->CI->load->library('email');
		$this->CI->load->library('configs');
	}
	
	
	function sendMail($to = '', $cc = '', $subject = '', $message = '', $from = "", $attach = '') {
		$config['mailtype'] = 'html';

        
		ini_set("SMTP","mail.capitolfaces.com");
		
        
        $this->CI->email->initialize($config);
        
        if($from == ""){
		  $this->CI->email->from($this->CI->configs->get('mes_from'));
        }else{
            $this->CI->email->from($from);
        }
		$this->CI->email->to($to);
		$this->CI->email->cc($cc);
		//$this->email->bcc('them@their-example.com');
		
		$this->CI->email->subject($subject);
        $data['message'] = $message;
		$this->CI->email->message($this->CI->load->view('email/email', $data, true));
		
        if($attach){
            $this->CI->email->attach($attach);
        }
        
		$this->CI->email->send();
		
		//echo $this->CI->email->print_debugger();
	}
	
	
}
