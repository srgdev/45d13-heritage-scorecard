<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* Excel library for Code Igniter applications
* Author: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
*/

class Toexcel {
    
    
    
    function Toexcel(){
        $this->CI =& get_instance();
        
    }
    
    
      
        
function to_excel($table, $filename='exceloutput', $title='exceloutput', $rows = '')
{
     
     
     $this->CI->crud->use_table($table);
     if($rows == ''){
        $query  = $this->CI->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
     }else{
        $query = $rows;
     }
     $headers = ''; // just creating the var for field headers to append to below
     $data = ''; // just creating the var for field data to append to below
     
     $obj =& get_instance();
     
     $fields = $this->CI->crud->getFieldList();
     if (count($query) == 0) {
          echo '<p>The table appears to have no data.</p>';
     } else {
          foreach ($fields as $key => $val) {
             $headers .= $val . "\t";
          }
     
          foreach ($query as $row) {
               $line = '';
               foreach($row as $value) {                                            
                    if ((!isset($value)) OR ($value == "")) {
                         $value = "\t";
                    } else {
                         // $value = str_replace('"', '""', $value);
                          $value = $value . "\t";
                    }
                    $line .= $value;
               }
               $data .= trim($line)."\n";
          }
          
          $data = str_replace("\r","",$data);
                         
          header("Content-type: application/x-msdownload");
          header("Content-Disposition: attachment; filename=$filename.xls");
          echo "$headers\n$data";  
     }
}
}

?>