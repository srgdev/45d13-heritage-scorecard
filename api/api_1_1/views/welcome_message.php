<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Heritage Action Scorecard API</title>

<style type="text/css">

body {
 background-color: #B1C0C9;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #666666;
 font-size: 18px;
 text-align:center;
 border-bottom:1px solid #EEEEEE;
 padding-bottom:8px
}

.apibox {
    background: #fff;
    width:350px;
    margin:200px auto;
    text-align:center;
    padding:20px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: 0px 0px 4px #000;

-moz-box-shadow: 0px 0px 4px #D3D3D3;

box-shadow: 0px 0px 9px #7b7b7b;
}

input[type='text'] {
    background:#F5F5F5;
    border:0;
    margin-bottom:10px;
    width:70%;
    border:1px solid #CCCCCC;
    padding:8px;
    font-size:14px;
}

input[type='submit'] {
    border:0;
    padding:5px; font-size:14px;
    background-color:#007FCE;
    color:#fff;
    cursor:pointer
}

</style>
</head>
<body>
    
<div class="apibox">  
<h1>Heritage Action Scorecard API Documentation</h1>
    <? echo form_open('welcome/login'); ?>
    <!-- <input type="text" name="email" placeholder="Email Address" /> <br /> -->
    <input type="text" name="key" placeholder="API KEY" /> <br />
    <input type="submit" name="submit" value="Login" />
</div>
</body>
</html>