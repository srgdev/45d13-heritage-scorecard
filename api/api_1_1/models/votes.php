<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class votes extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
  
  
  public function addMembers($results, $vote){
      $this->crud->use_table('votes');
      $query = $this->db->query('SELECT votes.vote, votes.member, Members.fName, Members.lName, Members.state, Members.score, Members.voteScore, Members.SponsorScore, Members.district
                    FROM votes, Members  
                    WHERE votes.vote_id = "'.$vote.'"
                    AND Members.congID = votes.member
            ');
      $members = $query->result();
      $results['members'] = $members;
      return $results;
  }
  
}