<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Members API
 *
 * 
 *
 * @package     AFP
 * @subpackage  Rest Server
 * @category    Controller
 * @author      Paul Radich
*/
require APPPATH.'/libraries/REST_Controller.php';


class scorecard extends REST_Controller {
    
    function members_get(){
        $this->checkKey();
        if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
        $data = array();
        $data['congressNum'] = $this->get('congress');
        $this->crud->use_table('Members');
        if($this->get('zip')){
            $results = $this->members->findByZip($this->get('zip'), $this->get('congress'));
        }else{
            if($this->get('id')) { $data['congID'] = $this->get('id'); }
            if($this->get('first')){$data['fName'] = $this->get('first');}
            if($this->get('last')){$data['lName'] = $this->get('last');}
            if($this->get('state')){$data['state'] = $this->get('state');}
            if($this->get('party')){$data['party'] = $this->get('party');}
            if($this->get('chamber')){$data['chamber'] = $this->get('chamber');}
            $orderBy = ($this->get('orderBy')) ? $this->get('orderBy') : 'lName';
            $order = ($this->get('order')) ? $this->get('order') : 'ASC';
            $results = $this->crud->retrieve($data,'', 0, 0, array($orderBy => $order));
        }
        
        if(count($results) == 1){
            $chamber =  $results[0]->chamber;
            $party =  $results[0]->party;
            $results[0]->party_average = $this->members->partyAverage($chamber, $party, $this->get('congress'));
            $results[0]->chamber_average = $this->members->chamberAverage($chamber, $this->get('congress'));
        }
        
        
        if(count($results) > 0){
            $this->response($results, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'No Members found for congress and criteria '), 404);
        }
    }
     
    
    function votes_get(){
        $this->checkKey();
        if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
        $data = array();
        $data['session'] = $this->get('congress');
        $this->crud->use_table('voteTable');
        
        //Added in v 1.2
        if($this->get('chamber')){
            $data['chamber'] = $this->get('chamber');
        }
        
        $orderBy = ($this->get('orderBy')) ? $this->get('orderBy') : 'id';
        $order = ($this->get('order')) ? $this->get('order') : 'ASC';
        
        if($this->get('vote') == 'all'){
            $result = $this->crud->retrieve($data,'', 0, 0, array($orderBy => $order));
        }else{
            $data['roll_id'] = $this->get('vote');
            $result = $this->crud->retrieve($data,'', 0, 0, array($orderBy => $order));
        }
        
        //Added in v 1.2
        if($this->get('membervotes') == 'true' || $this->get('vote') != 'all'){
            $results = $this->votes->addMembers($result);
        }else{
            $results = $result;
        }
        // print_r($results);
        if(count($results) > 0){
            $this->response($results, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'No votes found for congress and criteria '), 404);
        }
    }
    
    
    function bills_get(){
        $this->checkKey();
        if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
        $data = array();
        $data['session'] = $this->get('congress');
        $this->crud->use_table('bills');
        
        $orderBy = ($this->get('orderBy')) ? $this->get('orderBy') : 'id';
        $order = ($this->get('order')) ? $this->get('order') : 'ASC';
        
        if($this->get('bill') == 'all'){
            $results = $this->crud->retrieve($data,'', 0, 0, array($orderBy => $order));
        }else{
            $data['bill_id'] = $this->get('bill');
            $result = $this->crud->retrieve($data,'', 0, 0, array($orderBy => $order));
            $results = $this->bills->addMembers($result, $this->get('bill'));
        }
        
        if(count($results) > 0){
            $this->response($results, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'No votes found for congress and criteria '), 404);
        }
    }  
        
    function checkKey(){
        // if(!$this->get('apikey')){
            // $this->response(array('error' => 'Missing or invalid API Key'), 404);
        // }else{
            // $key = $this->get('apikey');
            // $this->crud->use_table('api_keys');
            // $query = $this->crud->retrieve(array('key' => $key), 'row', 0, 0, array('id' => 'DESC'));
            // if(count($query) == 0){
                // $this->response(array('error' => 'Missing or invalid API Key'), 404);
            // }
        // }
   } 
            
}
    