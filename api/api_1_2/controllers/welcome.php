<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->helper('url');
        if($this->session->userdata('goodkey') == 'yes'){
             redirect('/welcome/docs', 'location'); 
        }else{
		  $this->load->view('welcome_message');
        }
	}
    
    function login(){
        $key = $_REQUEST['key'];
        $this->crud->use_table('api_keys');
        $check = $this->crud->retrieve(array('key' => $key), 'row', 0, 0, array('id' => 'DESC'));
        if(count($check) > 0){
            $this->session->set_userdata('goodkey', 'yes');
            redirect('/welcome/docs', 'location'); 
        }else{
            redirect('/welcome/', 'location'); 
        } 
    }
    
    public function docs(){
        if($this->session->userdata('goodkey') == 'yes'){
             $this->load->view('api_docs'); 
        }else{
             redirect('/welcome', 'location');
        }
        
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */