<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
        
        $this->slApiLocation = "http://services.sunlightlabs.com/api/legislators.get?apikey=";
        $this->slZipApiLocation = 'http://services.sunlightlabs.com/api/districts.getDistrictsFromZip?apikey=';
    }
        
    public function findByZip($zip, $congress){
        $results = file_get_contents($this->config->item('sunlight_base').'legislators/locate?apikey='.$this->config->item('SUNLIGHT_LABS_API_KEY')."&zip=".$zip);
        $results = json_decode($results, true);
        $data = $results;
        $rows = array();
        $rows['results'] = "";
        $rows['zip'] = $zip;
        //$rows['call'] = $results;
        $states = array();
        $districts = array();
        $this->crud->use_table('Members');
        $i=0;
        foreach ($results['results'] as $r){
                
            $id = $r['bioguide_id'];
            // $rows['results'][$i]['member'] = $id;
            
            if(isset($r['district']) ){
                array_push($districts, $r['district']);
                array_push($states, $r['state']);
            }
            $i++;
        }
        
        //Loop each return for house district and state
        $i = 0;
        foreach($districts as $d) {
            $houseSearch = array(
                'state' => $states[0],
                'district' => $d,
                'congressNum' => $congress,
                'chamber' => 'house'
            ); 
            $this->crud->use_table('Members');
            $houseUsers = $this->crud->retrieve($houseSearch, '', 0, 0, array('id' => 'DESC'));
            foreach($houseUsers as $member){
               $memID = $member->congID;
               $rows['results'][$i]['member'] = $member;
               $i++;
           }

            //add the state to array
            // if(!in_array($d['district']['state'], $states)){array_push($states, $d['district']['state']);}
        }
        
       $stateT = array();
        //Loop states array and add the senate
        foreach($states as $state){
            if(!in_array($state, $stateT)){
                array_push($stateT, $state);
                $senateSearch = array(
                    'state' => $state,
                    'congressNum' => $congress,
                    'chamber' => 'senate'
                );
                $this->crud->use_table('Members');
                $senateUsers = $this->crud->retrieve($senateSearch, '', 0, 0, array('id' => 'DESC'));
               foreach($senateUsers as $member){
                   $memID = $member->congID;
                   $rows['results'][$i]['member'] = $member;
                   $i++;
               }
            }
       }
       
        
        return $rows;
    }  

    function partyAverage($chamber, $party, $congress){
        $this->crud->use_table('Members');
        $scores =  $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => $congress, 'party' => $party), '', 0, 0, array('id' => 'DESC'));
        // echo $this->runAverage($scores);
        return $this->runAverage($scores);
    }
    
    public function chamberAverage($chamber, $congress){
        $scores =  $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => $congress), '', 0, 0, array('id' => 'DESC'));
        return $this->runAverage($scores);
    }
    
    public function runAverage($scores){
        $total = count($scores);
        $score = 0;
        foreach($scores as $s){
            $score = $score + $s->score;
        }
        return round($score / $total);
    }
}
    