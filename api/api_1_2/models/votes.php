<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class votes extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
  
  //Changed in v 1.2
  public function addMembers($results){
      $this->crud->use_table('votes');
      $new_results = array();
      $i = 0;
      foreach($results as $result){
          $vote = $result->roll_id;
          $congress = $result->session;
          $query = $this->db->query('SELECT votes.vote, votes.member, Members.fName, Members.lName, Members.state, Members.score, Members.voteScore, Members.SponsorScore, Members.district
                        FROM votes, Members  
                        WHERE votes.vote_id = "'.$vote.'"
                        AND Members.congressNum = "'.$congress.'"
                        AND Members.congID = votes.member
                ');
          $members = $query->result();
          $result->members = $members;
          array_push($new_results, $result);
      }
      // print_r($new_results);
      return $new_results;
  }
  
}