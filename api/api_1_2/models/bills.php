<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class bills extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
  
  
  public function addMembers($results, $bill){
      $this->crud->use_table('sponsors');
      $query = $this->db->query('SELECT Members.congID ,Members.fName, Members.lName, Members.state, Members.score, Members.voteScore, Members.SponsorScore, Members.district
                    FROM  sponsors, Members  
                    WHERE sponsors.bill_id = "'.$bill.'"
                    AND sponsors.member = Members.congID
            ');
      $members = $query->result();
      $results['sponsors'] = $members;
      return $results;
  }
  
}