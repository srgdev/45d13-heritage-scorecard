<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Heritage Action Scorecard API V 1_2</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 24px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

h2 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 20px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

th {text-align:left}
td.one {width:25%}

</style>
</head>
<body>

<h1>Heritage Action Scorecard API Version 1_2</h1>
<p>API base path: <strong><?php echo base_url(); echo index_page(); ?></strong></p>
<p>All Api request must include your API key - example: <strong>apikey/--APIKEY--</strong></p>
<p>All Api request must include a congress number - example: <strong>congress/112</strong></p>
<br /><br />
Format: Results can be returned in three formats <br />
format/xml
<br /> format/json
<br /> format/jsonp
<br />

<h2>Members</h2>
<p><strong>URL:</strong> <?php echo base_url(); echo index_page(); ?>scorecard/members/</p>
<table width="100%">
	<tr>
		<th class="one">Search Method</th>
		<th>Description</th>
	</tr>
	<tr>
		<td class="one">zip</td>
		<td>A zip code search will only return zip searches no other search method will be read</td>
	</tr>
	<tr>
        <td class="one">id</td>
        <td>Bioguide ID search</td>
    </tr>
	<tr>
		<td class="one">first</td>
		<td>First name search</td>
	</tr>
	<tr>
		<td class="one">last</td>
		<td>Last name search</td>
	</tr>
	<tr>
		<td class="one">state</td>
		<td>State search. value passed as 2 letter abbrv.</td>
	</tr>
	<tr>
		<td class="one">party</td>
		<td>Party search. value passed as 1 letter abbrv</td>
	</tr>
	<tr>
		<td class="one">chamber</td>
		<td>chamber search</td>
	</tr>
</table>
<br /><br />
Results can be ordered by any field in the return. These are just some examples
<table width="100%">
    <tr>
        <th>Order By</th>
        <th>Method</th>
    </tr>
    <tr>
        <td>Last Name</td>
        <td>lName</td>
    </tr>
    <tr>
        <td>Score</td>
        <td>score</td>
    </tr>
    <tr>
        <td>State</td>
        <td>state</td>
    </tr>
    <tr>
        <td>Party</td>
        <td>party</td>
    </tr>
    <tr>
        <td>Chamber</td>
        <td>chamber</td>
    </tr>
</table>

<br /><br />
<table width="100%">
    <tr>
        <th>Order</th>
        <th>Method</th>
    </tr>
    <tr>
        <td>Results Ascending</td>
        <td>ASC</td>
    </tr>
    <tr>
        <td>Results Descending</td>
        <td>DESC</td>
    </tr>
   
</table>
<p>Example:All Republicans from CA ordered by last name</p>
<strong><?php echo base_url(); echo index_page(); ?>scorecard/members/congress/112/state/ca/party/r/orderBy/lName/apikey/--APIKEY--</strong>


<h2>Key Votes</h2>
<p><strong>URL:</strong> <?php echo base_url(); echo index_page(); ?>scorecard/votes/</p>

The Vote method has two options <br />
votes/all <br />
votes/--roll_call--
<br /><br />
<table width="100%">
    <tr>
        <th class="one">Vote Method</th>
        <th>Description</th>
    </tr>
    <tr>
        <td class="one">all</td>
        <td>Returns all Heritage assigned key votes for the given congress</td>
    </tr>
    <tr>
        <td class="one">Roll call #</td>
        <td>Returns the vote and how each member voted</td>
    </tr>
    
   
</table>
<br />
<table width="100%">
    <tr>
        <th class="one">Search Method</th>
        <th>Description</th>
    </tr>
    <tr>
        <td class="one">chamber</td>
        <td>chamber search</td>
    </tr>
    
</table>


<h3>Member Votes (New for all Members in V:1_2)</h3>
<p>Note: Member votes are always returned for individual votes</p>
<p><strong>WARNING: RETURNING MEMBER VOTES FOR ALL RESULTS (EVEN BY CHAMBER) IS A LONG CALL. ONLY USE IF YOU ARE CACHING RESULTS</strong></p>
<table width="100%">
    <tr>
        <th class="one">Vote Method</th>
        <th>Description</th>
    </tr>
    <tr>
        <td class="one">membervotes</td>
        <td> true : returns member votes for each vote</td>
    </tr>
   
</table>




<p>Example: Results for vote s2-2011</p>
<strong><?php echo base_url(); echo index_page(); ?>scorecard/votes/vote/s2-2011/congress/112/apikey/--APIKEY--</strong>


<h2>Bills ( Co-sponsorships )</h2>
<p><strong>URL:</strong> <?php echo base_url(); echo index_page(); ?>scorecard/bills/</p>

The bill method has two options <br />
bill/all <br />
bill/--bill#--
<br />
<table width="100%">
    <tr>
        <th class="one">bill Method</th>
        <th>Description</th>
    </tr>
    <tr>
        <td class="one">all</td>
        <td>Returns all Heritage assigned bills for the given congress</td>
    </tr>
    <tr>
        <td class="one">Bill #</td>
        <td>Returns the bill and Co-sponsorships</td>
    </tr>
   
</table>

<p>Example: Results for bill s827-112</p>
<strong><?php echo base_url(); echo index_page(); ?>bills/bill/s827-112/congress/112/apikey/--APIKEY--</strong>

</body>
</html>