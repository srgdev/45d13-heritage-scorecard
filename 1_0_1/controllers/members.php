<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    
    public function index(){
        $s = (isset($_REQUEST['c'])) ? $_REQUEST['c'] : 'all';
        $this->template->set('s', $s);
        $p = (isset($_REQUEST['p'])) ? $_REQUEST['p'] : 'all';
        $this->template->set('p', $p); 
        $this->membersmod->allMembers();
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/overall');
        $this->template->build('templates/overall');
    }
    
    public function member(){
        $id = $this->uri->segment(3);
        $this->membersmod->getMember($id);
        $this->template->set_partial('body', 'templates/member');
        $this->template->build('templates/member');
    }
    
    
    public function search(){
        $method = $this->uri->segment(3);
        
        if($method == 'zip'){
           $param = $_REQUEST['search-zip'];
           $results = $this->membersmod->searchZip($param);
           $term = $results['zip']; 
           $results = $results['results'];           
        }
        
        if($method == 'name'){
           $param = $_REQUEST['search-name'];
           $results = $this->membersmod->searchName($param); 
           $term = $param;   
        }
        
        $this->template->set('body_class', 'results');
        $this->template->set('term', $term);
        $this->template->set('results', $results);
        $this->template->set_partial('body', 'templates/search');
        $this->template->build('templates/search');
    }
    
    public function howScores(){
       $this->crud->use_table('CMS_pages');
       $page = $this->crud->retrieve(array('id' => '2'), 'row', 0, 0, array('id' => 'DESC'));
       $data['content'] = $page->content;
       $view = $this->load->view('templates/howScoresAreCalc', $data, true); 
       $result['returns'] = 'success';
       $result['data']  = $view;
       echo json_encode($result); 
    }
}