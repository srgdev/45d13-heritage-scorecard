<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class embed extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
    }
   
    public function index(){
        $member = $_REQUEST['member'];
        $congress = $_REQUEST['congress'];
        $this->crud->use_table('Members');
        $query = $this->crud->retrieve(array('congID' => $member, 'congressNum' => $congress), 'row', 0, 0, array('fName' => 'ASC'));
        $this->template->set('member', $query);
        $this->template->set_layout('embed');
        $this->template->build('templates/memEmbed');
    }
    
    public function embedMember(){
        $member = $_REQUEST['member'];
        $data['member'] = $member;
        $view = $this->load->view('templates/embedCode', $data, true);
        $result = array();
        $result['returns'] = 'success';
        $result['data']  = $view;
        echo json_encode($result);
    }
    
}
