<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class state extends CI_Controller {

	
	 
	public function index()
	{
	    $state = $this->uri->segment(3);
        if(!$state){ $state = $_REQUEST['state']; }
	    $this->membersmod->byState($state);
        $this->template->set_partial('body', 'templates/state');
	    $this->template->build('templates/state');
    }
    
  
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */