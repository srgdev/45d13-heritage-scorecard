<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class about extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $this->template->set('body_class', 'about');
        $this->membersmod->topPerformers();
        $this->template->set('hrAv', $this->scoresmod->getChamberPartyAverage('house', 'R'));
        $this->template->set('hdAv', $this->scoresmod->getChamberPartyAverage('house', 'D')); 
        $this->template->set('srAv', $this->scoresmod->getChamberPartyAverage('senate', 'R'));
        $this->template->set('sdAv', $this->scoresmod->getChamberPartyAverage('senate', 'D'));
        $this->crud->use_table('CMS_pages');
        $page = $this->crud->retrieve(array('id' => '1'), 'row', 0, 0, array('id' => 'DESC'));
        $this->template->set('page', $page); 
        $this->template->set_partial('body', 'templates/about');
        $this->template->build('templates/about');
    }
    
}