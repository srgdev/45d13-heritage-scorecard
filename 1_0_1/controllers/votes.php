<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class votes extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $s = (isset($_REQUEST['c'])) ? $_REQUEST['c'] : 'all';
        $this->template->set('s', $s);
        $this->keyvotes->all();
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/votes');
        $this->template->build('templates/votes');
    }
    
    
    public function vote(){
        $id = $this->uri->segment(3);
        $this->keyvotes->getVote($id);
        $this->template->set('body_class', 'search');
        $this->template->set_partial('body', 'templates/vote');
        $this->template->build('templates/vote');
    }
    
}