<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	 
	public function index()
	{
	    $this->membersmod->topPerformers();
        $this->template->set('hrAv', $this->scoresmod->getChamberPartyAverage('house', 'R'));
        $this->template->set('hdAv', $this->scoresmod->getChamberPartyAverage('house', 'D')); 
        $this->template->set('srAv', $this->scoresmod->getChamberPartyAverage('senate', 'R'));
        $this->template->set('sdAv', $this->scoresmod->getChamberPartyAverage('senate', 'D'));
	    // $this->template->set_partial('header', 'templates/header');
        // $this->template->set_partial('footer', 'templates/footer');
        $this->template->set_partial('body', 'templates/index');
	    $this->template->build('templates/index');
    }
    
    public function setCongress(){
        $congress = $_REQUEST['cong'];
        $url = $_REQUEST['url'];
        if($congress != 'NONE'){
            $comparesenate = array();
            $comparehouse = array();
            $this->session->set_userdata('comparesenate' , $comparesenate);
            $this->session->set_userdata('comparehouse' , $comparehouse);
            $this->session->set_userdata('congress' , $congress);
        }
        redirect($url, 'location');
        // redirect('welcome/index/', 'location');
    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */