<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class compare extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index(){
        $uri = $this->comparemod->returnUri();
        $house = $_REQUEST['housemems'];
        $housemembers = explode("_", $house);
        $senate = $_REQUEST['senatemems'];
        $senatemembers = explode("_", $senate);
        if(count($housemembers) > 1 || count($senatemembers) <= 1){
            redirect('/compare/compareHouseVotes'.$uri, 'location');
        }else{
            redirect('/compare/compareSenateVotes'.$uri, 'location');
        }
    }
    
    public function compareHouseVotes(){
        $this->comparemod->compareHouseVotes();
        $uri = $this->comparemod->returnUri();
        $this->template->set('uri', $uri);
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/compareHouseVotes');
        $this->template->build('templates/compareHouseVotes');
    }
    
    public function compareHouseBills(){
        $this->comparemod->compareHouseBills();
        $uri = $this->comparemod->returnUri();
        $this->template->set('uri', $uri);
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/compareHouseBills');
        $this->template->build('templates/compareHouseBills');
    }
    
    public function compareSenateVotes(){
        $this->comparemod->compareSenateVotes();
        $uri = $this->comparemod->returnUri();
        $this->template->set('uri', $uri);
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/compareSenateVotes');
        $this->template->build('templates/compareSenateVotes');
    }
    
    public function compareSenateBills() {
        $this->comparemod->compareSenateBills();
        $uri = $this->comparemod->returnUri();
        $this->template->set('uri', $uri);
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/compareSenateBills');
        $this->template->build('templates/compareSenateBills');
    }
    
    public function add(){
        $member = $_REQUEST['mem'];
        $chamber = $_REQUEST['chamber'];
        //$this->session->unset_userdata('compare'.$chamber);
        $get = get_data('compare'.$chamber);
        if(!is_array($get)){
            $get = array();
        }
        $result = array();
        if(count($get) <= 3){
            if(!in_array($member, $get)){
                $compare = $get;
                array_push($compare, $member);
                $this->session->set_userdata('compare'.$chamber , $compare);
                $data['members'] = get_data('compare'.$chamber );
                $view = $this->load->view('templates/'.$chamber.'Compare', $data, true);
                $result['returns'] = 'success';
                $result['data']  = $view;
            }else{
                $result['returns'] = 'nothing';
                $result['message'] = 'Member is already added to compare list';
            }
        }else{
            $result['returns'] = 'error';
            $result['message'] = 'A max of 4 members can be compared at one time';
        }
        echo json_encode($result);
    }
   
    public function remove(){
       $member = $_REQUEST['mem'];
       $chamber = $_REQUEST['chamber'];
       $get = get_data('compare'.$chamber);
       $new = array();
       foreach($get as $g){
           if($g != $member){
               array_push($new, $g);
           }
       } 
       $this->session->set_userdata('compare'.$chamber , $new);
       $data['members'] = get_data('compare'.$chamber);
       $view = $this->load->view('templates/'.$chamber.'Compare', $data, true);
       $result['returns'] = 'success';
       $result['data']  = $view;
       $this->session->set_userdata('compare'.$chamber , $new);
       echo json_encode($result);
    }
    
    public function addForm(){
       $this->crud->use_table('Members');
       $chamber = $_REQUEST['chamber'];
       $query = $this->crud->retrieve(array('congressNum' => currentCongress(), 'chamber' => $chamber), '', 0, 0, array('fName' => 'ASC'));
       $data['chamber'] = $chamber; 
       $data['members'] = $query; 
       $data['inCompare'] = get_data('compare'.$chamber );
       $view = $this->load->view('templates/addToCompare', $data, true); 
       $result['returns'] = 'success';
       $result['data']  = $view;
       echo json_encode($result);
    }
    
    public function addMass(){
        $chamber = $_REQUEST['chamber'];
        $this->session->set_userdata('compare'.$chamber , $_REQUEST['add']);
        $from =  $_SERVER['HTTP_REFERER'];
        
        if (strpos($from,'compareHouseBills') !== false) {
            $url = $this->comparemod->returnFromUri();
            redirect('/compare/compareHouseBills'.$url, 'location');
        }elseif(strpos($from,'compareHouseVotes') !== false) {
            $url = $this->comparemod->returnFromUri();
            redirect('/compare/compareHouseVotes'.$url, 'location');
        }elseif(strpos($from,'compareSenateBills') !== false) {
            $url = $this->comparemod->returnFromUri();
            redirect('/compare/compareSenateBills'.$url, 'location');
        }elseif(strpos($from,'compareSenateVotes') !== false) {
            $url = $this->comparemod->returnFromUri();
            redirect('/compare/compareSenateVotes'.$url, 'location');
        }else{
            redirect($_SERVER['HTTP_REFERER'], 'location');
        }
        
    }
}