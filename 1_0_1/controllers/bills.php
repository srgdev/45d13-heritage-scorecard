<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bills extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    
   public function index(){
       $s = (isset($_REQUEST['c'])) ? $_REQUEST['c'] : 'all';
        $this->template->set('s', $s);
        $this->billsmod->all();
        $this->template->set('body_class', 'results');
        $this->template->set_partial('body', 'templates/bills');
        $this->template->build('templates/bills');
    }
   
    public function bill(){
        $id = $this->uri->segment(3);
        $this->billsmod->getBill($id);
        $this->template->set('body_class', 'search');
        $this->template->set_partial('body', 'templates/bill');
        $this->template->build('templates/bill');
    }
}