<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class watchlist extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
    }
    
    
    public function index(){
        if(!$this->siteauth->checkAuth()){
            redirect('/watchlist/signin/', 'location');
        }
        $this->watchlistmod->getMembers();
        $this->template->set('body_class', 'watch');
        $this->template->set_partial('body', 'templates/watchlist');
        $this->template->build('templates/watchlist');
    }
    
    
    public function signin(){
        if($this->siteauth->checkAuth()){
            redirect('/watchlist', 'location');
        }
        $this->template->set('body_class', 'watch');
        $this->template->set_partial('body', 'templates/signin');
        $this->template->build('templates/signin');
    }
    
    public function signup(){ 
        if($this->siteauth->checkAuth()){
            redirect('/watchlist', 'location');
        }
        $this->template->set('body_class', 'watch');
        $this->template->set_partial('body', 'templates/signup');
        $this->template->build('templates/signup');
    }
    
    public function recover(){
        if($this->siteauth->checkAuth()){
            redirect('/watchlist', 'location');
        }
        $this->template->set('body_class', 'watch');
        $this->template->set_partial('body', 'templates/forgot');
        $this->template->build('templates/forgot');
    }
    
    public function sendPass(){
        $email = $_POST['email'];
        $this->crud->use_table('users');
        $check =  $this->crud->retrieve(array('email' => $email), 'row', 0, 0, array('id' => 'DESC'));
        if(count($check) > 0){
            $this->load->library('emaillib');
            $subject = 'Score card Watchlist Password';
            $message = ' You have sent a request to recover your watchlist password. <br/><br/> Password: '.$check->password;
            $this->emaillib->sendMail($email , '', $subject , $message , "info@heritageactionscorecard.com");
            setMessage('info', 'Your Password was sent to '.$email); 
        }else{
            setMessage('error', 'Your Email Address Was not found'); 
        }
        redirect('/watchlist/recover', 'location');
    }
    
    public function login(){
        $email = $_POST['email'];
        $pass  = $_REQUEST['password']; 
        if($this->siteauth->login($email, $pass)){
            if(isset($_REQUEST['remember'])){
                setcookie("email",$email);
                setcookie("pass",$pass);
            }else{
                setcookie("email",'');
                setcookie("pass",'');
            }
            redirect('/watchlist', 'location');
        }else{
            setMessage('error', 'Email Address and/or Password are incorrect '.$email );
            redirect('/watchlist/signin', 'location');
        }
    }
    
    public function signout(){
        $this->siteauth->logout();
        redirect($_SERVER['HTTP_REFERER'], 'location');
    }

    public function create(){
        $email = $_REQUEST['email'];
        $pass  = $_REQUEST['password'];
        if($this->siteauth->createAccount($email, $pass)){
            if(isset($_REQUEST['remember'])){
                setcookie("email",$email);
                setcookie("pass",$pass);
            }else{
                setcookie("email",'');
                setcookie("pass",'');
            }
            redirect('/watchlist', 'location');
        }else{
            setMessage('error', 'Email Address is already in use'); 
            redirect('/watchlist/signup', 'location');
        }
    }
    
    public function remove(){
        $member = $this->uri->segment(3);
        $this->watchlistmod->remove($member);
        setMessage('info', 'Member was removed from your watchlist'); 
        redirect($_SERVER['HTTP_REFERER'], 'location');
    }
    
    public function add(){
        if(!$this->siteauth->checkAuth()){
            redirect('/watchlist/signin/', 'location');
        }
        $member = $this->uri->segment(3);
        $this->watchlistmod->add($member);
        setMessage('info', 'Member was add to your watchlist');
        redirect($_SERVER['HTTP_REFERER'], 'location');
    }
}