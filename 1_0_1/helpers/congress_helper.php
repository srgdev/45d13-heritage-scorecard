<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function currentCongress(){
    $CI =& get_instance();
    if(!get_data('congress')){
        return $CI->configs->get('sitecongress');
        // return '112';
    }else{
        return get_data('congress');
    }
}


function lastUpdated(){
    $CI =& get_instance();
    $CI->crud->use_table('updated');
    $get = $CI->crud->retrieve(array('id' => '1'), 'row', 0, 0, array('id' => 'DESC'));
    return $get->updated;
}


function isSentinel($score){
    if($score > '90' && currentCongress() == '112'){
        return true;
    }else{
        return false;
    }
}

function getBillChamber($type){
    if($type == 'hr' || $type == 'hltr'){
        return 'House';
    }else{
        return 'Senate';
    }
}
