<?php





function facebookLink($title, $summary, $url){
    $base = 'https://www.facebook.com/sharer/sharer.php?s=100&p[title]='.$title.'&p[summary]='.$summary.'&p[url]='.$url;
    echo $base;
} 


function getUrl() {
  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
  $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
  $url .= $_SERVER["REQUEST_URI"];
  return $url;
}


function buildTitle($title){
    return 'Heritage Action Scorecard - '.$title;
}

function twitterLink($text){
    $base = "https://twitter.com/intent/tweet?source=webclient&text=".$text;
    echo $base;
}

function emailLink(){
    $base = "mailto:?subject=Hold Congress Accountable with Heritage Action's Legislative Scorecard &body=With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms and those of their children and grandchildren slipping away.%0A%0AHeritage Action's Legislative Scorecard, shows which Members of Congress are saying the right things AND doing the right things. It is a revealing barometer of a lawmaker's willingness to fight for principled conservative policies in Congress.%0A%0AFind the conservative legislative scorecard here: ".getUrl()." %0A%0AHeritage Action's scorecard is tough. After all, we are conservatives, not tenured university professors, and won't grade on a curve.";
    echo $base;
}

function votesEmailLink(){
    $base = "mailto:?subject=Hold Congress Accountable with Heritage Action's Legislative Scorecard &body=With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms and those of their children and grandchildren slipping away.%0A%0AHeritage Action's Legislative Scorecard, shows which Members of Congress are saying the right things AND doing the right things. It is a revealing barometer of a lawmaker's willingness to fight for principled conservative policies in Congress.%0A%0A See the key votes here: ".getUrl()." %0A%0AHeritage Action's scorecard is tough. After all, we are conservatives, not tenured university professors, and won't grade on a curve.";
    echo $base;
}

function voteEmailLink($vote){
    $vote = str_replace("\"", "%22", $vote);
    $base = 'mailto:?subject=See How Congress Voted on Full Repeal of Obamacare&body=     With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms - and those of their children and grandchildren - slipping away.%0A%0AHeritage Action outlines the conservative position on the key votes. '.$vote.' was one critical vote.%0A%0ASee how your Members of Congress voted here: '.getUrl().' %0A%0AHeritage Action scorecard is tough. After all, we are conservatives, not tenured university professors, and wont grade on a curve.';
    echo $base;
}

function billsEmailLink(){
    $base = "mailto:?subject=Hold Congress Accountable with Heritage Action's Legislative Scorecard &body=With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms and those of their children and grandchildren slipping away.%0A%0AHeritage Action's Legislative Scorecard, shows which Members of Congress are saying the right things AND doing the right things. It is a revealing barometer of a lawmaker's willingness to fight for principled conservative policies in Congress.%0A%0A See the bills here: ".getUrl()." %0A%0AHeritage Action's scorecard is tough. After all, we are conservatives, not tenured university professors, and won't grade on a curve.";
    echo $base;
}

function billEmailLink($bill){
    $base = "mailto:?subject=Conservatives Must Support Defending America’s Affordable Energy and Jobs Act &body=     With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms - and those of their children and grandchildren - slipping away.%0A%0A The bills that appear as cosponsorships on Heritage Action's Legislative Scorecard are the bills that Members of Congress must lead on if they want to be considered conservative. ".$bill." is a key policy for conservative to advance.%0A%0ASee who co-sponsors the bill here: ".getUrl()." %0A%0AHeritage Action scorecard is tough. After all, we are conservatives, not tenured university professors, and wont grade on a curve.";
    echo $base;
}
function memberEmailLink($name, $score){
    $base = $name.' earned"%20"a"%20"'.$score.'%"%20"onvHeritage"%20"Action"%20"legislative"%20"scorecard'; 
    $body = "With every vote cast in Congress, freedom either advances or recedes. From reckless spending and stifling regulations to Obamacare, Americans see their freedoms - and those of their children and grandchildren - slipping away. \r\n Heritage Action legislative scorecard, shows which Members of Congress are saying the right things AND doing the right things. It is a revealing barometer of a lawmaker's willingness to fight for principled conservative policies in Congress.%0A%0A ".$name." earned a ".$score."%. Learn more here: ".getUrl()." %0A%0AHeritage Action scorecard is tough. After all, we are conservatives, not tenured university professors, and won't grade on a curve.";
    echo "mailto:?subject=".$base."&body=".urlencode($body);
}


function goo_gl_short_url($longUrl) {     $GoogleApiKey = 'AIzaSyBkgYcZFpjQGX9EXNmN41hsykPZi8SMmzw';     $postData = array('longUrl' => $longUrl, 'key' => $GoogleApiKey);
    $jsonData = json_encode($postData);
    $curlObj = curl_init();
    curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
    //As the API is on https, set the value for CURLOPT_SSL_VERIFYPEER to false. This will stop cURL from verifying the SSL certificate.
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlObj, CURLOPT_HEADER, 0);
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
    curl_setopt($curlObj, CURLOPT_POST, 1);
    curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
    $response = curl_exec($curlObj);
    $json = json_decode($response);
    curl_close($curlObj);
    return $json->id;
}