a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:80:"
	
	
	
	
	
	
	
	
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:12:"Peach Pundit";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"http://www.peachpundit.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:44:"Fresh Political Pickins From The Peach State";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 18:00:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:29:"http://wordpress.org/?v=3.4.2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:20:{i:0;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"Ethics A Bigger Issue Than Gift Caps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"http://www.peachpundit.com/2013/01/17/ethics-a-bigger-issue-than-gift-caps/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:84:"http://www.peachpundit.com/2013/01/17/ethics-a-bigger-issue-than-gift-caps/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 18:00:46 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50831";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:359:"Today&#8217;s Courier Herald Column: Ethics, again, will be a much talked about topic around the legislature this session.  Unlike other sessions, however, the legislature may not just limit themselves to just talk. Senators have started the ball rolling, adopting new Senate Rules that install a cap on lobbyist expenditures for any member of the body [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Charlie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4858:"<p></p><p><em>Today&#8217;s Courier Herald Column:</em></p>
<p>Ethics, again, will be a much talked about topic around the legislature this session.  Unlike other sessions, however, the legislature may not just limit themselves to just talk.</p>
<p>Senators have started the ball rolling, adopting new Senate Rules that install a cap on lobbyist expenditures for any member of the body in excess of $100.  There are, however, enough loopholes in those rules to drive a nice junket through.  It is, as they say, a start but hardly the effort Georgians and this legislature needs.</p>
<p>While the gift cap has been the rallying cry of many who have been championing ethics reform, others have fairly consistently acknowledged its window dressing role in more fundamental problems that face our system of governance and the integrity required within it for it to work properly.  A serious ethics reform package must contain two things:  Independent oversight and personal accountability of the government official.<span id="more-50831"></span></p>
<p>Georgia has long relied on a system of “transparency” to self-police ethics in government.  We currently operate under the assumption that if campaign contributions and lobbyist expenditures are disclosed, the public can see who is spending money on whom, conclude for what purpose and motive this has been done, and then take corrective action if needed.  This concept is fundamentally flawed and broken.</p>
<p>After more than two years of “transparency”, issues reported from now former Rules Chairman Don Balfour of Gwinnett’s disclosures and other public records has brought him censure from the Senate and removal of Chairmanship from the Senate’s most powerful committee.  Some will say this is an example that the system works.  Those people hope you’re not paying attention.</p>
<p>Balfour’s transgressions were reported by a former Atlanta Journal-Constitution editor Jim Walls on his website Atlanta Unfiltered, but were largely ignored by his peers and scoffed at by former Senate leadership.  Only a constant drumbeat of press, comparisons to a former state senator who served jail time for similar transgressions, and two ethics complaints filed with the Senate from private individuals forced the Senate to act.</p>
<p>It’s worth noting that under these new improved Senate rules, a private citizen is no longer allowed to file ethics complaints with the Senate.  As is often the case with ethics “reform”, the details involved in the package undercut the changes promised in the headlines of press releases.</p>
<p>Georgia cannot assure its citizens that is has ethical and open Government until the system of “self-policing” has given way to an independent body that can review complaints and administer punishment without fear of budgetary or employment retribution.  And independent grand jury process to review complaints would be a far superior check on our official’s activities than meaningless reports filed by lobbyists and campaign consultants.</p>
<p>Furthermore, any true reform must contain accountability for the actions of the elected official.  Currently, if an elected official accepts a gift from a lobbyist that is not reported, the only sanction and penalty goes to the lobbyist.  There is no incentive for the official to worry if the trip, meal, sporting event ticket or other honorarium they receive will ever see the sunlight that is supposed to make this system work.  Quite the contrary, many would prefer these events go unreported.</p>
<p>There will be attempts to continue to add reporting burdens to the lobbying community and expand the scope of the number of people who are required to register as lobbyists.  Georgians should also reject this cynical ploy to pretend that lobbyist act alone with respect to ethical transgressions.</p>
<p>Ask any of the lobbyists that play by the rules and try to work within this system and they will quickly confide that it is broken.  While many elected officials do not abuse the meals and other items afforded to them, most lobbyists have tales of inviting certain legislators to dinner, only to have that legislator invite several additional friends and relatives to join.  When all members of the party order a second meal to go, it’s clear that some do use and abuse this system.  Additional lobbyist reporting will not fix that problem.</p>
<p>The current system produces a lot of records but little accountability.  For Georgians to get the ethics reform they so desperately want and need, legislators will have to work long and hard to design a system that removes their role of self-policing, and institutes specific measures which allow themselves to be held accountable when transgressions are committed.  Only then will we have achieved true ethics reform.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:80:"http://www.peachpundit.com/2013/01/17/ethics-a-bigger-issue-than-gift-caps/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:58:"Pro-2nd Amendment Rally At The State Capitol This Saturday";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:97:"http://www.peachpundit.com/2013/01/17/pro-2nd-amendment-rally-at-the-state-capitol-this-saturday/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:106:"http://www.peachpundit.com/2013/01/17/pro-2nd-amendment-rally-at-the-state-capitol-this-saturday/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 15:09:47 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50828";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:346:"I heard about this event this morning from a co-worker.  Guns Across America is coordinating peaceful pro-2nd amendment rallies at state capitols across the nation this Saturday, and there will be a rally in Georgia.  They&#8217;ll be gathering in the plaza out in front of the Washington Street side of the Capitol this Saturday, January [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Nathan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:695:"<p></p><p>I heard about this event this morning from a co-worker.  Guns Across America is coordinating peaceful pro-2nd amendment rallies at state capitols across the nation this Saturday, and <a href="http://www.facebook.com/pages/Guns-Across-America-Georgia/374909635939429">there will be a rally in Georgia</a>.  They&#8217;ll be gathering in the plaza out in front of the Washington Street side of the Capitol this Saturday, January 19th at 1p.  Attendees are encouraged to bring pro-gun and pro-2nd amendment signs.  I&#8217;m not sure who, if anyone, will be speaking to the crowd, but I&#8217;m sure the Facebook page for the event (linked above) will provide additional details.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:102:"http://www.peachpundit.com/2013/01/17/pro-2nd-amendment-rally-at-the-state-capitol-this-saturday/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"DeKalb Schools -Time To Pull The Band-Aid";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:79:"http://www.peachpundit.com/2013/01/17/dekalb-schools-time-to-pull-the-band-aid/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:88:"http://www.peachpundit.com/2013/01/17/dekalb-schools-time-to-pull-the-band-aid/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 13:54:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50824";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:280:"There are two ways to remove a band-aid; you can pull it off slowly, bit by tortuous bit, which prolongs the agony, or you can yank it off with one sudden pull, which may hurt a bit more, but only for a second. DeKalb County Schools is in desperate need of a leader with the [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Mike Hassinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8502:"<p></p><p>There are two ways to remove a band-aid; you can pull it off slowly, bit by tortuous bit, which prolongs the agony, or you can yank it off with one sudden pull, which may hurt a bit more, but only for a second. DeKalb County Schools is in desperate need of a leader with the courage to yank a band-aid, and fast. The State Board of Education will hold a hearing on the DeKalb School Board today, and afterwards make a recommendation to the Governor about what to do next with the 9 clowns in one small car that is the DeKalb County Board of Education. Pay close attention to this meeting and the subsequent actions by the State BOE and the Governor, because what&#8217;s at stake it&#8217;s not just DeKalb County&#8217;s future, or the future of ~100,000 DeKalb school kids. What&#8217;s at stake is Georgia&#8217;s ability to deal with systemic problems in its educational system, and a real measure of Governor Deal&#8217;s leadership. <span id="more-50824"></span></p>
<p><span style="font-size: 13px;">It may seem unfair to to charge the Governor with responsibility for DeKalb County&#8217;s educational debacle -he didn&#8217;t cause any of them. In fact, most of the worst ones predate his election as Governor. But as Georgia&#8217;s Governor, Nathan Deal is the only person with sufficient authority to take decisive action and begin the process of fixing what&#8217;s broken with the 3rd largest school system in the state. And what&#8217;s broken, exactly? The former superintendent is <a href="http://www.ajc.com/news/news/local/trial-scheduled-for-former-dekalb-school-superinte/nS9ry/">currently facing trial on conspiracy charges</a>, along with the former construction program manager; the system is also paying legal fees to defend him, but not her. The system has also been <a href="http://blogs.ajc.com/get-schooled-blog/2012/05/03/dekalb-lawsuit-cost-now-at-37-million-pass-me-the-aspirin/">paying millions in legal fees in a high-stakes gamble to get construction giant Heery International</a> to settle a lawsuit related to the criminal suit, but <a href="http://championnewspaper.com/news/articles/888judge-orders-dekalb-county-schools-to-mediate-in-lawsuit888.html">DeKalb&#8217;s Judge Clarence Seeliger has refused to set a trial date in <strong>that</strong> case for nearly half a decade</a>. The District Attorney currently has<a href="http://www.wsbtv.com/news/news/local/dekalb-board-member-says-audit-shows-troubled-dist/nTfLN/"> two inquiries into possible criminal activities</a> by school administrators. <a href="http://blogs.ajc.com/get-schooled-blog/2010/08/15/dekalb-throws-the-book-at-authoreducators-selling-their-stuff-to-the-school-system/">Principals have used school funds to buy books they had written</a>. A <a href="http://www.championnewspaper.com/news/articles/780financial-problems-plague-school-board-member-780.html">deadbeat school board member sees nothing wrong with selling $20,000 worth of pizza from his restaurant to the school system</a> he&#8217;s supposed to <a href="http://dunwoodynow.com/dekalb-school-board-member-resigns-budget-chairmanship/">be responsible</a> for. A former school board member threatened to &#8220;<a href="http://www.youtube.com/watch?v=avbkYoNqxCI">slug&#8221; a reporter</a> asking questions about how her relatives got jobs with the school system. And how can we forget the <a href="http://www.peachpundit.com/2012/02/10/this-is-why-dekalb-cant-have-nice-things/">shameful public episode</a> called &#8220;redistricting&#8221; from last year? </span></p>
<p><span style="font-size: 13px;">Accreditation agency SACS warned DeKalb <a href="http://dekalbschoolwatch.files.wordpress.com/2012/09/sacs-inquiry-letter.pdf"><em>2 years ago</em></a> (pdf) to straighten up, but they didn&#8217;t. SACS put them on accreditation probation last month, and issued a <a href="http://www.nancyjester.com/media/6445/sacsspecialreport12172012.pdf">damning report</a> about the utter <a href="http://www.thecrier.net/news/article_e882fcac-4927-11e2-936f-001a4bcf887a.html">dysfunctionality of Board</a>, which cited such things as millions of dollars borrowed for textbooks that no one can find, using some of those borrowed millions to pay legal fees instead of buying textbooks and direct meddling by not only incumbent board members but by <a href="http://dekalbschoolwatch.wordpress.com/2012/07/28/the-truth-from-don-mcchesney-hes-the-real-deal/">Board members-elect</a>, who had not <a href="http://www.marshallorson.org/">even been sworn in yet</a>! Just yesterday (</span><em style="font-size: 13px;">yesterday</em><span style="font-size: 13px;">!) <a href="http://www.ajc.com/news/news/local-education/dekalb-school-official-i-am-not-a-plagiarist/nTycg/#cmComments">Ty Tagami</a> reported that a consultant named Ralph Taylor, who had been hired to &#8220;<a href="http://www.ajc.com/news/news/local-education/dekalb-school-official-i-am-not-a-plagiarist/nTycg/#cmComments"><em>analyze [DeKalb Schools'] alternative education program copied more than a third of his report from scholarly publications posted on the Internet</em></a>…&#8221; After completing his plagiarism, Mr. Taylor was hired as  &#8221;Associate Superintendent for Support Services&#8221; at <a href="http://www.ajc.com/news/news/local-education/dekalb-school-official-i-am-not-a-plagiarist/nTycg/#cmComments">$117,461</a> per year. If you think his job might possibly be at risk because he got it through plagiarism, you&#8217;re wrong. &#8220;<em><a href="http://www.ajc.com/news/news/local-education/dekalb-school-official-i-am-not-a-plagiarist/nTycg/#cmComments">School Superintendent Cheryl Atkinson said Taylor’s job is safe. “The infraction pertains to his work as a consultant, not as an employee</a></em>…”  Again, this was just </span><em style="font-size: 13px;">yesterday, </em><span style="font-size: 13px;">and, sadly, I am not making this up</span><em style="font-size: 13px;">.</em></p>
<p>Maureen Downey posed the question in her &#8220;<a href="http://blogs.ajc.com/get-schooled-blog/">Get Schooled</a>&#8221; blog at the AJC yesterday: &#8220;<a href="http://blogs.ajc.com/get-schooled-blog/2013/01/16/what-would-you-tell-the-state-board-tomorrow-about-the-performance-of-the-dekalb-school-board/?cxntfid=blogs_get_schooled_blog">What would you the tell the State Board [of Education]… about the performance of the DeKalb school board</a>?&#8221; The letter that prompted Ms. Downey, and the comments that follow, are worth reading -most call for removal of all 9 Board members. So does a <a href="http://www.change.org/petitions/governor-nathan-deal-and-georgia-state-board-of-education-review-sacs-findings-if-accurate-replace-the-dekalb-county-school-board?utm_source=share_petition&amp;utm_medium=url_share&amp;utm_campaign=url_share_before_sign">petition started by a parent</a>. And so does an <a href="http://www.change.org/petitions/georgia-governor-nathan-deal-replace-the-dekalb-county-school-board-2?utm_source=share_petition&amp;utm_medium=url_share&amp;utm_campaign=url_share_before_sign">identical petition, started by a student</a> at DeKalb&#8217;s Lakeside High School. (A <a href="http://www.myfoxatlanta.com/story/20488658/teen-creates-petition-calling-for-replacement-of-dekalb-school-board"><em>student</em></a>!)</p>
<p><span style="font-size: 13px;">The list of &#8220;what&#8217;s broken&#8221; is virtually endless, and pointless, because the taxpaying public, and the parents of DeKalb students (and I am a member of both groups) have lost all faith and trust in the system. DeKalb now serves only as a cautionary tale for the rest of Georgia, because everything that could go wrong with our State&#8217;s current system of public, K-12 education has gone wrong in DeKalb. Every. Single. Thing. And the time to fix it is now. </span></p>
<p><span style="font-size: 13px;">DeKalb Schools are a nearly a billion dollars worth of cronyism, nepotism, dysfunction and failure. And the DeKalb School System, to paraphrase <a href="http://bible.cc/matthew/7-5.htm">Matthew 7:5</a>, are a plank in Georgia&#8217;s eye. Other problems -funding mechanisms, budget cuts, teacher pay, etc.- are just bits of dust. Will the governor remove the board, and begin the process of fixing the most visible failure in Georgia? </span></p>
<p><span style="font-size: 13px;">Because if he can&#8217;t be trusted to fix the most obvious problem, how can we trust him on anything else?</span></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:84:"http://www.peachpundit.com/2013/01/17/dekalb-schools-time-to-pull-the-band-aid/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:38:"Morning Reads for Thursday, January 17";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.peachpundit.com/2013/01/17/morning-reads-for-thursday-january-17/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:85:"http://www.peachpundit.com/2013/01/17/morning-reads-for-thursday-january-17/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 12:16:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50795";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:382:"Winter Storm Watch for North Georgia. Affected area is North of a line from Carrollton to Atlanta to Jefferson. Video: SUGA DADDIES at GSU.  A quite viable option for 2.0 students&#8230; Georgia Deal backs guns for school administrators Atlanta’s Council Briefings Opened to Public, Moved to Committee Room Atlanta boy&#8217;s gun control letter scores White House invite [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Bridget Cantrell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:9965:"<p></p><p><a href="http://www.b.weather.com/weather/alerts/localalerts/30513?cm_ven=Facebook&amp;cm_cat=www.facebook.com&amp;cm_pla=fb_feed&amp;cm_ite=fb_myfriendswx_rec&amp;phenomena=WS&amp;significance=A&amp;areaid=GAZ006&amp;office=KFFC&amp;etn=0001&amp;trackid=xh8YK12dHTdQE3UCZSl5mSLDoZCsa%2B4TUeI8Zy6Ydaxgbn4N0jOZCGOAcSnVI%2BFIDvG26vaPUt6O%0ARQnilPs1h34HOP%2FVIcSPvGVuhoZYfzlnKll2q0F2QxPW94t4gU0Qd59VsVdcTBWObG6ku0Zo0g%3D%3D" target="_blank">Winter Storm Watch for North Georgia</a>. Affected area is North of a line from Carrollton to Atlanta to Jefferson.</p>
<p>Video: <a href="http://www.11alive.com/video/default.aspx?bctid=2096488290001" target="_blank">SUGA DADDIES</a> at GSU.  A quite viable option for 2.0 students&#8230;</p>
<p><strong>Georgia</strong></p>
<ul>
<li><a href="http://onlineathens.com/local-news/2013-01-16/deal-backs-guns-school-administrators" target="_blank">Deal backs guns for school administrators</a></li>
<li><a href="http://www.atlantaprogressivenews.com/interspire/news/2013/01/15/victory-atlanta%E2%80%99s-council-briefings-opened-to-public-moved-to-committee-room.html" target="_blank">Atlanta’s Council Briefings Opened to Public, Moved to Committee Room</a></li>
<li><a href="http://www.11alive.com/news/article/272637/40/Atlanta-boys-gun-control-letter-scores-White-House-invite" target="_blank">Atlanta boy&#8217;s gun control letter scores White House invite</a></li>
<li>Warner Robins: <a href="http://www.macon.com/2013/01/16/2318452/major-drug-dealer-ydc-officer.html" target="_blank">8 arrested in major drug, gambling and dog fighting operation in Houston</a></li>
<li><a href="http://wabe.org/post/ga-house-speaker-wants-complete-ban-lobbyist-gifts" target="_blank">Ga. House Speaker Wants Complete Ban on Lobbyist Gifts</a>.  This isn&#8217;t <em>news</em>, but I posted because the trend is ramping up for everyone&#8217;s business to be put out there front and center: &#8220;<em>Critics say that means a three night trip taken by Senator Jeff Mullis to a Florida resort for a Georgia Industrial Loan Association convention and another trip taken to the Grove Park Inn in Asheville by Senator Jack Murphy for the Georgia Bankers Association would be covered.</em>&#8220;</li>
<li><a href="http://mdjonline.com/view/full_story/21421281/article-Sheriff-launches-investigation-into-threatening-letter-sent-to-Judge-Green-s-family?instance=home_top_bullets" target="_blank">Sheriff Warren launches investigation into threatening letter sent to Judge Green&#8217;s family</a>. “The letter threatened the judge and his wife by the accused stating … he would kill and eat their children,” the warrant states. “He would cook them first to make them more palatable.”</li>
<li>One of my girlfriends posted: &#8221;Does anybody from Floyd or South Cobb remember Stan the bus driver? So creepy.&#8221;  See?  Kids know when something isn&#8217;t right.  Smyrna, <a href="http://smyrna.patch.com/articles/smyrna-mableton-men-indicted-for-child-exploitation-offenses" target="_blank">Mableton Men Indicted for Child Exploitation</a>.  <a href="http://www.mdjonline.com/view/full_story/21121889/article-Bus-driver-arrested-on-charges-connected-to-child-pornography" target="_blank">Stan has worked in Cobb since 1989</a>.  Ugh.</li>
<li><a href="http://www.macon.com/2013/01/16/2319049/state-wants-feedback-on-bear-hunts.html" target="_blank">State wants feedback on bear hunts before setting new regulations</a></li>
<li>Video: <a href="http://www.youtube.com/watch?feature=player_embedded&amp;v=zybnaPqzJ6s#!" target="_blank">We&#8217;re here for ya, fatty</a>.  <a href="http://www.salon.com/2013/01/16/coke_goes_on_the_defensive/" target="_blank">Coke goes on the defensive</a>.</li>
<li><a href="http://www.ajc.com/news/news/breaking-news/cox-enterprises-investing-250-million-in-tech-star/nTxjp/" target="_blank">Cox Enterprises investing $250 million in tech startups</a></li>
<li>Officials agree: <a href="http://mdjonline.com/bookmark/21421465" target="_blank">Enhancement plan good idea</a>.  My beloved alma mater continues to work on the <a href="http://mu2lci.com/?cat=4" target="_blank">The Greentech Corridor</a> and revamping Cobb Parkway. <a href="http://jacobsplanning.us/mu2/wp-content/uploads/2012/12/MU2%20LCI%20Fact%20Sheet.pdf" target="_blank"> Fact sheet</a>.</li>
<li>Don&#8217;t lie &#8211; you&#8217;d do this if you could: <a href="http://onlineathens.com/breaking-news/2013-01-16/athens-dental-worker-doses-self-laughing-gas" target="_blank">Athens dental worker doses self with laughing gas</a></li>
<li><a href="http://www.ajc.com/news/business/walmart-to-open-mini-store-on-georgia-tech-campus/nTyJh/" target="_blank">Wal-mart’s smallest store in the U.S to open on GT campus</a></li>
</ul>
<p><strong>Inter/national</strong></p>
<ul>
<li><a href="http://www.usatoday.com/story/news/world/2013/01/16/algeria-militants-kidnap-foreigners-at-gas-plant/1839049/" target="_blank">Islamic militants take 7 Americans hostage in Algeria </a></li>
<li><a href="http://thehill.com/blogs/blog-briefing-room/news/277521-rubio-slams-obama-on-new-gun-proposals?utm_campaign=briefingroom&amp;utm_source=twitterfeed&amp;utm_medium=twitterfeed" target="_blank">Rubio slams Obama on new gun proposals</a></li>
<li><a href="http://www.washingtonpost.com/blogs/wonkblog/wp/2013/01/16/obamas-gun-proposals-what-will-work-and-what-wont/" target="_blank">Gun experts grade Obama’s proposals</a></li>
<li>Oh c&#8217;mon &#8211; it&#8217;s funny: <a href="http://www.infowars.com/other-tyrants-who-have-used-children-as-props/" target="_blank">Other Tyrants Who Have Used Children As Props</a></li>
<li><a href="http://www.npr.org/blogs/krulwich/2013/01/16/169511949/a-mysterious-patch-of-light-shows-up-in-the-north-dakota-dark" target="_blank">North Dakota is now the second largest oil producing state in America</a></li>
<li><a href="http://abcnews.go.com/US/oprah-winfrey-describes-intense-lance-armstrong-interview/story?id=18217400" target="_blank">Oprah Winfrey Describes Intense Lance Armstrong Interview</a>.  <a href="http://www.deadline.com/2013/01/ad-time-for-oprahs-lance-armstrong-interview-expected-to-sell-out-report/" target="_blank">Ad time for interview to sell out</a>.</li>
<li><a href="http://www.reuters.com/article/2013/01/16/cybersecurity-powerplants-idUSL1E9CGFPY20130116" target="_blank">Malicious virus shuttered U.S. power plant</a></li>
<li>It&#8217;s on the internet &#8211; it must be true.  <a href="http://www.politico.com/story/2013/01/survey-users-trust-social-media-as-news-source-86321.html" target="_blank">Users trust social media as news source</a></li>
<li><a href="http://www.dailymail.co.uk/tvshowbiz/article-2263739/Kim-Kardashian-Arabian-cover-girl-dons-veil-luxury-Middle-East-magazine.html" target="_blank">Veiled Kim Kardashian is an Arabian cover girl</a>&#8230; but reveals her breasts in daring gown.</li>
<li>Wow &#8211; that&#8217;s pretty creative even though skirting the system didn&#8217;t work out:<a href="Couple told to tear down secret £500,000 luxury home they hid inside a hay barn after losing final round of 10 year legal battle with planners" target="_blank"> Couple told to tear down secret £500,000 luxury home they hid inside a hay barn after losing final round of 10 year legal battle with planners</a></li>
</ul>
<p><strong>Whatevs</strong></p>
<ul>
<li>SMH.  <a href="http://www.usatoday.com/story/sports/ncaaf/2013/01/16/manti-teo-girlfriend-hoax-deadspin/1840415/" target="_blank">Manti Te&#8217;o's inspirational girlfriend story a hoax</a></li>
<li>I&#8217;m sorry to hear about Mr. Drummond, Willis. <a href="http://www.accessatlanta.com/ap/ap/obituaries/conrad-bain-of-diffrent-strokes-dead-at-89/nTyLD/" target="_blank">Conrad Bain of &#8216;Diff&#8217;rent Strokes&#8217; dead at 89</a></li>
<li>For GT Fans:<a href="http://gatech.edu/newsroom/release.html?nid=184501" target="_blank"> Georgia Tech names Mike Bobinski (fanna fofinski) as new Athletic Director</a></li>
<li><a href="http://www.ajc.com/news/news/woman-robbed-at-gunpoint-at-kroger-in-vinings/nTyK2/" target="_blank">Woman robbed at gunpoint at Kroger in Vinings</a>.  This isn&#8217;t funny, but&#8230;only in Vinings would the robber be wearing a designer sweater and khakis.</li>
<li><a href="http://www.dailymail.co.uk/news/article-2263497/Tiger-Woods-tries-win-ex-wife-Elin-200-million-deal-says-yes--claims-new-report.html" target="_blank">Tiger Woods tries to &#8216;win back ex-wife Elin</a> with a $200 million pre-nup &#8211; but only if he includes a $350 million anti-cheating clause.</li>
<li><a href="http://blog.insideview.com/2012/12/22/the-2-most-underrated-qualities-of-top-performers/" target="_blank">The 2 Most Underrated Qualities of Top Performers</a></li>
<li><a href="http://www.fastcompany.com/3003945/one-conversational-tool-will-make-you-better-absolutely-everything" target="_blank">The One Conversational Tool That Will Make You Better At Absolutely Everything</a></li>
<li><a href="http://blogs.hbr.org/cs/2013/01/women_need_to_realize_work_isnt_schol.html?utm_source=Socialflow&amp;utm_medium=Tweet&amp;utm_campaign=Socialflow" target="_blank">Women Need to Realize Work Isn&#8217;t School</a></li>
<li><a href="http://www.inc.com/steve-tobak/10-lessons-for-a-new-generation-of-leaders.html" target="_blank">10 Leadership Lessons for Gen-Y</a></li>
<li>Dang &#8211; why do they always self-destruct?  Amanda Bynes (multi-millionaire of childhood Nickelodeon fame) now follows up her DUIs with a <a href="http://www.huffingtonpost.com/2013/01/16/amanda-bynes-cheek-piercing-photo_n_2486484.html?ncid=edlinkusaolp00000003#slide=more250437" target="_blank">cheek-piercing</a></li>
<li>For Last Democrat in Georgia: <a href="http://www.uber-facts.com/2012/12/talking-to-yourself-makes-you-smarter/#disqus_thread" target="_blank">Talking To Yourself Makes You Smarter</a></li>
</ul>
<p>&nbsp;</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:81:"http://www.peachpundit.com/2013/01/17/morning-reads-for-thursday-january-17/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:68:"Would tripling salaries solve our ethics problem over the long haul?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"http://www.peachpundit.com/2013/01/16/ethics-problem/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.peachpundit.com/2013/01/16/ethics-problem/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 04:30:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50802";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:341:"I was going to put this in tomorrow&#8217;s morning reads, but it&#8217;s a topic worthy of its own post.  Jim Galloway discusses the forbidden topic lurking behind the ethics reform debate. A couple of highlights: State lawmakers earn $17,342 a year. Ten states pay less, according to one national overview. A $173 per diem – only [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Bridget Cantrell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2749:"<p></p><p>I was going to put this in tomorrow&#8217;s morning reads, but it&#8217;s a topic worthy of its own post.  Jim Galloway discusses <a href="http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/the-forbidden-topic-beneath-the-ethics-reform-debate/" target="_blank">the forbidden topic lurking behind the ethics reform debate</a>.</p>
<p style="padding-left: 30px">A couple of highlights:</p>
<blockquote>
<ul>
<li>State lawmakers earn $17,342 a year. Ten states pay less, according to one national overview. A $173 per diem – only four states have higher daily expense coverage – augments their pay to $24,000 or so. If you get what you pay for, then Georgians should have no reason to complain. They’ve been paying for an army of fry cooks and dishwashers.</li>
<li>“It really limits the people who can run for office,” [Godwin] said. A Legislature heavy with retirees, the wealthy and the willing poor doesn’t accurately reflect the public that sends them there, she argues.</li>
<li>The less savory argument is that an adequately reimbursed lawmaker would be less likely to feel entitled to the free meals, booze, and tickets to concerts and football games that are now on the table.</li>
<li>The problem is that lawmakers themselves are loathe to raise the pay issue. “I’m not going to vote for an increase in legislative pay when I have school teachers in every district that I represent who are being furloughed,” said state Sen. Josh McKoon.</li>
<li>Livable wages for state lawmakers would have to be an issue taken up by a fellow with plenty of clout and little to lose. A governor in his second term, for instance.</li>
</ul>
</blockquote>
<div>Quick math: 236 lawmakers @$17,342 = $4.09M; @$60,000 = $14.16M.</div>
<div>
<ol>
<li>What&#8217;s the true ROI on that $10M difference?</li>
<li>Could some of these watchdog folks accumulate enough examples of taxpayer money lost on unethical behavior to show where $10M actually starts looking like a great deal?</li>
<li>And no, legislators can&#8217;t push this &#8211; &#8220;Hey, pay us more and we&#8217;ll start being ethical!&#8221;  There&#8217;s no spin doctor around who can make that sound good.</li>
<li>And without a public outcry, why would the affluent legislators who do it for the ego rather than the money willingly give up that power anyway?</li>
<li>I wonder &#8211; where are the concerned citizens of each community who would speak out and say, &#8220;I would run against [this incumbent] if I were paid a salary I could live on all year.  40 days?  I&#8217;d be your representative 24/365.&#8221;  Those are the people who would need to get on the news to get the ball rolling I&#8217;d guess.</li>
</ol>
</div>
<p>&nbsp;</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:58:"http://www.peachpundit.com/2013/01/16/ethics-problem/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"25";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:70:"In Case You Were Wondering About Committee Assignments In The House…";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://www.peachpundit.com/2013/01/16/in-case-you-were-wondering-about-committee-assignments-in-the-house/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:115:"http://www.peachpundit.com/2013/01/16/in-case-you-were-wondering-about-committee-assignments-in-the-house/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 20:32:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50791";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:85:"Here they are. (Link goes to a pdf.) Peruse at your leisure and discuss below. &#160;";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Mike Hassinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:216:"<p></p><p><a href="http://www.house.ga.gov/Documents/Publications-Downloads/2013_Committee_Assignments.pdf">Here they are</a>. (Link goes to a pdf.)</p>
<p>Peruse at your leisure and discuss below.</p>
<p>&nbsp;</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:111:"http://www.peachpundit.com/2013/01/16/in-case-you-were-wondering-about-committee-assignments-in-the-house/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Mullis Elevated To Rules Chairman";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.peachpundit.com/2013/01/16/mullis-elevated-to-rules-chairman/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:81:"http://www.peachpundit.com/2013/01/16/mullis-elevated-to-rules-chairman/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 20:07:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:11:"Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50768";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:362:"It was officially announced on Monday evening that Senator Jeff Mullis (R-Chickamauga) would succeed fellow Senator Don Balfour (R-Snellville) as the Senate&#8217;s Rules chairman.  It&#8217;s a distinct honor to be selected as such, and I certainly wish Senator well in his new position within the Senate. Senator Mullis is my state senator and my friend [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Nathan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1847:"<p></p><p>It was officially announced on Monday evening that Senator Jeff Mullis (R-Chickamauga) would succeed fellow Senator Don Balfour (R-Snellville) as the Senate&#8217;s Rules chairman.  It&#8217;s a distinct honor to be selected as such, and I certainly wish Senator well in his new position within the Senate.</p>
<p>Senator Mullis is my state senator and my friend as well.  He&#8217;s gotten some heat over some of the policies, mainly T-SPLOST.  Which he did explain that he originally wanted it as a per-county option, but former Governor Sonny Perdue wanted a statewide option.  The compromise was the transportation districts. A lot of the legislative process does involve compromise.  Anyway, we&#8217;ve sliced, diced, chunked, and topped that conversation plenty of times before the July 31, 2012 election and after the election.  That&#8217;s not what I want to talk about.</p>
<p>I just wanted to take a point of personal privilege and congratulate my friend on his appointment.  I think it&#8217;s pretty outstanding to see someone from Chickamauga, GA (a place that most folks have heard about as a battle during the War of Northern Aggression but seem to have a tough time pronouncing) become one of the more influential people in the state.  I hope he will be open to advise and opinions from both sides and seek wisdom to do what&#8217;s best for Georgians.  I hope that Senator Mullis will use the opportunity to get conservative legislation, like stronger pro-gun rights legislation, meaningful tax reform (I&#8217;m liking what I see in <a href="http://www.legis.ga.gov/Legislation/en-US/display/20132014/SR/8">S.R. 8</a> sponsored by Senator Josh McKoon (R-Columbus)), and other policies to keep the State of Georgia strong, to the Senate floor and passed by the chamber.</p>
<p>Congratulations, Senator.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:77:"http://www.peachpundit.com/2013/01/16/mullis-elevated-to-rules-chairman/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:89:"National Rifle Association Responds To President Obama’s Executive Order On Gun Control";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:125:"http://www.peachpundit.com/2013/01/16/national-rifle-association-responds-to-president-obamas-executive-order-on-gun-control/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:134:"http://www.peachpundit.com/2013/01/16/national-rifle-association-responds-to-president-obamas-executive-order-on-gun-control/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 19:54:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50789";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:373:"The National Rifle Association Responds to President Barack Obama&#8217;s 23 executive actions on gun control: Throughout its history, the National Rifle Association has led efforts to promote safety and responsible gun ownership. Keeping our children and society safe remains our top priority. The NRA will continue to focus on keeping our children safe and securing [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Nathan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3989:"<p></p><p>The National Rifle Association Responds to President Barack Obama&#8217;s <a href="http://www.foxnews.com/politics/2013/01/16/list-executive-actions-obama-plans-to-take-as-part-anti-gun-violence-plan/">23 executive actions</a> on gun control:</p>
<blockquote><p>Throughout its history, the National Rifle Association has led efforts to promote safety and responsible gun ownership. Keeping our children and society safe remains our top priority.</p>
<p>The NRA will continue to focus on keeping our children safe and securing our schools, fixing our broken mental health system, and prosecuting violent criminals to the fullest extent of the law. We look forward to working with Congress on a bi-partisan basis to find real solutions to protecting America’s most valuable asset – our children.</p>
<p>Attacking firearms and ignoring children is not a solution to the crisis we face as a nation. Only honest, law-abiding gun owners will be affected and our children will remain vulnerable to the inevitability of more tragedy.</p></blockquote>
<p>Both Congressmen Jack Kingston (R-GA-01) and Tom Graves (R-GA-14) issued statements concerning Obama&#8217;s EOs.<br />
<span id="more-50789"></span><br />
From Kingston&#8217;s office:</p>
<blockquote><p>WASHINGTON, D.C. – Congressman Jack Kingston (GA-1) released the following statement in response to President Barack Obama’s remarks and executive actions on gun control:</p>
<p>“We owe this country a thorough and thoughtful discussion on what actions can be taken to prevent future tragedies.  Unfortunately, this national debate is already digressing into the usual camps and an ‘us versus them’ mentality.  The victims of Newtown and all victims of gun violence deserve better.</p>
<p>“I am encouraged that the President has put mental health as well as the relationship between video games, media images, and violence on the table.  I am disappointed, however, that he would immediately resort to executive orders rather than working with Congress on a comprehensive legislative response.</p>
<p>“As someone in elected office, I am very concerned that the President’s approach will divide this country in a way we have not been before.  Gun sales have been through the roof over the past month and the intensity of email traffic my office is receiving is worrisome.  The President should take a leadership role and say ‘let’s take a step back to approach this issue calmly.’</p>
<p>“As to the specific legislative proposals the President advocates, I am doubtful some of them pass constitutional muster or would be effective in preventing another Newtown.  The perpetrators of mass violence do not concern themselves with what laws they are breaking in the commission of their crimes. </p>
<p>“We are united in our desire to prevent gun violence.  Let’s put aside the failed old arguments of the past and come together to accomplish that task in a meaningful way.”</p></blockquote>
<p>From Graves&#8217; office:</p>
<blockquote><p>U.S. Rep. Tom Graves (R-GA-14) issued the following statement in response to President Obama’s press conference where the Administration’s gun control agenda was announced:</p>
<p>“As the father of three and the husband of an elementary school teacher, it has been devastating to see the loss of life at the hands of armed madmen. In the aftermath of these tragedies, our nation has at once been united in grief and divided by a political agenda put forward by the president. The political agenda disregards the reality that violent criminals and murderers do not live by the laws of our land. It focuses on the re-regulation of lawful gun use, with the least consideration given to the rights of people to protect themselves, their families and their places of community gathering. I will continue to defend Second Amendment rights and oppose legislation that seeks to infringe on or intimidate people from exercising those rights.”</p></blockquote>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:130:"http://www.peachpundit.com/2013/01/16/national-rifle-association-responds-to-president-obamas-executive-order-on-gun-control/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"8";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Republicans Leave DC For Three Days Of Work";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:82:"http://www.peachpundit.com/2013/01/16/republicans-leave-dc-for-three-days-of-work/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:91:"http://www.peachpundit.com/2013/01/16/republicans-leave-dc-for-three-days-of-work/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 18:00:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50781";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:343:"Today&#8217;s Courier Herald Column: House Republicans will leave Washington on Wednesday and head a few hours south to Williamsburg where they will have a three day “retreat”.  It should hardly be considered three days of rest and relaxation.  Even more so than most years, this will have to be a working vacation. Between now and [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Charlie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4740:"<p></p><p><em>Today&#8217;s Courier Herald Column:</em></p>
<p>House Republicans will leave Washington on Wednesday and head a few hours south to Williamsburg where they will have a three day “retreat”.  It should hardly be considered three days of rest and relaxation.  Even more so than most years, this will have to be a working vacation.</p>
<p>Between now and the end of March issues of the nation’s debt ceiling, sequestration budget cuts, and an expiring continuing resolution to fund the government must be addressed.   The Republican voice on these issues will largely be expressed by the House of Representatives.  The retreat, hopefully, will allow the house to appear on the same page.</p>
<p>There was a small revolt against re-electing speaker Boehner from within the far right corner of the Republican caucus just after the New Year’s fiscal cliff vote.  Key to tamping down any hint of rebellion was the commitment that the fiscal cliff vote was just a step which preserved as much of current tax rates as could be, with spending issues addressed in this next round.  Any sense that Republicans are the party of fiscal discipline now rides on how they handle these three issues.<span id="more-50781"></span></p>
<p>As a matter of strategy, Republicans continue to hold a weak hand.  They are only in charge of the House, with the Senate and White House continuing to be held by Democrats.  As the debate is currently framed with the public, Republicans are the ones being labeled as “obstructionists”, despite the Senate’s refusal to pass a budget for over 1,350 days and the President recently signaling – again – that he will not meet his legally prescribed deadline to submit a budget to Congress.</p>
<p>Instead of proposing legitimate proposals to cut spending, Democrats have taken a bifurcated strategy to chide Republicans on not giving unlimited borrowing power to a President who now claims we have no spending problem while doing everything possible to keep the national political discussion focused on potential sweeping gun control regulation and not on the imminent threat imposed by the country’s fiscal mess.</p>
<p>Republicans must spend the days at this retreat coalescing around a strategy to regain the high ground in message, as well as to regain public trust not just as a party that wants to cut taxes, but as the one concerned with closing the existing budget deficit.  They should start by temporarily taking the debt ceiling off of the table.</p>
<p>Of the three deadlines in front of Congress, the debt ceiling is the one that should be used as a last resort, not the first line to be drawn in the sand.  Congress – with the Republican House’s help – is the one who appropriated money to spend through March 30<sup>th</sup> when the current CR expires.  In order to remove the criticism that Congress won’t increase the nation’s borrowing capacity to cover spending that they have already authorized, the House should immediately pass an increase to the debt ceiling that would cover spending through March 30<sup>th</sup>.  If the Senate refuses to pass the same measure or the President chooses to veto, it would then be much easier to demonstrate who is actually being an obstructionist in this process.</p>
<p>The spending deadlines would then be put in the proper order of magnitude.  Sequestration on March 1<sup>st</sup> would be painful, but wouldn’t shut the government down and may be the signal needed to show that Republicans are serious about actual spending cuts.  The thirty days following would allow for honest debate on a long term spending fix which must address entitlements if there is to be meaningful reduction in long term deficits.</p>
<p>Republicans should also use this term to remind the President of his campaign promises.  A President and complicit media chided Republicans for not being willing to go along with $3 in cuts for every $1 in revenue increased.  The President, now having secured revenue increases from Congress, says we don’t have a spending problem but continues the never ending cry that “the rich” should pay more.  Republicans have come to the table with Revenue against the wishes of their base. It is time to demand Democrats offer and accept spending cuts.</p>
<p>House Republicans will spend three days among themselves to figure out how to address these issues as unified body while projecting leadership from a minority party position.  If they intend not to return as the actual minority party in the House in 2015, they need to adopt a strategy that can be clearly articulated to and accepted by the general public.  That’s some heavy lifting for a three day working retreat.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:87:"http://www.peachpundit.com/2013/01/16/republicans-leave-dc-for-three-days-of-work/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Eggs &amp; Issues Breakfast – the runny gossip";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"http://www.peachpundit.com/2013/01/16/eggs-issues-breakfast-the-runny-gossip/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:86:"http://www.peachpundit.com/2013/01/16/eggs-issues-breakfast-the-runny-gossip/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 15:15:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50777";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:345:"Jim Galloway&#8217;s update is entertaining. Booze and tobacco dropped from menu: By tradition, Coca-Cola has served as the corporate sponsor of a luncheon honoring the president pro tem of the state Senate at the opening of each year’s session. Also by tradition, the menu has included Bloody Marys and cigars. But no more. We’re told [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Bridget Cantrell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3579:"<p></p><p>Jim Galloway&#8217;s <a href="http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/your-daily-jolt-bloody-marys-stogies-dropped-from-senate-lunch-menu/" target="_blank">update</a> is entertaining.</p>
<p>Booze and tobacco dropped from menu:</p>
<blockquote><p>By tradition, Coca-Cola has served as the corporate sponsor of a luncheon honoring the president pro tem of the state Senate at the opening of each year’s session.</p>
<p>Also by tradition, the menu has included Bloody Marys and cigars. But no more. We’re told that both alcohol and stogies have been removed from this afternoon’s festivities honoring David Shafer, R-Duluth, the Senate’s newly elected leader.</p>
<p>The reason? The flourishes were thought to clash with the Capitol’s embrace of ethics reform, including a $100 per lawmaker cap on gifts from lobbyists passed by the Senate on Monday.</p>
<p>Or perhaps it was fallout from Coke’s new anti-obesity campaign. Either one.</p></blockquote>
<p>Ethics bill more of a sun visor than a cap: (<em>oh c&#8217;mon &#8211; that&#8217;s funny right there</em>)</p>
<blockquote><p><span id="more-50777"></span>Speaking of ethics, here’s the exchange that just took place between House Speaker David Ralston and Lt. Gov. Casey Cagle at this morning’s Eggs &amp; Issues breakfast, sponsored by the Georgia Chamber. My AJC colleague Greg Bluestein said Cagle had just bragged about his chamber’s passage of a $100 cap on gifts from lobbyists to lawmakers:</p>
<p>Ralston: “I tease my friend that it was more of a sun visor than a cap that they adopted yesterday. (Chuckling)</p>
<p>Cagle: “Where are those boxing gloves? (More chuckling)</p>
<p>Ralston: “We can disagree about the way we do this. At the end of the day I anticipate the lieutenant governor and the Senate and the Souse will come to an agreement about what constitutes a real change. We can debate a ban or a cap all day long but let me make a couple of points that get lost in this discussion.</p>
<p>“What the specifics are kind of cloud why we’re having this discussion. What it boils down to is who is going to influence public policy in this debate …Is it going to be influenced by the political class or media elites who make … a clearly erroneous perception of the kind of people in the Georgia House and the Georgia Senate?</p></blockquote>
<p>Bridg Note: I&#8217;ve been trying to meet as many legislators in person as I can.  Yes, it&#8217;s very easy to write snarky posts about people you don&#8217;t personally know &#8211; who aren&#8217;t <em>real</em>.  I&#8217;ll write more about my project this weekend, but know that I&#8217;m trying to move my perception of people towards first-hand interaction instead of online articles.  I was told Speaker Ralston&#8217;s office &#8220;would get back to me.&#8221;  I&#8217;ll be right here with bated breath.</p>
<p>Also at this morning Eggs &amp; Issues breakfast&#8230;</p>
<blockquote><p>&#8230;sponsored by the Georgia Chamber, every state officer present was asked to stand and be recognized. All but one, that is. School Superintendent John Barge was left out.</p>
<p>The slight was apparently unintentional. Barge was belatedly introduced after a speech by Atlanta Mayor Kasim Reed. But for a few minutes, everyone wondered whether it was another sign of the high-profile split between Barge and the rest of the state GOP leadership over charter schools. More than a few lawmakers have hinted that the school superintendent will see some punishment in his budget.</p></blockquote>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:82:"http://www.peachpundit.com/2013/01/16/eggs-issues-breakfast-the-runny-gossip/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:53:"Georgia Chamber Announces 2013 Legislative Priorities";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:82:"http://www.peachpundit.com/2013/01/16/georgia-chamber-2013-legislative-priorities/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:91:"http://www.peachpundit.com/2013/01/16/georgia-chamber-2013-legislative-priorities/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 14:43:56 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50769";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:369:"As one of the guys noted in the chatline - The Chamber&#8217;s agenda is &#8220;the&#8221; agenda.  A list of the Chamber&#8217;s 2013 Board of Governors is below the fold. The Georgia Chamber of Commerce formally introduced its 2013 legislative priorities Wednesday during the annual Eggs &#38; Issues Breakfast in Atlanta – identifying several key issues to [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Bridget Cantrell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:7844:"<p></p><p>As one of the guys noted in the chatline - The Chamber&#8217;s agenda is &#8220;the&#8221; agenda.  A list of the Chamber&#8217;s 2013 Board of Governors is below the fold.</p>
<blockquote><p>The Georgia Chamber of Commerce formally introduced its 2013 legislative priorities Wednesday during the annual Eggs &amp; Issues Breakfast in Atlanta – identifying several key issues to be addressed during the 2013 General Assembly deemed vital to strengthening the state’s business community and growing Georgia’s economy.</p>
<p>“Georgia enjoyed one of the most business-friendly sessions in history last year, but in today’s global marketplace you need to constantly evolve and improve in order to stay competitive,” said Georgia Chamber President and CEO Chris Clark. “To ensure that Georgia stays on the right path to economic prosperity, we need to give companies already here in Georgia every resource possible to grow, and at the same time create an environment that will attract new business and investment. We look forward to working with Governor Deal and the General Assembly this session to get these important issues addressed.”</p>
<p>“The Port of Savannah expansion will be the biggest economic development opportunity of our generation. To take advantage of the job growth it will bring, we need to be sure that Georgia is ready to offer companies the most pro-business climate possible,” said the Chamber’s 2013 Board Chair Steve Green. “We feel strongly that by working with lawmakers to concentrate on these legislative priorities, we will put Georgia in an even better position to compete both domestically and globally, create jobs, attract investment, and strengthen our overall business climate.&#8221;</p>
<p>The Georgia Chamber 2013 Legislative Priorities include:</p>
<p><span id="more-50769"></span>Economic Development</p>
<ul>
<li>Lessen regulations and improve incentives that facilitate expansions, capital investment, new recruitment, small business creation and hospitality growth</li>
<li>Expand access to venture and seed capital by offering incentives to attract venture capital firms to Georgia</li>
</ul>
<p>Education</p>
<ul>
<li>Encourage school improvement through a more flexible, accountable and transparent system tied to a new school rating system</li>
<li>Pursue a new school student-based budgeting funding system that would tie the majority of the state’s educational spending directly to student need</li>
</ul>
<p>Employment</p>
<ul>
<li>Enact workers’ compensation system reforms that promote a balanced and equitable system that is fair to the employee and employer and designed to return the employee to work as soon as medically appropriate</li>
<li>Preserve Georgia’s employment-at-will doctrine and strengthening the state’s right-to-work status</li>
</ul>
<p>Environment</p>
<ul>
<li>Pursue funding sources for continuation of Regional Water Councils and their work to implement regional water plans</li>
</ul>
<p>Health Care</p>
<ul>
<li>Support a health care financing program through which providers can continue to care for patients without compromising the current delivery system</li>
</ul>
<p>Legal Reform</p>
<ul>
<li>Pursue civil justice reforms that provide confidence of equitable and predictable treatment of business in the courts:
<ul>
<li>E-discovery: Create a judicial process for scoping electronic discovery requests that provides for cost sharing</li>
<li>Duplicative recovery reform: Permit introduction of evidence to the jury of third party payments for consideration of damages</li>
<li>Seat belt evidence: Permit introduction of evidence of seat belt use for determining proportional liability</li>
<li>Bad faith: Extend the time for response to demand letters and clarify who the third party insurer must pay</li>
<li>Transportation: Protect the “Transportation Investment Act” to ensure the benefits of the law as passed are not diluted in any way</li>
</ul>
</li>
</ul>
<p>Joining Green on the 2013 Chamber Board of Governors are:</p>
<p>Executive Committee</p>
<ul>
<li>Stephen S. Green (Morris Manning Martin &amp; Green Consulting LLP) – 2013 Chair</li>
<li>Chris Clark (Georgia Chamber of Commerce) – President and CEO</li>
<li>Edward S. Heys Jr. (Deloitte.) – Immediate Past Chair</li>
<li>Ernest L. Greer (Greenberg Traurig LLP) – 2014 Chair-Elect</li>
<li>Paul Bowers (Georgia Power) – 2015 Chair-Elect</li>
<li>Susan Bell (Ernst &amp; Young LLP) – Administrative Services Chair</li>
</ul>
<p>Policy Committee Chairs</p>
<ul>
<li>Bryan Batson (AGL Resources) – Energy &amp; Natural Resources</li>
<li>Tom Bishop (Georgia Power) – Law &amp; Judiciary</li>
<li>Michael T. Petrik (Alston &amp; Bird) – Tax &amp; Finance</li>
<li>Glen Kelley (Wells Fargo) – Education &amp; Workforce Development</li>
<li>Kevin A. Grenier (Gas South) – Health &amp; Wellness</li>
<li>Eric J. Tanenblatt (McKenna Long &amp; Aldridge) – Innovation &amp; Technology</li>
</ul>
<p>Government Affairs Council Chair</p>
<ul>
<li>Roy B. Robinson III (The RB Robinson Company LLC) – Chair</li>
</ul>
<p>Membership Council Chairs</p>
<ul>
<li>Martin Long (Long &amp; Associates LLC) – Small Business Advisory Council</li>
<li>Thomas M. Lowe III (Lowe Engineers LLC) – Sustainability Council</li>
<li>Craig S. Lesser (Pendelton Consulting) International Advisory Council</li>
<li>Kali Boatright (Douglas County Chamber of Commerce) – Georgia Association of Chamber of Commerce Executives Chair</li>
</ul>
<p>Regional Chairs</p>
<ul>
<li>Nicolas Bouckaert (Beaulieu Group) Region 1; Dalton</li>
<li>James J. Adams (Adams Transfer &amp; Storage) – Region 2; Gainesville</li>
<li>Donna Hyland (Children’s Healthcare of Atlanta) – Region 3; Atlanta</li>
<li>Randy Jackson (Kia Motors Manufacturing Georgia Inc.) – Region 4; Thomaston/Carrollton</li>
<li>William Rabun Neal (Reynolds Plantation) – Region 5; Athens</li>
<li>Robbo Hatcher, Jr. (H2 Capital) – Region 6; Macon</li>
<li>Ben J. Tarbutton III (First Sandersville Corp.) – Region 7; Augusta/Sandersville</li>
<li>Joel F. Ames (Atoms Energy) – Region 8; Columbus</li>
<li>James L. Allgood, Jr. (Allgood Pest Control) – Region 9; Dublin/Vidalia</li>
<li>J. Bodine Sinyard (Adams Exterminators) – Region 10; Albany</li>
<li>John Wesley Langdale III (The Langdale Company) – Region 11; Valdosta</li>
<li>Eric Johnson (Husey Gay Bell &amp; DeYoung) – Region 12; Savannah/Brunswick</li>
</ul>
<p>General Governors</p>
<ul>
<li>Dean Alford – Allied Energy</li>
<li>Jim Allen – Atlanta Braves</li>
<li>Stephen R. Avera – Flowers Foods</li>
<li>Kevin Scott Blair – SunTrust</li>
<li>Brendon Chambers – PNC Financial Services</li>
<li>Shan Cooper – Lockheed Martin</li>
<li>Kenneth C. Cornelius &#8211; Siemens</li>
<li>Jac Counts – Cancer Treatment Centers of America</li>
<li>Al Ertel – Alliant</li>
<li>Susanne Hall – The Coca-Cola Company</li>
<li>Tad Hutcheson – Delta</li>
<li>Morgan Kendrick – Blue Cross Blue Shield of Georgia</li>
<li>James Allen Kibler, Jr. – AMEC E&amp;I, Inc.</li>
<li>Doug Kidd &#8211; INVESCO</li>
<li>Hank Linginfelter – AGL Resources</li>
<li>Bill Linginfelter – Regions Bank</li>
<li>Michael Mayoras – RedPrairie</li>
<li>Michael R. Mitchell &#8211; Publix</li>
<li>Tom Moffett – Zep, Inc.</li>
<li>Frank Morris – UPS</li>
<li>Trey Paris – GE</li>
<li>llen Richardson – Koch Industries/Georgia Pacific</li>
<li>Michelle Robinson – Verizon</li>
<li>Sylvia E. Russell – AT&amp;T</li>
<li>Misty Skedgell – Turner Broadcasting</li>
<li>Kessel D. Stelling, Jr. &#8211; Synovus</li>
<li>Rob Stewart – State Farm</li>
<li>Rob VanGorden – The Richards Group</li>
<li>Paul Wood – Georgia EMC</li>
<li>Michael Zuna &#8211; AFLAC</li>
</ul>
<p>&nbsp;</p></blockquote>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:87:"http://www.peachpundit.com/2013/01/16/georgia-chamber-2013-legislative-priorities/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Morning Reads 1/16/13";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"http://www.peachpundit.com/2013/01/16/morning-reads-11613/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:67:"http://www.peachpundit.com/2013/01/16/morning-reads-11613/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 12:00:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50759";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:340:"Free Phones will soon be Five Dollar Phones. I like mine on flatbread with spicy mustard. Sen. Crane to everyone else: &#8220;You shall rue the day you gave power back to Cagle. RUE IT.&#8221; Adult justice reform still a hot topic &#8217;round the Gold Dome. Here&#8217;s how you fix the &#8220;justice system.&#8221; Make a Domestic [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Ron Daniels";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2554:"<p></p><p>Free Phones will soon be <a href="http://www.ajc.com/news/business/georgia-to-charge-for-free-phone-program/nTxjG/">Five Dollar Phones</a>. I like mine on flatbread with spicy mustard.</p>
<p>Sen. Crane to everyone else: &#8220;<a href="http://chronicle.augusta.com/news/government/2013-01-15/ga-senate-still-debating-power-shift?v=1358261796">You shall rue the day you gave power back to Cagle. RUE IT.</a>&#8221;</p>
<p>Adult justice reform still a hot topic <a href="http://www.sfgate.com/news/crime/article/Georgia-lawmakers-eye-more-adult-justice-reforms-4195103.php">&#8217;round the Gold Dome</a>. Here&#8217;s how you fix the &#8220;justice system.&#8221; Make a Domestic Relations Court. That will clear the path for everything else to be handled efficiently. Oh, and fix the laws that overly favor mega-banks that are killing both consumers and community banks.</p>
<p>Georgia Soldier <a href="http://www.ajc.com/news/news/georgia-soldier-killed-in-florida-wreck/nTxpR/">dies in Florida</a>; overturns vehicle.</p>
<p>North Georgia man threatens to eat<a href="http://www.nydailynews.com/news/national/man-threatened-eat-judge-kids-article-1.1240370"> Superior Court Judge&#8217;s family</a>.</p>
<p>Whether the Lord is willing or not, the <a href="http://www.11alive.com/news/article/272555/40/Update-On-Flooded-RiversCreeks-in-Georgia">creeks may rise</a>. Now if the Falcons can just Rise Up.</p>
<p>Forestry;<a href="http://www.forest-blade.com/news/business/article_449c48e2-5f33-11e2-92c6-0019bb2963f4.html"> an economic engine</a>?</p>
<p>Georgia Tech students robbed at <a href="http://www.cbsatlanta.com/story/20593672/two-students-robbed-at-gunpoint">gun point</a>. Word from inside the Tech campus is that students will soon be armed with Robocop-esque minions. Criminals, be wary.</p>
<p>Hospital beds. <a href="http://www.bizjournals.com/atlanta/news/2013/01/15/georgia-senate-committee-approves-tax.html">The perfect thing to tax</a>.</p>
<p>The Bibb County Board of Education just can&#8217;t stay<a href="http://www.macon.com/2013/01/15/2317204/lawsuit-bibb-school-system-illegally.html"> out of litigation</a>. Hopefully, newcomer Jason Downey helps straighten those folks out.</p>
<p><a href="http://www.macon.com/2013/01/15/2317098/buffalos-cafe-in-warner-robins.html">I&#8217;m glad this happened</a>. Likely the worst restaurant of the kind I&#8217;ve been to.</p>
<p><a href="http://www.youtube.com/watch?v=oj9fofFGXKc">For your ears</a>. Let&#8217;s take bets on how long the legislative session lasts.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:63:"http://www.peachpundit.com/2013/01/16/morning-reads-11613/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"6";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:82:"What would happen in terms of federal aid when a hurricane hits the Georgia coast?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:120:"http://www.peachpundit.com/2013/01/15/what-would-happen-in-terms-of-federal-aid-when-a-hurricane-hits-the-georgia-coast/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:129:"http://www.peachpundit.com/2013/01/15/what-would-happen-in-terms-of-federal-aid-when-a-hurricane-hits-the-georgia-coast/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 04:06:15 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50761";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:314:"By my count, all but one Republican member from Georgia of the U.S. House voted against Hurricane Sandy aid this evening. Jack Kingston was the lone exception, but he is listed as not voting. So what happens if we have more or less the same delegation in Washington when a significant hurricane with a strong [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Bill Dawers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2389:"<p></p><p>By my count, all but one Republican member from Georgia of the U.S. House <a href="http://clerk.house.gov/evs/2013/roll023.xml" target="_blank">voted against</a> Hurricane Sandy aid this evening. Jack Kingston was the lone exception, but he is listed as not voting. </p>
<p>So what happens if we have more or less the same delegation in Washington when a significant hurricane with a strong storm surge hits the Georgia coast?</p>
<p>It&#8217;s going to happen eventually. As I <a href="http://www.billdawers.com/2011/08/23/recapping-the-history-of-major-hurricanes-category-3-hitting-the-georgia-coast/" target="_blank">wrote about</a> in 2011, no major hurricanes &#8212; defined as category 3 to 5 &#8212; made landfall on the Georgia coast in the 20th century, but there were <em>three</em> in the latter half of the 19th century. Also in 2011, The Weather Channel <a href="http://www.weather.com/outlook/weather-news/news/articles/2011-hurricane-overdue-cities_2011-05-27?page=3" target="_blank">dubbed Savannah the 4th most overdue city</a> in the country to get hit by a hurricane. The only other city on the East Coast ahead of us on that list was New York City.<span id="more-50761"></span></p>
<p>There seem to be some geographical reasons for Georgia&#8217;s charmed 20th century, but the luck isn&#8217;t going to hold out forever. Especially since the Hurricane Floyd evacuation was a nightmare, far too many Chatham County residents are likely to stay put next time there&#8217;s a serious threat. In a category 2 hurricane, the islands &#8212; Tybee, Wilmington, Whitemarsh, etc. &#8212; would get significant flooding and racing waters. In a category 4 or 5 hurricane, the vast majority of the county would be inundated. </p>
<p>And that&#8217;s just Chatham County. Given the low-lying counties along Georgia&#8217;s coast, a bad storm with a strong surge could devastate a large area.</p>
<p>Given a state budget in the range of $20 billion, there&#8217;s simply no way either state or local funding could lead the coast to a speedy recovery. The sticking point regarding the Sandy vote tonight seems to have been a desire to make sure there are budget cuts elsewhere to compensate for the federal aid to affected states. Will our representatives similarly demand that the rest of the nation make sacrifices so that we can receive aid when that storm hits?</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:125:"http://www.peachpundit.com/2013/01/15/what-would-happen-in-terms-of-federal-aid-when-a-hurricane-hits-the-georgia-coast/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"47";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Whaat?  Hell NO on 2.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"http://www.peachpundit.com/2013/01/15/whaat-hell-no-on-2-0/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:68:"http://www.peachpundit.com/2013/01/15/whaat-hell-no-on-2-0/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 22:00:10 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50750";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:307:"Jim Galloway brings a quick rundown of new legislation the Senate Democrats are pushing.   The one that caught me was a bill to reduce the GPA requirement for HOPE grants to 2.o. Are you just trying to get some media, Senator Carter?  I went to one of the roughest schools in the county and managed [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Bridget Cantrell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:913:"<p></p><p>Jim Galloway brings a <a href="http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/your-daily-jolt-senate-dems-to-push-back-on-ethics-abortion-hope-and-foreclosures/" target="_blank">quick rundown</a> of new legislation the Senate Democrats are pushing.   The one that caught me was a bill to reduce the GPA requirement for HOPE grants to 2.o.</p>
<p>Are you just trying to get some media, Senator Carter?  I went to one of the roughest schools in the county and managed to graduate with over a 4.0 and medium effort.</p>
<p>2.0 people should not be getting HOPE, sir. If they don&#8217;t apply themselves more than that in high school, all the taxpayers are doing is giving them a free year of school because you know they&#8217;re not going to complete <span style="color: #000000"><del>a degree</del> </span>the postsecondary program of their choice.</p>
<p>Yeah &#8211; I said it.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:64:"http://www.peachpundit.com/2013/01/15/whaat-hell-no-on-2-0/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"48";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"Lifeline Revisited";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"http://www.peachpundit.com/2013/01/15/lifeline-revisited/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:66:"http://www.peachpundit.com/2013/01/15/lifeline-revisited/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 17:09:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50479";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:355:"UPDATE: According to Chairman Eaton&#8217;s office, despite opposition from regulated entities and perhaps some others, the below initiative has passed. The PSC has required providers to charge Lifeline users $5/month, taking a real step to rid this essential program of fraud. Link to come. Just a bit of a bump&#8230; Right now, the Georgia Public [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Stefan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2050:"<p></p><p>UPDATE: According to Chairman Eaton&#8217;s office, despite opposition from regulated entities and perhaps some others, the below initiative has passed. The PSC has required providers to charge Lifeline users $5/month, taking a real step to rid this essential program of fraud. Link to come.</p>
<p>Just a bit of a bump&#8230;</p>
<p>Right now, the Georgia Public Service Commission is hearing arguments for and against the imposition of a $5 minimum charge on Lifeline telephone service.  As previously discussed <a href="http://www.peachpundit.com/2012/09/27/its-not-lifeline-awareness-week/">here</a>, the Lifeline program is funded by the Federal Universal Service Fund and provides a $9.25 discount to low-income individuals that qualify for other Federal programs.  Certain phone companies have used the Federal program, which is administered by the PSC, to offer free cell phones and a small bucket of minutes.  The program is rife with <a href="http://www.cbsatlanta.com/story/17048072/your-money-wasted-fraud-in-free-cell-phone-program">fraud and abuse</a> and the $5 minimum charge would cure these problems while keeping the program in place for those that need it.</p>
<p>However, the powers that be would like to continue to loot the fund at a rate of $9.25 per line, and it is in their interest to keep giving out &#8220;free&#8221; phones that are never used.  And it is in the best interest of the &#8220;consumer&#8221; to get as many phones as possible. So between the two of them, we are finding out where exactly Quantity Demanded goes when Price hits Zero. This vote was pushed back for months, not sure why, but it gives the commissioners a second chance to do the right thing. Hint: it&#8217;s not the thing Tracfone wants them to do.</p>
<p><span id="more-50479"></span><img class="aligncenter" src="http://earthenergyreader.files.wordpress.com/2012/12/supply-and-demand-graph.png?w=690" alt="" width="517" height="517" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div></div>
<p>&nbsp;</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:62:"http://www.peachpundit.com/2013/01/15/lifeline-revisited/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"42";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:52:"Governor Asks Legislature To Abdicate Responsibility";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:91:"http://www.peachpundit.com/2013/01/15/governor-asks-legislature-to-abdicate-responsibility/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:100:"http://www.peachpundit.com/2013/01/15/governor-asks-legislature-to-abdicate-responsibility/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 16:00:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50729";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:362:"Today&#8217;s Courier Herald Column: Monday, the Georgia General Assembly convened for the first of its 40 days.  It’s clear that budget issues will dominate the early part of this session, with uncertainty in Washington threatening to delay completion of the budget and threatening to extend the legislature well into the spring. The Governor on Monday [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"Charlie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4608:"<p></p><p><em>Today&#8217;s Courier Herald Column:</em></p>
<p>Monday, the Georgia General Assembly convened for the first of its 40 days.  It’s clear that budget issues will dominate the early part of this session, with uncertainty in Washington threatening to delay completion of the budget and threatening to extend the legislature well into the spring.</p>
<p>The Governor on Monday made a move to limit the amount of budget uncertainty, proposing to convert the responsibility of issuing the “hospital bed tax” to the Department of Community Health.  This would, in concept, allow lawmakers to sidestep the pledge most Republicans have made to Americans for Tax Reform not to raise taxes.  Instead, lawmakers would just be delegating their constitutional authority to unelected bureaucrats.</p>
<p>A major current revenue stream of roughly one half billion dollars to the state treasury would be protected, Republicans would get to honor their pledge, and hospitals would be subject to taxation by a board on which they have no representation.</p>
<p>This is no way to govern.<span id="more-50729"></span></p>
<p>Legislators who are elected to serve the people of Georgia have certain responsibilities.  It’s been clear for some time that the priority of those responsibilities of those elected have become skewed, as evidenced by the number of legislators who have been begging for and receiving other state jobs which are more lucrative than the ones they asked and received the public’s trust to get.</p>
<p>Now to avoid breaking a pledge to another unelected player in this process, these same legislators are being asked by the Governor to cede their duty to a board appointed by him so that we can have budget certainty.  Legislators should not abdicate this responsibility.</p>
<p>Republicans who have a hard time trying to decide if they are able to match the whims of ATR’s Grover Norquist need to ask themselves a counterbalancing test question.  If this were the federal government would they so eagerly vote to give the same powers to President Barack Obama?</p>
<p>Frankly, that is a conceptual question any legislator of either party should ask themselves any time they are about to vote to increase the power of government or delegate authority from the legislative process to appointed bureaucrats.  If a new power is to be created or delegated, would they want this power to be in the hands of their worst political enemies?</p>
<p>Our American system of government is that of a pendulum.  Alliances change. Parties in power change.  But the powers given to government – almost always passed as “temporary” measures, are never again ceded to the people.  Do they want these powers they create or cede in the hands of those whom they do not trust, or who they are diametrically opposed to philosophically?</p>
<p>Republicans must begin to distance themselves from the inconsistent pronouncements of Norquist of what does and doesn’t constitute a tax (which is often defined by whether or not Norquist was brought in to be part of negotiations or not), and get back to determining for themselves what constitutes good public policy.  Ducking tough issues and votes by abdicating responsibility entrusted by the people is shameful and a political gimmick Republicans are likely to regret.</p>
<p>To his credit, Norquist has already referred to the Governor’s power grab as “a step in the wrong direction.”  On this we agree.</p>
<p>But this is also time to take on the random nature of Norquist and American Tax Reform’s policies head on.  Norquist, who during the recent Fiscal Cliff negotiations proclaimed not continuing temporary tax cuts was tantamount to a tax increase, contends that extending a temporary tax here in Georgia is also a tax increase.  Continuing the status quo that he likes is good.  Continuing the status quo that he doesn’t like is bad.</p>
<p>If Republicans must find a way to placate Norquist, then slice a decimal place of the existing bed tax rate and demand it be called a tax cut.  It’s equally senseless in the amount of gymnastics that politicians are willing to go through to avoid charges of increasing taxes, but Republicans may be able to sleep well at night if they renew the tax at 1.44% of hospital revenue instead of 1.45%.</p>
<p>At least then they could tell themselves they “cut” taxes.  They wouldn’t have to tell themselves that they are too feckless and powerless to do what the people send them to Atlanta to do, and that they had to allow appointed bureaucrats to do it for them.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:96:"http://www.peachpundit.com/2013/01/15/governor-asks-legislature-to-abdicate-responsibility/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Jason Carter tweets about gerrymandering";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:79:"http://www.peachpundit.com/2013/01/15/jason-carter-tweets-about-gerrymandering/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:88:"http://www.peachpundit.com/2013/01/15/jason-carter-tweets-about-gerrymandering/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 14:43:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50738";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:252:"A tweet from State Senator Jason Carter a few moments ago: #GaLegislature day 2. Ds are 45% of State but gerrymandering means 30% of the Senate; and now 20% of major Senate committees #partisanship &#8212; Jason Carter (@SenatorCarter) January 15, 2013";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Bill Dawers";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:533:"<p></p><p>A tweet from State Senator Jason Carter a few moments ago:</p>
<blockquote class="twitter-tweet tw-align-center"><p><a href="https://twitter.com/search/%23GaLegislature">#GaLegislature</a> day 2. Ds are 45% of State but gerrymandering means 30% of the Senate; and now 20% of major Senate committees <a href="https://twitter.com/search/%23partisanship">#partisanship</a></p>
<p>&mdash; Jason Carter (@SenatorCarter) <a href="https://twitter.com/SenatorCarter/status/291192048516755456">January 15, 2013</a></p></blockquote>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:84:"http://www.peachpundit.com/2013/01/15/jason-carter-tweets-about-gerrymandering/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"13";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"Enough Of This Anti-Stadium Silliness, Already";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"http://www.peachpundit.com/2013/01/15/enough-of-this-anti-stadium-silliness-already/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:93:"http://www.peachpundit.com/2013/01/15/enough-of-this-anti-stadium-silliness-already/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 14:00:25 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50727";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:353:"Polls show Georgians don&#8217;t like it. Polls show Atlantans don&#8217;t like it. Georgia Common Cause opposes it, as does the Tea Party and several people I know, some of whom I respect. But they&#8217;re all wrong on this deal to replace the Georgia Dome with a new stadium. First, let&#8217;s address this &#8220;public money&#8221; nonsense. [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Mike Hassinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:7045:"<p></p><p>Polls show Georgians <a href="http://www.ajc.com/news/news/falcons-stadium-will-be-a-hard-sell-legislators-sa/nTtWy/">don&#8217;t like it.</a> Polls show <a href="http://www.ajc.com/news/news/falcons-stadium-will-be-a-hard-sell-legislators-sa/nTtWy/"><em>Atlantans</em></a> don&#8217;t like it. Georgia Common Cause <a href="http://commoncausega.org/2012/12/10/public-benched-on-stadium-deal/">opposes it</a>, as does the <a href="http://gareport.com/blog/2012/12/10/stadium-deal-could-be-game-over-for-deal-administration/">Tea Party</a> and several people I know, some of whom I <a href="http://www.peachpundit.com/2013/01/14/falcons-blow-early-leadon-new-stadium/">respect</a>. But they&#8217;re all wrong on this deal to replace the Georgia Dome with a new stadium.</p>
<p><span style="font-size: 13px;">First, let&#8217;s address this &#8220;public money&#8221; nonsense. The Georgia World Congress Center Authority has the legal power to issue bonds, (about $200 million worth) to fund things related to the Georgia World Congress Center &#8220;campus,&#8221; which includes the Congress Center, Centennial Olympic Park and The Georgia Dome. The <a href="http://www.gwcc.com/about/Default.aspx"><strong><em>job</em></strong></a> of the authority is to bring convention, sports and entertainment events to downtown Atlanta, and it&#8217;s been doing that since 1976 when the <a href="http://www.atlanta.convention-center.org/hotels-near/georgia-world-congress-center-history.htm">Congress Center hosted the Bobbin Convention</a>. From time to time the Georgia legislature put state dollars into various projects on or near the campus -sometimes for direct purchases of land, sometimes for improvements, sometimes for parking -the &#8220;stuff&#8221; you need to have from time to time keep one of the &#8220;<a href="http://www.gwcc.com/about/Default.aspx">top five biggest convention destinations in the country [and] one of the best sports and entertainment campuses in the world</a>&#8221; being &#8220;<a href="http://en.wikipedia.org/wiki/Georgia_World_Congress_Center">biggest</a>,&#8221; and the &#8220;best.&#8221; <span id="more-50727"></span></span></p>
<p><span style="font-size: 13px;">In 1989 the current dome was built to accommodate the demands of then-owner Rankin Smith, because Smith wanted a bigger share of the parking and concession revenues than he was getting by sharing the old Atlanta-Fulton County stadium with the Braves. <a href="http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-3557">So the legislature and the governor crafted a deal whereby the legislature let the GWCC borrow $210 million to build the Dome, and let the City of Atlanta pay the borrowers back with revenue generated by a hotel-motel tax</a>. So, are we clear on that? The <a href="http://www.legis.ga.gov/legislation/en-US/display/33181">ONLY reason the hotel-motel tax exists is to pay back money borrowed to build and improve things at the Congress Center, Centennial Olympic Park and the Dome</a>. And since 1989, Georgia hasn&#8217;t put a bunch of direct &#8220;State tax&#8221; dollars into those facilities -it&#8217;s been revenue from Atlanta&#8217;s (and Fulton County&#8217;s) hotel-motel tax. And that&#8217;s better than using taxes that all Georgians pay (as they used to do) to create &#8220;economic benefit&#8221; for Atlanta and the surrounding area. (For you low-information types, &#8220;economic benefit&#8221; = &#8220;jobs.&#8221;)</span></p>
<p><span style="font-size: 13px;">Well, then, how big is that economic benefit? Former Governor Zell Miller claimed it was $10 Billion -but when <a href="http://www.politifact.com/georgia/statements/2012/dec/21/zell-miller/stadium-claim-flagged-faulty-numbers/">Politifact fact-checked him, did some of their own calculations, contacted Georgia State University economist Bruce Seaman, and came up with somewhere between $5 billion and $7.5 billion</a>. Assuming the lowest possible number available, Atlanta and the surrounding area gets a $5 billion boost in economic activity because we let the GWCC Authority borrow money to make the Congress Center &#8220;campus&#8221; attractive to sporting, convention and entertainment events -and then pay those bonds back with revenues from a hotel-motel tax, which is mostly levied on visitors. Yes, it&#8217;s a tax, and yes, it&#8217;s technically &#8220;public&#8221; money that could be used for something else -if Atlanta wanted to use it for something else. (They don&#8217;t.) But as a means of paying back bondholders, levied <strong>only</strong> on those folks who choose to stay in a hotel in Atlanta and originally authorized <strong>only</strong> for the specific purpose of paying for improvements to the things the GWCC Authority owns and operates, it&#8217;s about as far from &#8220;tax money&#8221; as a revenue stream could be and still be called &#8220;public.&#8221; Put it this way: I live in DeKalb County, and have no reason to stay overnight in an Atlanta hotel. I&#8217;ll <strong>never</strong> be charged a <strong>DIME</strong> of taxes to pay for this new stadium -and neither will most of the people in Georgia- but we&#8217;ll all see more economic activity because of the conventions, trade shows, concerts and sporting events the venue attracts. </span></p>
<p><span style="font-size: 13px;">And as for Common Cause&#8217;s objection that this process has been devoid of &#8220;<a href="http://www.ajc.com/news/news/atlantans-want-more-info-on-stadium-deal/nTGHF/">meaningful public input…</a>&#8221; well, go back and click those links. They&#8217;re to publicly available news sources, <a href="http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-3557">The New Georgia Encyclopedia</a> and the Georgia World Congress Center&#8217;s <a href="http://www.gwcc.com/about/stadium/Resources.aspx">website</a>, which even has a set of <a href="http://www.gwcc.com/about/stadium/Default.aspx">chronological links to <strong>all</strong> the documents about this deal</a>. There&#8217;s a <a href="http://www.wsbtv.com/news/falcons-stadium-timeline/">public timeline from the biggest media organization in the State</a> for crying out loud! The <a href="http://www.gwcc.com/about/stadium/Resources.aspx">facts are out there</a> -and have been since the beginning. As my grandmother used to scold us when we didn&#8217;t arrive promptly at the dinner table, &#8220;<em>Whaddya want, a gilt-edged invitation</em>?&#8221; </span></p>
<p><span style="font-size: 13px;">Yes, there are <a href="http://wabe.org/post/coalition-wants-hotelmotel-tax-revenue-marta">LOTS</a> of things that Atlanta and the legislature could spend $300 million on -but none of those other things were the reason for the hotel-motel tax. The reason for that tax was to build the Dome. There&#8217;s no denying the Dome benefits Atlanta, the Metro region and Georgia. And creates jobs without raising taxes. </span></p>
<p>-Isn&#8217;t that what we <em><strong>want</strong></em> the legislature to do?</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:89:"http://www.peachpundit.com/2013/01/15/enough-of-this-anti-stadium-silliness-already/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"72";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:45:"Morning Reads for Tuesday, January 15th, 2013";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:82:"http://www.peachpundit.com/2013/01/15/morning-reads-for-tuesday-january-15th-2013/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:91:"http://www.peachpundit.com/2013/01/15/morning-reads-for-tuesday-january-15th-2013/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 13:08:20 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Politics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50710";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:412:"Georgia: Legislative session opens with dissent (FloridaTimesUnion) Mixed views on Deal&#8217;s Bed Tax Circumvent (AJC) Atlanta school board considers its own police  (AJC) &#8220;Around Town&#8221; wishes local pol lunacy would cease (MDJ) A year from now, Savannah&#8217;s Mayor hopes to preen (SavannahMorningNews) Calvin Smyre remains the Dean (Columbus) Pat Gardner says Medicaid expansion, not cuts [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Stefan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4064:"<p></p><p>Georgia:</p>
<p><img class="alignnone" src="http://www.gpb.org/files/imagecache/newsArticle/news/images/body/photo_of_state_house_of_reps.jpg" alt="" width="360" height="288" /></p>
<ul>
<li>Legislative session opens with dissent (<a href="http://jacksonville.com/news/georgia/2013-01-14/story/georgia-legislative-session-opens-political-sparring-first-day">FloridaTimesUnion</a>)</li>
<li>Mixed views on Deal&#8217;s Bed Tax Circumvent (A<a href="http://www.ajc.com/news/news/state-regional-govt-politics/bed-tax-plan-practical-solution-or-cop-out/nTwwr/">JC</a>)</li>
<li>Atlanta school board considers its own police  (<a href="http://www.ajc.com/news/news/local-education/atlanta-school-board-considers-own-police-force/nTwzc/">AJC</a>)</li>
<li>&#8220;Around Town&#8221; wishes local pol lunacy would cease (<a href="http://www.mdjonline.com/view/full_story/21392035/article-Around-Town--Catching-grief---and-not-just-from-the-left?instance=special%20_coverage_right_column">MDJ</a>)</li>
<li>A year from now, Savannah&#8217;s Mayor hopes to preen (<a href="http://savannahnow.com/news/2013-01-14/savannahs-mayor-closes-first-year-looks-second#.UPVJOm-AEXw">SavannahMorningNews</a>)</li>
<li>Calvin Smyre remains the Dean (<a href="http://www.ledger-enquirer.com/2013/01/12/2342411/calvin-smyre-begins-39th-year.html">Columbus</a>)</li>
<li>Pat Gardner says Medicaid expansion, not cuts (<a href="http://saportareport.com/blog/2013/01/georgia-facing-healthcare-fiscal-cliff-if-it-refuses-medicaid-expansion/">Saportareport</a>)</li>
<li>Tom Baxter on Gold Dome nuts (<a href="http://saportareport.com/blog/2013/01/the-wacky-doo-legislature-comes-back-to-town/">Saportareport</a>)</li>
</ul>
<p>National:</p>
<p><img class="alignnone" src="http://thinkprogress.org/wp-content/uploads/2012/11/congress1.jpg" alt="" width="600" height="343" /></p>
<ul>
<li>Keynes, trains and automobiles (<a href="http://www.economist.com/news/finance-and-economics/21569435-can-fiscal-and-monetary-splurge-reboot-japans-recessionary-economy-keynes?fsrc=rss%7Cfec" target="_blank">The Economist</a>)</li>
<li>The Science of Why We Don’t Believe Science-based Appeals (<a href="http://www.motherjones.com/politics/2011/03/denial-science-chris-mooney" target="_blank">Mother Jones</a>)</li>
<li>False balance: Fox News demands a recount on US’ warmest year (<a href="http://arstechnica.com/science/2013/01/false-balance-fox-news-demands-a-recount-on-us-warmest-year/" target="_blank">arstechnica</a>)</li>
<li>For the ‘Party of Eisenhower and Reagan’, shed a tear (<a href="http://www.theatlantic.com/international/archive/2013/01/no-longer-the-party-of-eisenhower-and-reagan/267092/" target="_blank">The Atlantic</a>)</li>
<li>This Isn’t the Petition Response You’re Looking For (<a href="https://petitions.whitehouse.gov/response/isnt-petition-response-youre-looking">White House</a>)</li>
<li>Chuck Hagel: On Foreign Policy and More (<a href="http://www.foreignaffairs.com/articles/59921/chuck-hagel/a-republican-foreign-policy?page=show" target="_blank">Foreign Affairs</a>)</li>
<li>Everything We Know So Far About A Drone Strike (<a href="http://www.propublica.org/article/everything-we-know-so-far-about-drone-strikes" target="_blank">ProPublica</a>)</li>
<li>Chuck Hagel: On Foreign Policy and More (<a href="http://www.foreignaffairs.com/articles/59921/chuck-hagel/a-republican-foreign-policy?page=show" target="_blank">Foreign Affairs</a>)</li>
<li>What no ethics laws really looks like (<a href="http://www.chicagomag.com/Chicago-Magazine/January-2013/Illinois-Lawmakers-Gone-Wild/?src=longreads">ChicagoMagazine)</a></li>
<li>Clarence Thomas Finally Speaks (<a href="http://www.cnn.com/2013/01/14/justice/thomas-court-speaks/index.html?hpt=hp_t3">CNN</a>)</li>
<li>President Bush finally home after hospital stay of seven weeks (<a href="http://www.reuters.com/assets/print?aid=USBRE90D0YN20130114">Reuters</a>)</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>(It&#8217;s MLK&#8217;s bday today, I know, we&#8217;ll wait for Monday)</p>
<p>&nbsp;</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:87:"http://www.peachpundit.com/2013/01/15/morning-reads-for-tuesday-january-15th-2013/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"15";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:41:"
		
		
		
		
		
				

		
		
			
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Senate Chairmanships Announced";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.peachpundit.com/2013/01/14/senate-chairmanships-announced/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:78:"http://www.peachpundit.com/2013/01/14/senate-chairmanships-announced/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 02:02:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:11:"Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://www.peachpundit.com/?p=50699";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:369:"Posted without comment. For now. Agriculture: Sen. John Wilkinson (R – Toccoa) Appropriations: Sen. Jack Hill (R – Reidsville) Banking: Sen. John Crosby (R – Tifton) Economic Development: Sen. Frank Ginn (R – Danielsville) Education and Youth: Sen. Lindsay Tippins (R – Marietta) Ethics: Sen. Rick Jeffares (R – Locust Grove) Finance: Sen. Judson Hill [...]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Obi's Sister";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1703:"<p></p><p>Posted without comment.  For now.</p>
<p>Agriculture: Sen. John Wilkinson (R – Toccoa)<br />
Appropriations: Sen. Jack Hill (R – Reidsville)<br />
Banking: Sen. John Crosby (R – Tifton)<br />
Economic Development: Sen. Frank Ginn (R – Danielsville)<br />
Education and Youth: Sen. Lindsay Tippins (R – Marietta)<br />
Ethics: Sen. Rick Jeffares (R – Locust Grove)<br />
Finance: Sen. Judson Hill (R – Marietta)<br />
Government Oversight: Sen. Bill Heath (R – Bremen)<br />
Health and Human Services: Sen. Renee Unterman (R – Buford)<br />
Higher Education: Sen. Bill Cowsert (R – Athens)<br />
Insurance and Labor: Sen. Tim Golden (R – Valdosta)<br />
Interstate Cooperation: Sen. Hardie Davis (D – Augusta)<br />
Judiciary (Civil): Sen. Josh McKoon (R – Columbus)<br />
Judiciary (Non-Civil): Sen. Jesse Stone (R – Waynesboro)<br />
Natural Resources and Environment: Sen. Ross Tolleson (R – Perry)<br />
Public Safety: Sen. Buddy Carter (R – Pooler)<br />
Reapportionment and Redistricting: Sen. Don Balfour (R – Snellville)<br />
Regulated Industries: Sen. Jack Murphy (R – Cumming)<br />
Retirement: Sen. Fran Millar (R – Dunwoody)<br />
Rules: Sen. Jeff Mullis (R – Chickamauga)<br />
Science and Technology: Sen. Barry Loudermilk (R – Cassville)<br />
Special Judiciary: Sen. Curt Thompson (D – Tucker)<br />
State and Local Government Operations: Sen. William Ligon (R – Brunswick)<br />
State Institutions: Sen. John Albers (R – Roswell)<br />
Transportation: Sen. Steve Gooch (R – Dahlonega)<br />
Urban Affairs: Sen. Ronald Ramsey (D – Decatur)<br />
Veterans, Military and Homeland Security: Sen. Ed Harbison (D –Columbus)</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:74:"http://www.peachpundit.com/2013/01/14/senate-chairmanships-announced/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"25";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:32:"http://www.peachpundit.com/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:6:"hourly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:12:{s:6:"server";s:16:"cloudflare-nginx";s:4:"date";s:29:"Thu, 17 Jan 2013 18:42:01 GMT";s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:10:"connection";s:10:"keep-alive";s:4:"vary";s:15:"Accept-Encoding";s:12:"x-powered-by";s:10:"PHP/5.3.13";s:15:"x-cf-powered-by";s:8:"WP 1.3.7";s:10:"x-pingback";s:37:"http://www.peachpundit.com/xmlrpc.php";s:13:"last-modified";s:29:"Thu, 17 Jan 2013 18:00:46 GMT";s:10:"set-cookie";s:124:"__cfduid=dfc84ff857cf8ba6dd441da791c75d7b11358448121; expires=Mon, 23-Dec-2019 23:50:00 GMT; path=/; domain=.peachpundit.com";s:6:"cf-ray";s:15:"325acf75d9304fb";s:16:"content-encoding";s:4:"gzip";}s:5:"build";s:14:"20121206165648";}