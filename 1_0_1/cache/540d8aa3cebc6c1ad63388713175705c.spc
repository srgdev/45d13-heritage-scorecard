a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:47:"
	
	
	
	
	
	
	
			
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:17:"Political Insider";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:96:"http://blogs.ajc.com/political-insider-jim-galloway?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:67:"From the ATL to DC with Jim Galloway: Because all politics is local";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 17:41:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:29:"http://wordpress.org/?v=2.8.4";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:36:"
		
		
		
		
		
				
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:39:"The Union of Georgia Elected Officials?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:147:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/the-union-of-georgia-elected-officials/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:111:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/the-union-of-georgia-elected-officials/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 17:18:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:6:"ethics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27533";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1049:"<p>At the tail end of Thursday’s state of the state address, Gov. Nathan Deal weighed into the debate over ethics at the state Capitol.</p>
<p>Never mind the dig at the press corps. The governor may have frozen the discussion by declaring that any rules applied to state lawmakers should also apply to every elected official in Georgia. Emphasis mine:</p>
<blockquote><p>”I will conclude my remarks on a topic that does not require the recitation of statistics, but is one that is recognized in both the public and private domains as a cornerstone of success – that is ethics. We can build the strongest foundations of frugality, efficiency and competitiveness upon which our state government will rest; but if the citizens of Georgia don’t trust us, it will all be in vain, for the vibrations of distrust will crack even the strongest foundations.</p>
<p>“There will always be those in the media and elsewhere who thrive on sowing the seeds of doubt and distrust and who will never recant their sinister innuendos &#0133;</p></blockquote>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:107:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/the-union-of-georgia-elected-officials/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:33:"
		
		
		
		
		
				

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"Your daily jolt: Paul Broun, Saxby Chambliss trade jabs on spending";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:174:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/your-daily-jolt-paul-broun-saxby-chambliss-trade-jabs-on-spending/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:138:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/your-daily-jolt-paul-broun-saxby-chambliss-trade-jabs-on-spending/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Jan 2013 14:38:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:13:"Uncategorized";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27528";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1175:"<p>You have to wonder if the first salvo in the 2014 race for the U.S. Senate, between Republican incumbent Saxby Chambliss and presumed primary challenger Paul Broun, has just been fired. Roll Call, the D.C. newspaper, has these paragraphs from <a href="http://atr.rollcall.com/georgia-paul-broun-dings-saxby-chambliss-on-taxes-spending-senator-hits-back/">an interview with the Athens congressman:</a></p>
<blockquote><p>Asked whether Chambliss understands the big issues of debt and deficit the country is facing as he does, Broun paused for a moment.</p>
<p>“Doesn’t seem so,” Broun said. “He seems to want to raise taxes on people, and he also wants to continue spending. So I don’t know if he does or not, you’ll have to ask him.”</p>
<p>But pressed whether he was interested in challenging Chambliss in next year’s primary, Broun took a pass on answering. “Right now I’m focusing on trying to get our country to be responsible financially as a nation,” he said. “It’s not time to even think about that.”</p></blockquote>
<p>In the same article, Chambliss denies that the fiscal cliff deal that came out of the Senate amounted to &#0133;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:134:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/17/your-daily-jolt-paul-broun-saxby-chambliss-trade-jabs-on-spending/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:36:"
		
		
		
		
		
				
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:59:"The forbidden topic lurking behind the ethics reform debate";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:161:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/the-forbidden-topic-beneath-the-ethics-reform-debate/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:125:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/the-forbidden-topic-beneath-the-ethics-reform-debate/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 23:11:48 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:6:"ethics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27523";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1028:"<p>However reluctantly, the Legislature has begun a sensitive debate over the freebies that lawmakers accept from those pushing the bills they pass judgment upon.</p>
<p>At the risk of being accused of goal post-moving, allow me to point out that everyone involved – lawmakers, the press, tea partyers, and do-gooders of all stripes – has shied away from the fundamental situation that makes any conversation about ethics reform so difficult.</p>
<p>The topic is so politically volatile that no lawmaker, Republican or Democrat, is allowed to mention the subject – unless it is to douse it with cold water. But here it is in a nutshell: We need to start paying a decent salary to these 236 lawmakers sent to Atlanta each year.</p>
<p>The idea was considered and ultimately discarded by the alliance of conservatives, liberals and civic-minded pushing this year’s $100 cap on gifts from lobbyists to lawmakers.</p>
<p>“They don’t think that anybody is going to buy into it this year,” said Kay Godwin, a &#0133;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:121:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/the-forbidden-topic-beneath-the-ethics-reform-debate/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:42:"
		
		
		
		
		
				
		
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:97:"Nathan Deal to beef up mental health reviews, gives green light to guns for school administrators";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:205:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/nathan-deal-to-beef-up-mental-health-reviews-gives-green-light-to-guns-for-school-administrators/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:169:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/nathan-deal-to-beef-up-mental-health-reviews-gives-green-light-to-guns-for-school-administrators/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 20:17:25 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"Nathan Deal";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:7:"Newtown";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:11:"gun control";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27515";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1054:"<p>Gov. Nathan Deal said Wednesday he will initiate a stricter review of Georgia’s mental health records for gun permit applicants, and gave a green light to legislation that would allow school administrators to carry concealed weapons.</p>
<p>The governor made his remarks during a reporters scrum that included my AJC colleague Greg Bluestein, who filed this:</p>
<blockquote><p>Deal said he’d support procedural changes to ensure state agencies are checking mental-health records of people seeking guns.  “We think that is one area where we need to be more vigilant,” he said. </p>
<p>Deal spokesman Brian Robinson said there’s no legislation tied to the effort, but that the state will beef up its review process to make sure the state is “doing everything it can to protect Georgians’ safety.” </p>
<p>A federal law adopted after the Virginia Tech massacre requires states to share the names of mentally ill people with the national background-check system to prevent them from buying guns. While many states &#0133;</p></blockquote>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:165:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/nathan-deal-to-beef-up-mental-health-reviews-gives-green-light-to-guns-for-school-administrators/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:39:"
		
		
		
		
		
				
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:56:"Hank Johnson: NRA has “invoked racist sensitivities”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:158:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/hank-johnson-nra-has-invoked-racist-sensitivities/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:122:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/hank-johnson-nra-has-invoked-racist-sensitivities/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 19:57:20 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:12:"Barack Obama";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:12:"Hank Johnson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:26:"National Rifle Association";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27508";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1343:"<p>The big news in Washington today is <a href="http://www.latimes.com/news/politics/la-pn-obama-gun-policy-proposals-20130116,0,6710113.story">President Barack Obama&#8217;s roll-out</a> of executive orders and proposed Congressional actions in response to the Sandy Hook Elementary massacre last month.</p>
<p>The proposals are being met with an aggressive counterattack from the National Rifle Association, which called Obama a hypocrite for not calling for armed guards in all schools when his own daughters are protected by armed guards at all times. That being the Secret Service. Here&#8217;s the ad:</p>
<p></p>
<p>U.S. Rep. Hank Johnson, D-Decatur, didn&#8217;t like this one bit. In a conversation with Your Washington Correspondent today, Johnson said the NRA&#8217;s opposition to the Obama proposals &#8212; <a href="http://www.slate.com/blogs/weigel/2013/01/16/read_president_obama_s_new_proposed_executive_orders_and_legislation_on.html">read them all here</a> &#8212; is personal and, in part, racially motivated. Said Johnson:</p>
<blockquote><p>&#8220;They are a tool of the free enterprise system and they, like many of their philosophical friends, don’t want any regulations on anything, and they don’t want the federal government to be involved in any area other than the defense of the nation &#0133;</p></blockquote>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Daniel Malloy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:118:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/hank-johnson-nra-has-invoked-racist-sensitivities/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:39:"
		
		
		
		
		
				
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"Senate Republicans look for Dem help in ‘bed tax’ debate";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:163:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/senate-republicans-look-for-dem-help-in-bed-tax-debate/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:127:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/senate-republicans-look-for-dem-help-in-bed-tax-debate/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 19:40:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"Health care";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:11:"Nathan Deal";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27509";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1069:"<p>My AJC collegue Kristina Torres reports that Thursday’s Senate debate over Gov. Nathan Deal’s proposed new Medicaid funding plan will likely be lively. And necessarily bipartisan.</p>
<p>From Torres:</p>
<blockquote><p>The chamber&#8217;s Republican leaders, who fast-tracked the bill through their usual process in these early days of the legislative session, now need help from the chamber&#8217;s Democratic minority to waive their normal rules and allow Senate Bill 24 to hit the floor.</p>
<p>That’s because the chamber requires a two-thirds majority – or 38 votes – to amend the rules. The Republicans hold 37 seats, at least until a special election Feb. 5 to fill the District 11 seat of former Sen. John Bulloch. And at least one Republican, Josh McKoon of Columbus, voted against SB 24 in committee on Tuesday.</p>
<p>The expected trade-off for Democratic cooperation is a full discussion before the vote – unlike a vote Monday on the chamber’s new rules, which returned power back to Lt. Gov. Casey Cagle over things &#0133;</p></blockquote>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:123:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/senate-republicans-look-for-dem-help-in-bed-tax-debate/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:66:"
		
		
		
		
		
				
		
		
		
		
		
		
		
		
		
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:69:"Your daily jolt: Bloody Marys, stogies dropped from Senate lunch menu";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:176:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/your-daily-jolt-bloody-marys-stogies-dropped-from-senate-lunch-menu/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:140:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/your-daily-jolt-bloody-marys-stogies-dropped-from-senate-lunch-menu/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 16 Jan 2013 14:34:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:12:{i:0;a:5:{s:4:"data";s:15:"Atlanta Falcons";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"Casey Cagle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:8:"Congress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:13:"David Ralston";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:13:"Jack Kingston";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:14:"Johnny Isakson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:10:"Kasim Reed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:8;a:5:{s:4:"data";s:11:"Nathan Deal";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:9;a:5:{s:4:"data";s:10:"Paul Broun";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:10;a:5:{s:4:"data";s:12:"Phil Gingrey";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:11;a:5:{s:4:"data";s:16:"Port of Savannah";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27495";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1062:"<p>By tradition, Coca-Cola has served as the corporate sponsor of a luncheon honoring the president pro tem of the state Senate at the opening of each year’s session.</p>
<p>Also by tradition, the menu has included Bloody Marys and cigars. But no more. We’re told that both alcohol and stogies have been removed from this afternoon’s festivities honoring David Shafer, R-Duluth, the Senate’s newly elected leader.</p>
<p>The reason? The flourishes were thought to clash with the Capitol’s embrace of ethics reform, including a $100 per lawmaker cap on gifts from lobbyists passed by the Senate on Monday.</p>
<p>Or perhaps it was fallout from Coke’s new anti-obesity campaign. Either one.</p>
<p>***
<strong>Speaking of ethics, here’s the exchange that just took</strong> place between House Speaker David Ralston and Lt. Gov. Casey Cagle at this morning’s Eggs &#038; Issues breakfast, sponsored by the Georgia Chamber. My AJC colleague Greg Bluestein said Cagle had just finished bragging about the Senate&#8217;s approval of a $100 &#0133;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:136:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/16/your-daily-jolt-bloody-marys-stogies-dropped-from-senate-lunch-menu/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:39:"
		
		
		
		
		
				
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"Nathan Deal: Dollars for new stadium ‘belong to Atlanta’";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:176:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/nathan-deal-new-stadium-dollars-%e2%80%98belong-to-atlanta%e2%80%99/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:140:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/nathan-deal-new-stadium-dollars-%e2%80%98belong-to-atlanta%e2%80%99/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 21:46:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:3:{i:0;a:5:{s:4:"data";s:15:"Atlanta Falcons";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:11:"Nathan Deal";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27488";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1021:"<p>He didn’t give it his endorsement, but Gov. Nathan Deal on Tuesday laid out some arguments likely to be used by supporters of a new, $1 billion stadium that would serve as a new home for the Atlanta Falcons.</p>
<p>First of all, the governor told my AJC colleague Greg Bluestein that he wants the team to stay put. “I think it’s important for us to keep the Atlanta Falcons in Atlanta. We are proud of them, and they’ve had a great year, and we’re hoping they’re going to go all the way to the Super Bowl.”</p>
<p>Critics of a new stadium point out that the Georgia Dome is only 20 years old. But the governor said that if a new stadium doesn’t replace it, expensive updates would be required. “We do know the existing stadium, even though it looks very good now, within the not too distant future there will be significant repairs and upgrades that will be needed to maintain the current Dome,” he said.</p>
<p>As he has before, Deal said his involvement in the matter was limited, and &#0133;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:136:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/nathan-deal-new-stadium-dollars-%e2%80%98belong-to-atlanta%e2%80%99/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:51:"
		
		
		
		
		
				
		
		
		
		
		
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:84:"Your daily jolt: Senate Dems to push back on ethics, abortion, HOPE and foreclosures";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:190:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/your-daily-jolt-senate-dems-to-push-back-on-ethics-abortion-hope-and-foreclosures/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:154:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/your-daily-jolt-senate-dems-to-push-back-on-ethics-abortion-hope-and-foreclosures/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 14:41:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:7:{i:0;a:5:{s:4:"data";s:11:"Casey Cagle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:16:"HOPE scholarship";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:11:"Health care";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:12:"Phil Gingrey";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:11:"immigration";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:11:"marco rubio";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27480";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1076:"<p>On Feb. 5, when the south Georgia contest to replace John Bulloch is completed, Republicans will have 38 members in a 56-member state Senate.</p>
<p>That will give the GOP a super-majority of two-thirds. Which means Democrats will be a super-minority.</p>
<p>Nonetheless, Senate Democrats this morning intend to push out their agenda for this year’s legislative session. Much of it is blue-skying, but here’s a quick rundown of the new legislation they&#8217;ll attempt:</p>
<p>&#8211; A bill to eliminate all distinctions between the Zell Miller scholarship and other HOPE grants, and to require colleges universities and tech schools to accept HOPE payments as full tuition. Sponsor: Curt Thompson of Tucker;</p>
<p>&#8211; A bill to reduce GPA requirement for HOPE grants to 2.0. Sponsor: Jason Carter of Decatur;</p>
<p>&#8211; A prohibition on the use of handheld devices while driving. Horacena Tate of Atlanta;</p>
<p>&#8211; Repeal of the “fetal pain” bill that the Legislature passed last year, reducing the period during which a woman can seek &#0133;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:150:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/your-daily-jolt-senate-dems-to-push-back-on-ethics-abortion-hope-and-foreclosures/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:36:"
		
		
		
		
		
				
		

		
		
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"Lobbyist alert: Your list of Senate committee assignments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:165:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/lobbyist-alert-your-list-of-senate-committee-assignments/?cxntfid=blogs_political_insider_jim_galloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:129:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/lobbyist-alert-your-list-of-senate-committee-assignments/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Jan 2013 13:03:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:11:"Casey Cagle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:19:"Georgia Legislature";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:60:"http://blogs.ajc.com/political-insider-jim-galloway/?p=27475";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:651:"<p>The Committee on Assignments late Monday released its list of state Senate committee memberships.<a href="https://docs.google.com/file/d/1GXNh2QRwdlOgqhVRaLnOOUSL-yxGoTJYHRrwycPdlH91d-OwaRzKEnZqRCD1/edit"> Click here to read,</a> but I&#8217;m also experimenting with the embed function on Google Docs.</p>
<p>See if it works for you below:</p>
<p></p>
<p><strong>- By Jim Galloway, Political Insider</strong></p>
<p><strong>For instant updates, <a href="http://twitter.com/politicalinsidr">follow me on Twitter,</a> or connect with me <a href="http://www.facebook.com/pages/AJC-Political-Insider/175368598443?ref=ts">on Facebook.</a></strong></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"jgalloway";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:125:"http://blogs.ajc.com/political-insider-jim-galloway/2013/01/15/lobbyist-alert-your-list-of-senate-committee-assignments/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:57:"http://blogs.ajc.com/political-insider-jim-galloway/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:4:"date";s:29:"Thu, 17 Jan 2013 18:42:00 GMT";s:6:"server";s:21:"Apache/2.2.3 (CentOS)";s:13:"cache-control";s:28:"max-age=240, must-revalidate";s:12:"x-powered-by";s:9:"PHP/5.3.0";s:4:"vary";s:6:"Cookie";s:10:"x-pingback";s:62:"http://blogs.ajc.com/political-insider-jim-galloway/xmlrpc.php";s:4:"etag";s:34:""0e9492ceddf1b08c64f982a9db4a33e8"";s:13:"x-server-name";s:4:"php1";s:13:"last-modified";s:29:"Thu, 17 Jan 2013 17:41:39 GMT";s:10:"connection";s:5:"close";s:12:"content-type";s:23:"text/xml; charset=UTF-8";}s:5:"build";s:14:"20121206165648";}