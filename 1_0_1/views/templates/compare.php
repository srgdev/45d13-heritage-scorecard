<a id="pageTop"></a>
<div id="conBox">
    <div id="infoBar">
        <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> Compare</div>
        <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <a href="#"><img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></a></div>
        <?php $this->load->view('templates/compare_bar'); ?>
        <br class="clear">
    </div>
    
    <div id="dataNavBar">
        <h1>COMPARE</h1>
        <div id="dataNav"><a href="#" class="active">HOUSE</a><a href="#" class="">SENATE</a><a href="#" class="active">VOTES</a><a href="#" class="last">CO-SPONSORSHIPS</a></div>
        <div id="dataNavRight">
            <div id="dataSocnet">
                <a href="#" class="tooltip" title="Email This Page"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                <a href="#" class="tooltip" title="Send This Page to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                <a href="#" class="tooltip" title="Send This Page to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
            </div>
        </div>
        <br class="clear">
   </div> <!--end data nav bar box -->
   
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable compareTable">
      <tr>
        <th class="leftTitle logoCell titleCell"><!-- <img src="<?php echo  imagesPath() ?>/logonew.png" width="96" height="48"> --></th>
        <th class="haPosCell leftTitle "></th>
        <th class="spacerCell"></th>
        <?php foreach($housememImgs as $img){ ?>
                    <th class="compareCell titleImg"><img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $img; ?>" width="115" height="125"></th>
        <?php } ?>
        
      </tr>
      <tr class="titleRow">
        <th class="leftTitle logoCell titleCell">TITLE</th>
        <th class="haPosCell leftTitle ">HA POSITION</th>
        <th class="spacerCell"></th>
        <?php foreach($housememNames as $name){ ?>
             <th class="compareCell"><?php echo $name ?></th>
        <?php } ?>
        
      </tr>
      <tr>
        <td class="titleCell">&nbsp;</td>
        <td class="haPosCell ">&nbsp;</td>
        <td class="spacerCell">&nbsp;</td>
        <?php foreach($housememScores as $score){ ?>
            <td class="compareCell "><?php echo $score ?>%</td>    
        <?php } ?>    
      </tr>
      <?php $i = 0; foreach($housevotes as $vote){ ?>
          <tr>
            <td class="titleCell"><?php echo $vote['title']; ?></td>
            <td class="haPosCell orangeCell"><?php if($vote['position'] == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
            <td class="spacerCell">&nbsp;</td>
            <?php foreach($vote['votes'] as $vv) { ?>
                <td class="compareCell <?php if($vv == "Not Voting"){ echo 'greyCell'; }elseif($vv == $vote['position']) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($vv == "Not Voting"){?> <span class="noVote"></span> <?php }elseif($vv == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
            <?php } ?>
            <!-- <td class="compareCell blueCell"><span class="noPos"></span></td> -->
          </tr>
     <?php $i++; } ?>
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br class="clear">
</div> <!-- End conBox -->   
