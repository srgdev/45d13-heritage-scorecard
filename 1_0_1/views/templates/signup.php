<a id="pageTop"></a>        
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> Wathclist <span class="divider"></span> Sign In</div>
                <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
                <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1>WATCHLIST</h1>
            </div> <!--end data nav bar box -->
           <br><br>
           <div class="signinform">
               
               <div class="left login">
                   
                   <?php $attributes = array('id' => 'signin', 'data-validate' => "parsley"); 
                         echo form_open('watchlist/create', $attributes);  ?>
                       <h2>Create Account</h2>
                       <?php echo getMessage(); ?> 
                       <input type="text" name="email" value="" id="email" placeholder="Email Address" data-required="true" data-type="email" data-trigger="change" autocomplete="off"/>
                       <br />
                       <input type="text" name="emailval" value="" id="emailval" placeholder="Verify Email Address" data-required="true" data-type="email" data-trigger="change" data-equalto-message="Email Addresses do not match" data-equalto="#email" autocomplete="off"/>
                       <br />
                       <input type="password" name="password" value="" id="password" placeholder="Password" data-required="true" data-trigger="change" autocomplete="off"/>
                       <br />
                       <input type="submit" value="Create Account" class="gradLightGrey shadow rounded btns"/>
                       <input type="checkbox" name="remember" class="remember" <?php if(isset($_COOKIE["email"])) { if($_COOKIE['email'] != ''){ ?> checked="checked" <?php }} ?> /> <label for="remember">Remember Me</label> 
                   </form>
               </div>
               
               <div class="left signUp">
                   <div class="loginTxt">
                       <div class="loginTxtTop">Get ready to track the Members of Congress you care about:</div>
    
                        1. Use your email address and create a password here. <br />
                        2. Search for Members of Congress on our Legislative Scorecard. <br />
                        3. With one click, add Members of Congress to your watchlist. <br />
                        4. Track their scores and hold them accountable! 
                    </div>
               </div>
               
               
               
               <br class="clear" />
           </div>
           <a href="#pageTop" class="topAnchor">^ Back to top</a>
           <br class="clear">
        </div> <!-- End conBox -->   