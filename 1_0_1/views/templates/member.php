<a id="pageTop"></a>
<div id="conBox">
    <div id="infoBar">
        <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>members?c=<?php echo $member->chamber ?>"><?php echo ucfirst($member->chamber) ?></a>  <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>members?p=<?php echo $member->party ?>"> <?php echo ucfirst(getParty($member->party)) ?></a><span class="divider"></span> <a href="<?php echo base_url().index_page()?>state/state/<?php echo $member->state ?>"><?php echo ucfirst(getState($member->state)) ?></a></div>
        <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
        <?php $this->load->view('templates/compare_bar'); ?>
        <br class="clear">
    </div>
    
    <div id="dataNavBar">
        <h1><?php echo $member->title ?>. <?php echo $member->fName ?> <?php echo $member->lName ?></h1>
        <div id="dataNav"> (<?php echo $member->party ?>) <?php echo $member->state ?> &nbsp;&nbsp;&nbsp; 
            <?php if($member->chamber == 'house'){?>
            DISTRICT <?php echo $member->district ?>
            <?php } ?>
            </div>
        <div id="dataNavRight">
            <div id="dataSocnet">
                <a href="<?php echo base_url().index_page() ?>/watchlist/add/<?php echo $member->congID; ?>" class="tooltip" title="Add <?php echo $member->title.'. '.$member->lName; ?> To Your Watchlist"><img src="<?php echo  imagesPath() ?>/socnet-eye.png" width="15" height="13"></a>
                <a href="#" class="tooltip embed" data-member="<?php echo $member->congID; ?>" title="Embed <?php echo $member->title.'. '.$member->lName; ?> on your site"><img src="<?php echo  imagesPath() ?>/socnet-embed.png" width="15" height="13"></a>
                <a href="<?php memberEmailLink($member->title.". ".$member->fName." ".$member->lName, $member->score) ?>" class="tooltip" title="Email <?php echo $member->title.'. '.$member->lName; ?>"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                <a href="<?php facebookLink(buildTitle($member->title.". ".$member->fName." ".$member->lName.' Results'), $member->title.". ".$member->fName." ".$member->lName." earned a ".$member->score." % on Heritage Action legislative scorecard. See the details and hold you Members of Congress accountable here: ".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send <?php echo $member->title.'. '.$member->lName; ?> to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                <a href="<?php twitterLink($member->title.". ".$member->fName." ".$member->lName." earned a ".$member->score." on @Heritage_Action scorecard: ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send <?php echo $member->title.'. '.$member->lName; ?> to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
            </div>
        </div>
        <br class="clear">
   </div> <!--end data nav bar box -->
   <div class="memalert">
        <?php echo getMessage(); ?>
    </div> 
   <div id="repScoreBox">
      
        <div id="repPic">
            <img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $member->image_path ?>" width="146" height="170">
            <a href="#" class="memCompareAdder" rel="<?php echo $member->congID; ?>" data-chamber="<?php echo $member->chamber; ?>"><img src="<?php echo  imagesPath() ?>/bio-pic_plusbtn.jpg" width="146" height="23"></a>
        </div>
        <?php if(isSentinel($member->score)) { ?>
            <div id="repSentinel"><img src="<?php echo  imagesPath() ?>/bio-logo_Sentinel.png" width="138" height="144"></div>
        <?php } ?>
        <div id="repGraph">
            <div class="scoreLbl"><?php echo $member->title ?>. <?php echo $member->fName ?> <?php echo $member->lName ?></div>
            <div id="repScoreBar" style="width:<?php echo $member->score ?>%; min-width:100px;">
                <div class="scorePercent"><?php echo $member->score ?><span class="thin">%</span></div>
            </div>
            <div class="scoreLbl"><?php echo strtoupper($member->chamber) ?> <?php echo strtoupper(getParty($member->party)) ?> AVERAGE</div>
            <div id="avgScoreBar" style="width:<?php echo $average ?>%;  min-width:100px;">
                <div class="scorePercent"><?php echo $average ?> <span class="thin">%</span></div>
            </div>
            <br class="clear">
            <p class="howcalc"><img src="<?php echo  imagesPath() ?>/icon-exclaim.jpg" width="12" height="12">&nbsp; How scores are calculated</p>
        </div>
        <br class="clear">
   </div>
    
    <h1 class="memTitle">VOTES <span class="thin">(<?php echo $votesRight ?> of <?php echo $votesTotal ?>)</span></h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable repTable">
      <tr class="titleRow">
        <th>TITLE</th>
        <th class="leftTitle descripCell">DESCRIPTION</th>
        <th>HA POSITION</th>
        <th>REP POSITION</th>
      </tr>
      
      <? foreach($votes as $vote){ ?>
          
              <tr rel="<?php echo base_url(); echo index_page(); ?>votes/vote/<?php echo $vote->roll_id?>">
                <td class="titleCell"><?php echo $vote->question ?></td>
                <td class="descripCell"><?php echo trimDescription($vote->description) ?></td>
                <td class="haPosCell orangeCell"><?php if($vote->perferred_vote == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
                <td class="repPosCell <?php if($vote->vote == "Not Voting" || $vote->vote == "Present"){ echo 'greyCell'; }elseif($vote->vote == $vote->perferred_vote) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($vote->vote == "Not Voting" || $vote->vote == "Present"){?> <span class="noVote"></span> <?php }elseif($vote->vote == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
              </tr>
          
      <?php } ?>
      
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br><br>
    
    <h1 class="memTitle">CO-SPONSORSHIPS <span class="thin">(<?php echo $billsRight ?> of <?php echo $billsTotal ?>)</span></h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable repTable">
      <tr class="titleRow">
        <th >TITLE</th>
        <th class="leftTitle descripCell">DESCRIPTION</th>
        <th>HA POSITION</th>
        <th>REP POSITION</th>
      </tr>
      <?php foreach($bills as $bill){ ?>
          <tr rel="<?php echo base_url(); echo index_page(); ?>bills/bill/<?php echo $bill['bill']->bill_id ?>">
            <td class="titleCell"><?php echo $bill['bill']->short_title ?></td>
            <td class="descripCell"><?php echo trimDescription($bill['bill']->description) ?></td>
            <td class="haPosCell orangeCell"><?php if($bill['bill']->sponsor_position == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
            <td class="repPosCell <?php if($bill['mempos'] == $bill['bill']->sponsor_position) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($bill['mempos'] == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
          </tr>
      <?php } ?>
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br class="clear">
</div> <!-- End conBox -->   