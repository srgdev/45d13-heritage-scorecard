<a id="pageTop"></a>        
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> Watchlist </div>
                <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
                <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1>WATCHLIST</h1>
            </div> <!--end data nav bar box -->
           <br><br>
              <!--  <?php foreach($members as $member){
                  print_r($member['votes']); 
               }?> -->
               <div class="watchalert">
                    <?php echo getMessage(); ?>
                </div> 
           <ul id="watchItemsBox">
              <?php foreach($members as $member){ ?>
                <li class="watchItem">
                    <div class="watchWrapper">
                        <div class="watchItemInner">
                            <div class="watchPic">
                                <div class="watchPicHolder">
                                    <img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $member['member']->image_path ?>" width="146" height="170">
                                </div>
                                <a href="#" class="memCompareAdder" rel="<?php echo $member['member']->congID; ?>" data-chamber="<?php echo $member['member']->chamber; ?>"><img src="<?php echo  imagesPath() ?>/bio-pic_plusbtn.jpg" width="146" height="23"></a>
                            </div>
                            <div class="watchInfo">
                                <a href="<?php echo base_url().index_page() ?>/members/member/<?php echo $member['member']->congID; ?>"><div class="watchName gradLightBlue"><?php echo $member['member']->title.'. '.$member['member']->fName.' '.$member['member']->lName; ?>  </div></a>
                                <div class="watchRow">
                                    <div class="watchLoc">(<?php echo $member['member']->party; ?>) <?php echo $member['member']->state; ?><br><?php if($member['member']->district != ''){ echo 'DISTRICT '.$member['member']->district;} ?></div>
                                    <div class="watchScore"><?php echo $member['member']->score; ?><span class="thin">%</span></div>
                                    <br class="clear">
                                </div>
                                <div class="watchRow">
                                    <div class="watchAct">Latest Acivity <img src="<?php echo  imagesPath() ?>/search-info_divider.png" width="6" height="10"></div>
                                    <div class="watchSocnet">
                                        <a href="#" class="tooltip" title="Embed <?php echo $member['member']->title.'. '.$member['member']->lName; ?> on your site"><img src="<?php echo  imagesPath() ?>/socnet-embed.png" width="15" height="13"></a>
                                        <a href="<?php memberEmailLink($member['member']->title.". ".$member['member']->fName." ".$member['member']->lName, $member['member']->score) ?>" class="tooltip" title="Email <?php echo $member['member']->title.'. '.$member['member']->lName; ?>"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                                        <a href="<?php facebookLink(buildTitle($member['member']->title.". ".$member['member']->fName." ".$member['member']->lName.' Results'), $member['member']->title.". ".$member['member']->fName." ".$member['member']->lName." earned a ".$member['member']->score." % on Heritage Action legislative scorecard. See the details and hold you Members of Congress accountable here: ".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send <?php echo $member['member']->title.'. '.$member['member']->lName; ?> to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                                        <a href="<?php twitterLink($member['member']->title.". ".$member['member']->fName." ".$member['member']->lName." earned a ".$member['member']->score." on @Heritage_Action scorecard: ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send <?php echo $member['member']->title.'. '.$member['member']->lName; ?> to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
                                    </div>
                                    <br class="clear">
                                    <div class="right delWatch"><a href="<?php echo base_url().index_page() ?>/watchlist/remove/<?php echo $member['member']->congID; ?>">Remove</a></div>
                                    <br class="clear">
                                </div>
                            </div>
                        <br class="clear" >
                        </div>
                    </div>
                    <div class="watchPointer"></div>
                    <div class="watchDetail dropdown">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable repTable">
                          <tr class="watchHeadingRow">
                            <td colspan="3"><h1>VOTES</h1></td>
                            <td class="spacerCell"></td>
                            </tr>
                          <tr class="titleRow">
                            <th class="titleCell ">TITLE</th>
                            <th class="descripCell leftTitle">DESCRIPTION</th>
                            <th class="haPosCell">HA POSITION</th>
                            <th class="repPosCell">REP POSITION</th>
                          </tr>
                          <?php foreach($member['votes'] as $v){ ?>
                              <tr rel="<?php echo base_url(); echo index_page(); ?>votes/vote/<?php echo $v->roll_id?>">
                                <td class="titleCell"><?php echo $v->question ?></td>
                                <td class="descripCell"><?php echo $v->description ?></td>
                                <td class="haPosCell orangeCell"><?php if($v->perferred_vote == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></span></td>
                                <td class="repPosCell <?php if($v->vote == "Not Voting"){ echo 'greyCell'; }elseif($v->vote == $v->perferred_vote) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($v->vote == "Not Voting"){?> <span class="noVote"></span> <?php }elseif($v->vote == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
                              </tr>
                          <?php } ?>
                          
                        <tr class="watchHeadingRow">
                            <td colspan="3"><h1>CO-SPONSORSHIPS</h1></td>
                            <td class="spacerCell"></td>
                            </tr>
                          <tr class="titleRow">
                            <th class="titleCell ">TITLE</th>
                            <th class="descripCell leftTitle">DESCRIPTION</th>
                            <th class="haPosCell">HA POSITION</th>
                            <th class="repPosCell">REP POSITION</th>
                          </tr>
                           <?php foreach($member['bills'] as $b){ ?>
                              <tr rel="<?php echo base_url(); echo index_page(); ?>bills/bill/<?php echo $b['bill']->bill_id ?>">
                                <td class="titleCell"><?php echo $b['bill']->short_title ?></td>
                                <td class="descripCell"><?php echo $b['bill']->description ?></td>
                                <td class="haPosCell orangeCell"><?php if($b['bill']->sponsor_position == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
                                <td class="repPosCell <?php if($b['mempos'] == $b['bill']->sponsor_position) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($b['mempos'] == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
                              </tr>
                          <?php } ?>
                        </table>
                        <div class="watchClose">^ Hide Activity</div>
                    </div>
                </li>
                <?php } ?>
               
           </ul>
           <a href="#pageTop" class="topAnchor">^ Back to top</a>
           <br class="clear">
        </div> <!-- End conBox -->   