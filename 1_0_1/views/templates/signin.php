<a id="pageTop"></a>        
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> Wathclist <span class="divider"></span> Sign In</div>
                <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
                <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1>WATCHLIST</h1>
            </div> <!--end data nav bar box -->
           <br><br>
           <div class="signinform">
               <div class="left login">
                   <?php $attributes = array('id' => 'signin', 'data-validate' => "parsley"); 
                         echo form_open('watchlist/login', $attributes);  ?>
                       <h2>Sign In</h2>
                       <?php echo getMessage(); ?> 
                       <input type="text" name="email"  id="email" placeholder="Email Address" data-required="true" data-type="email" data-trigger="change" value="<?php if(isset($_COOKIE["email"])) {echo $_COOKIE["email"]; } ?>" />
                       <br />
                       <input type="password" name="password"  id="password" placeholder="Password" data-required="true" data-trigger="change" value="<?php if(isset($_COOKIE["pass"])) {echo $_COOKIE["pass"]; } ?>" autocomplete="off"  />
                       <br />
                       <input type="submit" value="Sign In" class="gradLightGrey shadow rounded btns" />
                       <input type="checkbox" name="remember" class="remember" <?php  if(isset($_COOKIE["email"])) { if($_COOKIE['email'] != ''){ ?> checked="checked" <?php }} ?> /> <label for="remember">Remember Me</label>
                       <br /><br />
                       <a href="<?php echo base_url().index_page() ?>watchlist/signup">Don't have a Watchlist? Create one here.</a>
                       <br />
                       <a href="<?php echo base_url().index_page() ?>watchlist/recover">I forgot My Password.</a>
                   </form>
               </div>
               
               <div class="left signUp">
                   <div class="loginTxt">
                       <div class="loginTxtTop">Want to track the scores of the Members of Congress you care about?</div>
    
                        1. Sign in to your watchlist (If you don't have one, <a href="<?php echo base_url().index_page() ?>watchlist/signup">create one here</a>). <br />
                        2. Search for and add Members of Congress to your watchlist. <br />
                        3. Track their scores and hold them accountable! <br />
                    </div>
               </div>
               
               
               
               <br class="clear" />
           </div>
           <a href="#pageTop" class="topAnchor">^ Back to top</a>
           <br class="clear">
        </div> <!-- End conBox -->   