<a id="pageTop"></a>
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>votes ">Keyvotes</a> <span class="divider"></span><a href="<?php echo base_url().index_page() ?>votes?c=<?php echo $vote->chamber ?> "><?php echo ucfirst($vote->chamber) ?></a>   <span class="divider"></span> <?php echo ucfirst($vote->roll_id); ?></div>
                <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <a href="#"><img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></a></div>
                <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1><?php echo $vote->question ?></h1>
                <div id="dataNav">Vote# <?php echo ucfirst($vote->roll_id); ?></div>
                <div id="dataNavRight">
                    <div id="dataSocnet">
                        <a href="<?php voteEmailLink($vote->question) ?>" class="tooltip" title="Email This Vote"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                        <a href="<?php facebookLink(buildTitle('Vote Results'), "Were your Members of Congress on the conservative side when they voted on ".str_replace("\"", "%22", $vote->question)."? See the conservative position and details from Heritage Action here:".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send This Vote to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                        <a href="<?php twitterLink("Did they vote right on ".str_replace("\"", "%22", $vote->question)."? @heritage_action has the details:  ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send This Vote to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
                    </div>
                </div>
                <br class="clear">
           </div> <!--end data nav bar box -->
           
           <div id="voteInfoBox">
                <div id="voteInfo">
                    <div id="voteResultBox" class="gradLightBlue rounded">
                        Vote Result on <?php echo date_to_human($vote->vote_date, 'Y-m-d') ?> 
                        <div id="voteResultDisplay" class="rounded shadowInner"><?php if($yesvotes > $novotes){ echo 'YES'; }else{echo 'NO'; } ?></div>
                        <span><?php echo $yesvotes; ?> to <?php echo $novotes; ?></span>
                    </div>
                    <?php if($vote->url){ ?>
                        <a href="<?php echo $vote->url ?>" id="voteAlertBtn" class="rounded gradOrange">SEE THE KEY VOTE ALERT</a>
                    <?php } ?>
                </div>
                <div id="infoDescrip">
                <h1>HERITAGE ACTION POSITION: <span class="orange"><?php echo strtoupper($vote->perferred_vote); ?></span></h1>
                <h4>Vote Description</h4>
<p><?php echo $vote->description ?></p>

<h4>Heritage Action Position</h4>
<p><?php echo $vote->ha_position; ?></p>
                </div>
                <br class="clear">
            </div><!-- End Bill Info Box -->
           
           <div id="dataNavBar" class="subNavbar">
                <div id="dataNav" class="voteNav"><a href="#" class="active yesvote">VOTED YES</a><a href="#" class="novote">VOTED NO</a><a href="#" class="notvote last">DIDN'T VOTE</a></div>
                <div id="dataNavRight">
                    <div id="dataSortSwitch"><h4>SORTING OPTIONS</h4> <div id="sortSwitch"><img class="sortKnob" src="<?php echo  imagesPath() ?>/btn-sort_switch.png" width="17" height="17"></div></div>
                </div>
                <br class="clear">
                <div id="dataSortOptions">
                    <div id="dataSortOptionsInner" class="rounded shadowInner">
                        <div id="dropList" class="rounded shadowInner">
                            <select id="sortList" class="sessionList">
                                <option value="name" selected="selected">Name</option>
                                <option value="score">Score</option>
                                <option value="district">District</option>
                                <option value="state">State</option>
                            </select>
                        </div>
                        <div class="sortOptionBox twoOptions">
                            <span><img src="<?php echo  imagesPath() ?>/sort-up.png" width="16" height="16" class="lblAsceDesc" value="ACSE">&nbsp;&nbsp;&nbsp;</span>
                            <select id='sliderAsceDesc'>
                                <option value='ACSE' selected="selected">Ascend</option>
                                <option value='DESC'>Descend</option>
                            </select>
                            <span>&nbsp;&nbsp;&nbsp;<img src="<?php echo  imagesPath() ?>/sort-down.png" width="16" height="16" class="lblAsceDesc" value="DESC"></span>
                        </div>
                        
                        <div class="sortOptionBox threeOptions">
                            <span class="lblRepDem" value="Rep">Rep&nbsp;&nbsp;&nbsp;</span>
                            <select id='sliderRepDem'>
                                <option value='Rep'>Rep</option>
                                <option value='All' selected="selected">All</option>
                                <option value='Dem'>Dem</option>
                            </select>
                            <span class="lblRepDem" value="Dem">&nbsp;&nbsp;&nbsp;Dem</span><br>
                            <span class="labelAll lblRepDem" value="All">All</span>
                        </div>
                        <br class="clear">
                    </div>
                </div> <!-- end Data sort options box -->
           </div> <!--end data nav bar box -->
        
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable overallTable">
              
              <thead>
                  <tr class="titleRow">
                    <th id="state" >STATE</th>
                    <?php if($vote->chamber == 'house') { ?><th id="district" rel="num">DISTRICT</th><?php } ?>
                    <th id="name" class="leftTitle">NAME</th>
                    <th >PARTY</th>
                    <th id="score" rel="num">SCORE</th>
                  </tr>
              </thead>
              
              
              <?php foreach($votes as $s){ ?>
                  <?php $pos = ($s->vote != 'Not Voting') ? $s->vote : 'not'; ?>
                  <tr class="chamberTR partyTR <?php echo $s->chamber?>TR <?php echo $s->party?>TR <?php echo $pos; ?>TR <?php if($pos == 'No' || $pos == 'not'){ echo 'voteHide'; } ?>" rel="<?php echo base_url(); echo index_page(); ?>members/member/<?php echo $s->congID; ?>">
                    <td class="stateCell"><?php echo $s->state ?></td>
                    <?php if($vote->chamber == 'house') { ?><td class="districtCell"><?php echo $s->district ?></td><?php } ?>
                    <td class="nameCell"><!-- <i class="sentIcon"></i> --> <?php echo $s->title ?>. <?php echo $s->fName ?> <?php echo $s->lName ?></td>
                    <td class="partyCell"><?php echo $s->party ?></td>
                    <td class="scoreCell blueCell"><?php echo $s->score ?><span class="thin">%</span></td>
                  </tr>
              <?php } ?>
            </table>
            <a href="#pageTop" class="topAnchor">^ Back to top</a>
            <br class="clear">
        </div> <!-- End conBox -->  