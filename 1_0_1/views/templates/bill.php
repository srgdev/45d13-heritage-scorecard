<a id="pageTop"></a>
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>bills">Co-Sponsorships</a> <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>bills?c=<?php echo strtolower(getBillChamber($bill->billType)) ?>"><?php echo getBillChamber($bill->billType) ?></a>  <span class="divider"></span> <?php echo strtoupper($bill->bill_id); ?></div>
               <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
               <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1><?php echo $bill->short_title ?></h1>
                <div id="dataNav">Bill# <?php echo strtoupper($bill->bill_id) ?></div>
                <div id="dataNavRight">
                    <div id="dataSocnet">
                        <a href="<?php billEmailLink($bill->short_title) ?>" class="tooltip" title="Email This Bill"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                        <a href="<?php facebookLink(buildTitle('Bill Results'), "Conservatives must support ".$bill->short_title.". Thanks to Heritage Action, you can see how your Members of Congress stack up here:".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send This Bill to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                        <a href="<?php twitterLink("See if your Members of Congress support ".$bill->short_title.": ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send This Bill to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
                    </div>
                </div>
                <br class="clear">
           </div> <!--end data nav bar box -->
           
           <div id="billInfoBox">
                <div id="billInfo">
                    <div id="voteCountBox" class="gradLightBlue rounded">
                        <div id="voteCountDisplay" class="rounded shadowInner">
                            <div id="voteCount"><span><?php echo $sponsorTotal ?></span></div>
                        </div>
                        <p>CO-SPONSORED THIS BILL</p>
                    </div>
                    <?php if($bill->url){ ?>
                        <a href="<?php echo $bill->url ?>" id="voteAlertBtn" class="rounded gradOrange">SEE THE KEY VOTE ALERT</a>
                    <?php } ?>
                </div>
                <div id="infoDescrip">
                    <?php $position = ($bill->sponsor_position == 'Yes') ? 'Sponsor' : 'Do Not Sponsor'; ?>
                    <h1>HERITAGE ACTION POSITION: <span class="orange"><?php echo strtoupper($position); ?></span></h1>
                <h4>Bill Description</h4>
                <p><?php echo $bill->description ?></p>

                <h4>Heritage Action Position</h4>
                <p><?php echo $bill->position_txt ?></p>
                </div>
                <br class="clear">
            </div><!-- End Bill Info Box -->
           
           <div id="dataNavBar" class="subNavbar">
                <div id="dataNav" class="voteNav"><a href="#" class="active onBill">ON THE BILL</a><a href="#" class="notBill">NOT ON THE BILL</a><!-- <a href="#" class="last allBill">SEE ALL</a> --></div>
                <div id="dataNavRight">
                    <div id="dataSortSwitch"><h4>SORTING OPTIONS</h4> <div id="sortSwitch"><img class="sortKnob" src="<?php echo  imagesPath() ?>/btn-sort_switch.png" width="17" height="17"></div></div>
                </div>
                <br class="clear">
                <div id="dataSortOptions">
                    <div id="dataSortOptionsInner" class="rounded shadowInner">
                        <div id="dropList" class="rounded shadowInner">
                            <select id="sortList" class="sessionList">
                                <option value="name" selected="selected">Name</option>
                                <option value="score">Score</option>
                                <option value="district">District</option>
                                <option value="state">State</option>
                            </select>
                        </div>
                        <div class="sortOptionBox twoOptions">
                            <span><img src="<?php echo  imagesPath() ?>/sort-up.png" width="16" height="16" class="lblAsceDesc" value="ACSE">&nbsp;&nbsp;&nbsp;</span>
                            <select id='sliderAsceDesc'>
                                <option value='ACSE' selected="selected">Ascend</option>
                                <option value='DESC'>Descend</option>
                            </select>
                            <span>&nbsp;&nbsp;&nbsp;<img src="<?php echo  imagesPath() ?>/sort-down.png" width="16" height="16" class="lblAsceDesc" value="DESC"></span>
                        </div>
                        
                        <div class="sortOptionBox threeOptions">
                            <span class="lblRepDem" value="Rep">Rep&nbsp;&nbsp;&nbsp;</span>
                            <select id='sliderRepDem'>
                                <option value='Rep'>Rep</option>
                                <option value='All' selected="selected">All</option>
                                <option value='Dem'>Dem</option>
                            </select>
                            <span class="lblRepDem" value="Dem">&nbsp;&nbsp;&nbsp;Dem</span><br>
                            <span class="labelAll lblRepDem" value="All">All</span>
                        </div>
                        <br class="clear">
                    </div>
                </div> <!-- end Data sort options box -->
           </div> <!--end data nav bar box -->
        
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable overallTable">
              <thead>
                  <tr class="titleRow">
                    <th id="state" >STATE</th>
                    <th id="district" rel="num">DISTRICT</th>
                    <th id="name" class="leftTitle">NAME</th>
                    <th >PARTY</th>
                    <th id="score" rel="num">SCORE</th>
                  </tr>
              </thead>
              <?php foreach($members as $s){ ?>
                  <?php $billClass = ($s['billcheck'] == 'no') ? 'notBillTR billHide' : 'onBillTR'; ?>
                  <tr class="chamberTR partyTR <?php echo $s['member']->chamber?>TR <?php echo $s['member']->party?>TR <?php echo $billClass; ?>" rel="<?php echo base_url(); echo index_page(); ?>members/member/<?php echo $s['member']->congID; ?>">
                    <td class="stateCell"><?php echo $s['member']->state ?></td>
                    <td class="districtCell"><?php echo $s['member']->district ?></td>
                    <td class="nameCell"><!-- <i class="sentIcon"></i> --> <?php echo $s['member']->title ?>. <?php echo $s['member']->fName ?> <?php echo $s['member']->lName ?></td>
                    <td class="partyCell"><?php echo $s['member']->party ?></td>
                    <td class="scoreCell blueCell"><?php echo $s['member']->score ?><span class="thin">%</span></td>
                  </tr>
              <?php } ?>
            </table>
            <a href="#pageTop" class="topAnchor">^ Back to top</a>
            <br class="clear">
        </div> <!-- End conBox -->   
    