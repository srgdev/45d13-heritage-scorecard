<a id="pageTop"></a>
<div id="conBox">
    <div id="infoBar">
        <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>/members"> Members </a> <span class="divider"></span> <?php echo ucfirst(getState(strtoupper($state))) ?></div>
        <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
         <?php $this->load->view('templates/compare_bar'); ?>
        <br class="clear">
    </div>
    
    <div id="dataNavBar">
        <h1>STATE RESULTS: <?php echo ucfirst(getState(strtoupper($state))) ?></h1>
        <!-- <div id="dataNav"><a href="#">VOTES</a><a href="#" class="active">MEMBERS</a><a href="#" class="last">CO-SPONSORSHIPS</a></div> -->
        <div id="dataNavRight">
            <div id="dataSocnet">
                <a href="<?php emailLink() ?>" class="tooltip" title="Email <?php echo ucfirst(getState(strtoupper($state))) ?> Results"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                <a href="<?php facebookLink(buildTitle(getState(strtoupper($state)).' Member Results'), "It's time to hold Congress accountable! Find out how your congressmen scored on Heritage Action's Legislative Scorecard. See the details:".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send <?php echo ucfirst(getState(strtoupper($state))) ?> Results to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                <a href="<?php twitterLink("Help hold Congress accountable with @Heritage_Action's Legislative Scorecard: ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send <?php echo ucfirst(getState(strtoupper($state))) ?> Results to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
            </div>
            <div id="dataSortSwitch"><h4>SORTING OPTIONS</h4> <div id="sortSwitch"><img class="sortKnob" src="<?php echo  imagesPath() ?>/btn-sort_switch.png" width="17" height="17"></div></div>
        </div>
        <br class="clear">
        <div id="dataSortOptions">
            <div id="dataSortOptionsInner" class="rounded shadowInner">
                <div id="dropList" class="rounded shadowInner">
                    <select id="sortList" class="sessionList">
                        <option value="name" selected="selected">Name</option>
                        <option value="score">Score</option>
                        <option value="district">District</option>
                    </select>
                </div>
                <div class="sortOptionBox twoOptions">
                    <span><img src="<?php echo  imagesPath() ?>/sort-up.png" width="16" height="16" class="lblAsceDesc" value="ACSE">&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderAsceDesc'>
                        <option value='ASC' >Ascend</option>
                        <option value='DESC' selected="selected">Descend</option>
                    </select>
                    <span>&nbsp;&nbsp;&nbsp;<img src="<?php echo  imagesPath() ?>/sort-down.png" width="16" height="16" class="lblAsceDesc" value="DESC"></span>
                </div>
                <div class="sortOptionBox threeOptions">
                    <span class="lblHouseSenate" value="House">House&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderHouseSenate'>
                        <option value='House'>House</option>
                        <option value='All' selected="selected">All</option>
                        <option value='Senate'>Senate</option>
                    </select>
                    <span class="lblHouseSenate" value="Senate">&nbsp;&nbsp;&nbsp;Senate</span><br>
                    <span class="labelAll lblHouseSenate" value="All">All</span>
                </div>
                <div class="sortOptionBox threeOptions">
                    <span class="lblRepDem" value="Rep">Rep&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderRepDem'>
                        <option value='Rep'>Rep</option>
                        <option value='All' selected="selected">All</option>
                        <option value='Dem'>Dem</option>
                    </select>
                    <span class="lblRepDem" value="Dem">&nbsp;&nbsp;&nbsp;Dem</span><br>
                    <span class="labelAll lblRepDem" value="All">All</span>
                </div>
                <br class="clear">
            </div>
        </div> <!-- end Data sort options box -->
   </div> <!--end data nav bar box -->

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable overallTable">
     <thead> 
      <tr class="titleRow">
        <th >STATE</th>
        <th id="district" rel="num">DISTRICT</th>
        <th id="name" class="leftTitle">NAME</th>
        <th >PARTY</th>
        <th id="score" rel="num">SCORE</th>
      </tr> 
      </thead> 
      <?php foreach($stateResults as $s){ ?>
          <tr class="chamberTR partyTR <?php echo $s->chamber?>TR <?php echo $s->party?>TR" rel="<?php echo base_url(); echo index_page(); ?>members/member/<?php echo $s->congID; ?>">
            <td class="stateCell"><?php echo $s->state ?></td>
            <td class="districtCell"><?php echo $s->district ?></td>
            <td class="nameCell"><!-- <i class="sentIcon"></i> --> <?php echo $s->title ?>. <?php echo $s->fName ?> <?php echo $s->lName ?></td>
            <td class="partyCell"><?php echo $s->party ?></td>
            <td class="scoreCell blueCell"><?php echo $s->score ?><span class="thin">%</span></td>
          </tr>
      <?php } ?>
      
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br class="clear">
</div> <!-- End conBox -->   