<?php

    $num = 4 - count($members);
    for($i = 1; $i <= $num; $i++){
       ?>
       <div class="compareImgHolder topAdder left" data-chamber="house" style="height:41px; overflow:hidden">
            <img src="<?php echo  imagesPath() ?>/compare-plus.png" width="35" height="39" class="cover tooltip" title="Add Members of the House" /> 
            <img src="<?php echo  imagesPath() ?>/compare-plusH.jpg" width="35" height="39" class="topAdder" data-chamber="house"  >
        </div>
       <?php
    }
    foreach($members as $m){
        $this->crud->use_table('Members');
        $m = $this->crud->retrieve(array('congID' => $m, 'congressNum' => currentCongress()), 'row', 0, 0, array('id' => 'DESC'));
        ?>
            <div class="compareImgHolder topRemove left" style="height:41px; overflow:hidden" rel="<?php echo $m->congID; ?>" data-chamber="<?php echo $m->chamber; ?>">                
                <img src="<?php echo  imagesPath() ?>/compare-minus.png" width="35" height="39" class="cover tooltip" title="Remove <?php echo $m->title.'. '.$m->fName.' '.$m->lName; ?>" />        
                <img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $m->image_path ?>" width="35"  >
            </div>    
        <?php
    }

?>

<script type="text/javascript" charset="utf-8">
	$('.tooltip').tooltipster({
        theme: '.tooltipster-shadow',
        touchDevices: false
    });
</script>