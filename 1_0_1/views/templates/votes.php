<script type="text/javascript" charset="utf-8">
    $(function(){
        var val = '<?php echo $s ?>';
        
        if(val == 'All'){
            $('.chamberTR').removeClass('chamberHide');
            setChmaberBreadCrumbs('all');
            $("#sliderHouseSenate").val('All');
        }
        
        if(val == 'house'){
            $('.houseTR').removeClass('chamberHide');
            $('.senateTR').addClass('chamberHide');
            setChmaberBreadCrumbs('house');
            $("#sliderHouseSenate").val('House');
        }
        
        if(val == 'senate'){
            $('.senateTR').removeClass('chamberHide');
            $('.houseTR').addClass('chamberHide');
            setChmaberBreadCrumbs('senate');
            $("#sliderHouseSenate").val('Senate');
        }
        
        function setChmaberBreadCrumbs(chamber){
            var crumb = '<span class="divider"></span>';
            if(chamber == 'house'){
                crumb = crumb + "House";
                $('.breadchamber').html(crumb);
                $('.breadchamber').show();
            }else if(chamber == 'senate'){
                crumb = crumb + "Senate"
                $('.breadchamber').show();
                $('.breadchamber').html(crumb);
            }else{
                $('.breadchamber').hide();
            }
        }
    });
</script>

<a id="pageTop"></a>
<div id="conBox">
    <div id="infoBar">
        <div id="infoPath"><?php echo currentCongress() ?>th Congress<span class="divider"></span> Keyvotes<span class="breadchamber"></span></div>
        <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
         <?php $this->load->view('templates/compare_bar'); ?>
        <br class="clear">
    </div>
    
    <div id="dataNavBar">
       <h1>OVERALL RESULTS</h1>
       <div id="dataNav"><a href="#" class="active">VOTES</a><a href="<?php echo base_url(); echo index_page(); ?>members" >MEMBERS</a><a href="<?php echo base_url(); echo index_page(); ?>bills" class="last">CO-SPONSORSHIPS</a></div>
        <div id="dataNavRight">
            <div id="dataSocnet">
                <a href="<?php votesEmailLink() ?>" class="tooltip" title="Email This Page"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                <a href="<?php facebookLink(buildTitle('Overall Votes Results'), "How do your Members of Congress vote when it counts? See the critical votes in Congress on Heritage Action's Legislative Scorecard: ".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send This Page To Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                <a href="<?php twitterLink("Help hold Congress accountable with @Heritage_Action's Legislative Scorecard: ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send This Page To Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
            </div>
            <div id="dataSortSwitch"><h4>SORTING OPTIONS</h4> <div id="sortSwitch"><img class="sortKnob" src="<?php echo  imagesPath() ?>/btn-sort_switch.png" width="17" height="17"></div></div>
        </div>
        <br class="clear">
        <div id="dataSortOptions">
            <div id="dataSortOptionsInner" class="rounded shadowInner">
               <!--  <div id="dropList" class="rounded shadowInner">
                    <select id="sortList" class="sessionList">
                        <option value="name" selected="selected">Name</option>
                        <option value="score">Score</option>
                        <option value="district">District</option>
                        <option value="state">State</option>
                    </select>
                </div>
                <div class="sortOptionBox twoOptions">
                    <span><img src="<?php echo  imagesPath() ?>/sort-up.png" width="16" height="16" class="lblAsceDesc" value="ACSE">&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderAsceDesc'>
                        <option value='ASC' >Ascend</option>
                        <option value='DESC' selected="selected">Descend</option>
                    </select>
                    <span>&nbsp;&nbsp;&nbsp;<img src="<?php echo  imagesPath() ?>/sort-down.png" width="16" height="16" class="lblAsceDesc" value="DESC"></span>
                </div> -->
                <div class="sortOptionBox threeOptions">
                    <span class="lblHouseSenate" value="House">House&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderHouseSenate'>
                        <option value='House'>House</option>
                        <option value='All' selected="selected">All</option>
                        <option value='Senate'>Senate</option>
                    </select>
                    <span class="lblHouseSenate" value="Senate">&nbsp;&nbsp;&nbsp;Senate</span><br>
                    <span class="labelAll lblHouseSenate" value="All">All</span>
                </div>
               <!--  <div class="sortOptionBox threeOptions">
                    <span class="lblRepDem" value="Rep">Rep&nbsp;&nbsp;&nbsp;</span>
                    <select id='sliderRepDem'>
                        <option value='Rep'>Rep</option>
                        <option value='All' selected="selected">All</option>
                        <option value='Dem'>Dem</option>
                    </select>
                    <span class="lblRepDem" value="Dem">&nbsp;&nbsp;&nbsp;Dem</span><br>
                    <span class="labelAll lblRepDem" value="All">All</span>
                </div> -->
                <br class="clear">
            </div>
        </div> <!-- end Data sort options box -->
   </div> <!--end data nav bar box -->

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable repTable">
      <tr class="titleRow">
        <th >TITLE</th>
        <th class="leftTitle descripCell">DESCRIPTION</th>
        <th>HA POSITION</th>
      </tr>
      
      <? foreach($results as $vote){ ?>
          
              <tr rel="<?php echo base_url(); echo index_page(); ?>votes/vote/<?php echo $vote->roll_id?>" class="chamberTR <?php echo $vote->chamber ?>TR">
                <td class="titleCell"><?php echo $vote->question ?></td>
                <td class="descripCell"><?php echo trimDescription($vote->description) ?></td>
                <td class="haPosCell orangeCell"><?php if($vote->perferred_vote == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
              </tr>
          
      <?php } ?>
      
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br class="clear">
</div> <!-- End conBox -->   