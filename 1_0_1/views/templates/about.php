<script type="text/javascript" src="<?php echo jsPath(); ?>/raphael.2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/justgage.1.0.1.min.js"></script>
<!--Only On home page -->
<script type="text/javascript">
    $(document).ready(function()
{
    
    var g1 = new JustGage({
      id: "g1", 
      value: <?php echo $hrAv ?>, 
      min: 0,
      max: 100,
      title: "HOUSE REPUBLICANS",
      gaugeColor: "#e4e2e2",
      titleFontColor : "#f8f8f8",
      levelColors: [
      "#173454",
      "#173454",
      "#173454"
    ]          
   });
    
   var g2 = new JustGage({
      id: "g2", 
      value: <?php echo $hdAv ?>, 
      min: 0,
      max: 100,
      title: "HOUSE DEMOCRATS",
      titleFontColor : "#f8f8f8",
      gaugeColor: "#e4e2e2",
      levelColors: [
      "#173454",
      "#173454",
      "#173454"
    ] 
    });
    
    var g3 = new JustGage({
      id: "g3", 
      value: <?php echo $srAv ?>, 
      min: 0,
      max: 100,
      title: "SENATE REPUBLICANS",
      titleFontColor : "#f8f8f8",
      gaugeColor: "#e4e2e2",
      levelColors: [
      "#173454",
      "#173454",
      "#173454"
    ] 
    });
    
     var g4 = new JustGage({
      id: "g4", 
      value: <?php echo $sdAv ?>, 
      min: 0,
      max: 100,
      title: "SENATE DEMOCRATS",
      gaugeColor: "#e4e2e2",
      titleFontColor : "#f8f8f8",
      levelColors: [
      "#173454",
      "#173454",
      "#173454"
    ] 
    });
  
   
});
</script>
<!--Only On home page --> 

<div id="conHome">
            <div id="mapBox">
                <div class="oneColInner">
                    <h1>About Score Card</h1>
                    <?php echo stripcslashes($page->content) ?>
                </div>
            </div>
            
            <div id="topPerfBox">
                <div class="topPerfBtn left topPerfHouseBtn active">
                    <div class="topPerfBtnInner"><em>Top House</em> <h4>PERFORMERS</h4></div>
                </div>
                <div class="topPerfBtn right topPerfSenateBtn">
                    <div class="topPerfBtnInner"><em>Top Senate</em> <h4>PERFORMERS</h4></div>
                </div>
                
                <br class="clear">
                
                <div id="topPerfBoxInner">
                    <p>Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></p>
                    <div id="topPerfHouse" class="topPerfResults">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="topPerfTable resultsTable">
                          <?php foreach($topHouse as $h){ ?>
                              <tr rel="<?php echo base_url(); echo index_page(); ?>members/member/<?php echo $h->congID; ?>">
                                <td class="picCell"><div class="imgholder"><img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $h->image_path ?>"  ></div></td>
                                <td class="nameCell">Rep. <?php echo $h->fName ?> <?php echo $h->lName ?></td>
                                <td class="stateCell"><?php echo $h->state ?></td>
                                <td class="scoreCell blueCell"><?php echo $h->score ?>%</td>
                              </tr>
                          <?php } ?>
                        </table>
                    </div>
                    <div id="topPerfSenate" class="topPerfResults">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="topPerfTable resultsTable">
                           <?php foreach($topSenate as $h){ ?>
                              <tr rel="<?php echo base_url(); echo index_page(); ?>members/member/<?php echo $h->congID; ?>">
                                <td class="picCell"><div class="imgholder"><img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $h->image_path ?>" ></div></td>
                                <td class="nameCell">Rep. <?php echo $h->fName ?> <?php echo $h->lName ?></td>
                                <td class="stateCell"><?php echo $h->state ?></td>
                                <td class="scoreCell blueCell"><?php echo $h->score ?>%</td>
                              </tr>
                          <?php } ?>
                        </table>
                    </div>
                </div>
            </div> <!-- End top performers box -->
        
        </div><!-- End conHome -->
        <br class="clear">
        <div id="gaugeBox" class="respOff" data-set="homeGauges">
            <div class="appendContent">
                <div class="gaugeOuter">
                    <div class="gaugeInner gradLightGrey shadow">
                        <div id="g1" class="gauge"></div>
                        <h2>AVERAGE HOUSE REPUBLICANS</h2>
                    </div>
                </div>
                <div class="gaugeOuter">
                    <div class="gaugeInner gradLightGrey shadow">
                        <div id="g2" class="gauge"></div>
                        <h2>AVERAGE HOUSE DEMOCRATS</h2>
                    </div>
                </div>
                <div class="gaugeOuter">
                    <div class="gaugeInner gradLightGrey shadow">
                        <div id="g3" class="gauge"></div>
                        <h2>AVERAGE SENATE REPUBLICANS</h2>
                    </div>
                </div>
                <div class="gaugeOuter">
                    <div class="gaugeInner gradLightGrey shadow">
                        <div id="g4" class="gauge"></div>
                        <h2>AVERAGE SENATE DEMOCRATS</h2>
                    </div>
                </div>
                <br class="clear">
            </div>
        </div>
        


