<a id="pageTop"></a>
<div id="conBox">
    <div id="infoBar">
        <div id="infoPath"><?php echo currentCongress() ?>th Congress  <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>members?c=senate">Senate</a> <span class="divider"></span> <a href="<?php echo base_url().index_page() ?>votes"> Keyvotes </a> <span class="divider"></span> Compare</div>
        <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
        <?php $this->load->view('templates/compare_bar'); ?>
        <br class="clear">
    </div>
    
    <div id="dataNavBar">
        <h1>COMPARE</h1>
        <div id="dataNav"><a href="<?php echo base_url().index_page(); ?>compare/compareHouseVotes<?php echo $uri ?>" class="">HOUSE</a><a href="#" class="active">SENATE</a><a href="#" class="active">VOTES</a><a href="<?php echo base_url().index_page(); ?>compare/compareSenateBills<?php echo $uri ?>" class="last">CO-SPONSORSHIPS</a></div>
        <div id="dataNavRight">
            <div id="dataSocnet">
                <a href="<?php emailLink() ?>" class="tooltip" title="Email This Page"><img src="<?php echo  imagesPath() ?>/socnet-email.png" width="14" height="13"></a>
                <a href="<?php facebookLink(buildTitle(''), "Look at the differences in these congressional voting records on the @Heritage_Action scorecard".getUrl() , getUrl()) ?>" target="_blank" class="tooltip" title="Send This Page to Facebook"><img src="<?php echo  imagesPath() ?>/socnet-fb.png" width="12" height="13"></a>
                <a href="<?php twitterLink("Look at the differences in these congressional voting records on the @Heritage_Action scorecard ".goo_gl_short_url(getUrl())) ?>" target="_blank" class="tooltip" title="Send This Page to Twitter"><img src="<?php echo  imagesPath() ?>/socnet-twt.png" width="12" height="13"></a>
            </div>
        </div>
        <br class="clear">
   </div> <!--end data nav bar box -->
   <?php
        $memcount = count($senatememImgs);
        $count = 4 - $memcount; 
   ?>
   
   <div class="compareTitleRow">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable compareTable">
           <tr class="titleRow ">
            <th class="leftTitle logoCell titleCell">TITLE</th>
            <th class="haPosCell leftTitle ">HA POSITION</th>
            <th class="spacerCell"></th>
            <?php foreach($senatememNames as $name){ ?>
                 <th class="compareCell"><?php echo $name ?></th>
            <?php } ?>
            <?php 
                if($count > 0){
                    for($i = 1; $i <= $count; $i++){ ?>
                        <th class="compareCell">Add Member</th>              
                    <?php }
                }
            ?>
          </tr>
       </table>
   </div>
   
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="resultsTable compareTable">
      <tr>
        <th class="leftTitle logoCell titleCell"><!-- <img src="<?php echo  imagesPath() ?>/logonew.png" width="96" height="48"> --></th>
        <th class="haPosCell leftTitle "></th>
        <th class="spacerCell"></th>
        <?php foreach($senatememImgs as $img){ ?>
                    <th class="compareCell titleImg"> <div class="compareImg"><img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $img; ?>"  height="125"></div></th>
        <?php } ?>
        <?php 
            if($count > 0){
                for($i = 1; $i <= $count; $i++){ ?>
                    <th class="compareCell titleImg addToCompare" data-chamber="senate"><div class="compareImg"><img src="<?php echo  imagesPath() ?>/pic-compare.png" width="115" height="125"></div></th>              
                <?php }
            }
        ?>
      </tr>
      <tr class="titleRow">
        <th class="leftTitle logoCell titleCell">TITLE</th>
        <th class="haPosCell leftTitle ">HA POSITION</th>
        <th class="spacerCell"></th>
        <?php foreach($senatememNames as $name){ ?>
             <th class="compareCell"><?php echo $name ?></th>
        <?php } ?>
        <?php 
            if($count > 0){
                for($i = 1; $i <= $count; $i++){ ?>
                    <th class="compareCell">Add Member</th>              
                <?php }
            }
        ?>
      </tr>
      <tr>
        <td class="titleCell">&nbsp;</td>
        <td class="haPosCell ">&nbsp;</td>
        <td class="spacerCell">&nbsp;</td>
        <?php foreach($senatememScores as $score){ ?>
            <td class="compareCell "><?php echo $score ?>%</td>    
        <?php } ?> 
        <?php 
            if($count > 0){
                for($i = 1; $i <= $count; $i++){ ?>
                    <td class="compareCell ">--</td>              
                <?php }
            }
        ?>   
      </tr>
      <?php $i = 0; foreach($senatevotes as $vote){ ?>
          <tr rel="<?php echo base_url(); echo index_page(); ?>votes/vote/<?php echo $vote['roll_id']?>">
            <td class="titleCell"><?php echo $vote['title']; ?></td>
            <td class="haPosCell orangeCell"><?php if($vote['position'] == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
            <td class="spacerCell">&nbsp;</td>
            <?php foreach($vote['votes'] as $vv) { ?>
                <td class="compareCell <?php if($vv == "Not Voting" || $vv == "Present"){ echo 'greyCell'; }elseif($vv == $vote['position']) { echo 'orangeCell';}else{ echo 'ltBlueCell'; } ?>"><?php if($vv == "Not Voting" || $vv == "Present"){?> <span class="noVote"></span> <?php }elseif($vv == 'Yes'){?> <span class="yesPos"></span> <?php }else{?><span class="noPos"></span><?php } ?></td>
            <?php } ?>
            <!-- <td class="compareCell blueCell"><span class="noPos"></span></td> -->
            <?php 
            if($count > 0){
                for($i = 1; $i <= $count; $i++){ ?>
                    <td class="compareCell ">&nbsp;</td>              
                <?php }
            }
            ?>
          </tr>
     <?php $i++; } ?>
    </table>
    <a href="#pageTop" class="topAnchor">^ Back to top</a>
    <br class="clear">
</div> <!-- End conBox -->   
