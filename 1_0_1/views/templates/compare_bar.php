<div id="infoCompare">
    <div class="left houseCompare">
        <div class="houseBlock">
            <?php 
                if(!get_data('comparehouse' )){$this->session->set_userdata('comparehouse' , array());}
                $members = get_data('comparehouse' );
                $num = 4 - count($members);
                for($i = 1; $i <= $num; $i++){ ?> 
                    <div class="compareImgHolder left topAdder" data-chamber="house" style="height:41px; overflow:hidden">
                        <img src="<?php echo  imagesPath() ?>/compare-plus.png" width="35" height="39" class="cover tooltip" title="Add Members of the House" />
                        <img src="<?php echo  imagesPath() ?>/compare-plusH.jpg" width="35" height="39" class="topAdder "   >
                    </div>
                    
                <?php }
                foreach($members as $m){
                    $this->crud->use_table('Members');
                    $m = $this->crud->retrieve(array('congID' => $m, 'congressNum' => currentCongress()), 'row', 0, 0, array('id' => 'DESC'));
                    ?>
                        <div class="compareImgHolder topRemove left" style="height:41px; overflow:hidden" rel="<?php echo $m->congID; ?>" data-chamber="<?php echo $m->chamber; ?>">                
                            <img src="<?php echo  imagesPath() ?>/compare-minus.png" width="35" height="39" class="cover tooltip" title="Remove <?php echo $m->title.'. '.$m->fName.' '.$m->lName; ?>" />        
                            <img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $m->image_path ?>" width="35"  >
                        </div>    
                    <?php
                }
                
            ?>
        </div>
        <div class="houseLoading loading">
            <img src="<?php echo  imagesPath() ?>/loading.gif" style="border:0" >
        </div>
    </div>
    
    <a href="<?php echo  base_url(); echo index_page() ?>/compare"><div class="compareBtn gradLightGrey shadow rounded">COMPARE</div></a>

    <div class="left senateCompare">
        <div class="senateBlock">
            <?php
                if(!get_data('comparesenate' )){$this->session->set_userdata('comparesenate' , array());}
                $members = get_data('comparesenate' );
                foreach($members as $m){
                    $this->crud->use_table('Members');
                    $m = $this->crud->retrieve(array('congID' => $m, 'congressNum' => currentCongress()), 'row', 0, 0, array('id' => 'DESC'));
                    ?>
                        <div class="compareImgHolder topRemove left" style="height:41px; overflow:hidden" rel="<?php echo $m->congID; ?>" data-chamber="<?php echo $m->chamber; ?>">                
                            <img src="<?php echo  imagesPath() ?>/compare-minus.png" width="35" height="39" class="cover tooltip" title="Remove <?php echo $m->title.'. '.$m->fName.' '.$m->lName; ?>" />        
                            <img src="<?php echo  base_url() ?>admin/memImgs/<?php echo $m->image_path ?>" width="35"   />
                        </div>    
                    <?php
                }
                $num = 4 - count($members);
                for($i = 1; $i <= $num; $i++){
                   ?> 
                   <div class="compareImgHolder  topAdder left" data-chamber="senate" style="height:41px; overflow:hidden">
                        <img src="<?php echo  imagesPath() ?>/compare-plus.png" width="35" height="39" class="cover tooltip" title="Add Members of the Senate" />
                        <img src="<?php echo  imagesPath() ?>/compare-plusS.jpg" width="35" height="39" class="topAdder" data-chamber="senate"  >
                   </div> 
                   
                   <?php
                }
            ?>
        </div>
        
        <div class="senateLoading loading">
            <img src="<?php echo  imagesPath() ?>/loading.gif" style="border:0" >
        </div>
        
    </div>
    <br class="clear" />
</div>