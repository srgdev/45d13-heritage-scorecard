<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<div class="closeDrop"><img src="<?php echo  imagesPath() ?>/close.png" /></div>
<h1 id="">Add to Compare</h1>

<div class="scrollPrompt">Scroll to see full list</div>
<form action="<?php echo base_url().index_page(); ?>compare/addMass" method="post" accept-charset="utf-8">
    <input type="hidden" name="chamber" value="<?php echo $chamber; ?>" id="chmaber"/>
  <!-- <div id="dropList" class="rounded shadowInner">
      <select id="sortList" class="sessionList">
          <option value="">Select Member</option>
          <?php foreach($members as $member) { ?>
              <option value="<?php echo $member->congID ?>"><?php echo $member->title.'. '.$member->fName.' '.$member->lName ?></option>
          <?php } ?>
      </select>
  </div> -->
  <div class="compareList">
      <?php foreach($members as $member) { ?>
        <div class="addCheckbox left"><input type="checkbox" name="add[]" class="addBox" value="<?php echo $member->congID ?>" id="some_name" <?php if(in_array($member->congID, $inCompare)) { echo 'checked="checked"'; }?> /> <?php echo $member->title.'. '.$member->fName.' '.$member->lName ?></div>
      <?php } ?>
  </div>
  <br class="clear" />
  <br />
  <input type="submit" value="Add to Compare" class="gradLightGrey shadow rounded btns"/>
  
</form>

<script type="text/javascript" charset="utf-8">
	$(function(){
	    $('.addBox').click(function(){
	        var $b = $('.compareList input[type=checkbox]');
            var count  = $b.filter(':checked').length;
            if(count > 4) {
                alert('Only 4 Members can be compared at a time')
                return false;
            }
	    })
	})
</script>