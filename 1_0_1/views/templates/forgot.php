<a id="pageTop"></a>        
        <div id="conBox">
            <div id="infoBar">
                <div id="infoPath"><?php echo currentCongress() ?>th Congress <span class="divider"></span> Wathclist <span class="divider"></span> Recover Password</div>
                <div id="infoSync">Last Updated on: <?php echo lastUpdated() ?> &nbsp; <img src="<?php echo  imagesPath() ?>/btn-sync.png" width="11" height="11"></div>
                <?php $this->load->view('templates/compare_bar'); ?>
                <br class="clear">
            </div>
            
            <div id="dataNavBar">
                <h1>WATCHLIST</h1>
            </div> <!--end data nav bar box -->
           <br><br>
           <div class="signinform">
               <div class="left login">
                   <?php $attributes = array('id' => 'signin', 'data-validate' => "parsley"); 
                         echo form_open('watchlist/sendPass', $attributes);  ?>
                       <h2>Your Email Address</h2>
                       <div class="watchalert">
                       <?php echo getMessage(); ?>
                       </div> 
                       <input type="text" name="email"  id="email" placeholder="Email Address" data-required="true" data-type="email" data-trigger="change" value="<?php if(isset($_COOKIE["email"])) {echo $_COOKIE["email"]; } ?>" />
                       <input type="submit" value="Send  Password" class="gradLightGrey shadow rounded btns" />
                       <br /><br />
                       <a href="<?php echo base_url().index_page() ?>/watchlist/signup">Don't have a Watchlist? Create one here.</a>
                   </form>
               </div>
               
               
               
               
               
               <br class="clear" />
           </div>
           <a href="#pageTop" class="topAnchor">^ Back to top</a>
           <br class="clear">
        </div> <!-- End conBox -->   