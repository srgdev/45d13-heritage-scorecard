(function($){
  $(document).ready(function(){     
    $.ajax({
      url: "http://heritageaction.com/latest-post-info/", 
      format: 'JSON',
      success: function(data){
        $("#blog-latest-info").html(data);
      }, 
      cache: false
    });
    
    $("#search-trigger").click(function(){
      if($("#search").hasClass('hover')){
        $("#search").removeClass('hover');
      }
      else if(!$("#search").hasClass('hover')){
        $("#search").addClass('hover');
      }
    });
    
    $('.search-input').data('holder',$('.search-input').attr('placeholder'));

    $('.search-input').focusin(function(){
        $(this).attr('placeholder','');
    });
    $('.search-input').focusout(function(){
        $(this).attr('placeholder',$(this).data('holder'));
    });
    
    // allow clicking on nav dropdown to act as link click
    $("#menu-main-nav > li.donate").click(function(){
        window.location.href = $('a',$(this)).attr('href');
    });
    
    $("#search-btn").click(function() {
      $("#searchform").submit();
      return false;
    });
    
    $(".dashboardZipGo").click(function(){
       if(jQuery(".dashboardZipSearch").val() !== '' && jQuery(".dashboardZipSearch").val() !== "Enter your zip"){
          window.location = 'http://heritageaction.com/congress/' + jQuery(".dashboardZipSearch").val();
       }
       else{
          jQuery(".dashboardZipSearch").addClass('error');
       }        
    });
    
    $(document).keypress(function(e){
      if($(".dashboardZipSearch").is(":focus")){
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13){
          if(jQuery(".dashboardZipSearch").val() !== '' && jQuery(".dashboardZipSearch").val() !== "Enter your zip"){
              window.location = 'http://heritageaction.com/congress/' + jQuery(".dashboardZipSearch").val();
           }
           else{
              jQuery(".dashboardZipSearch").addClass('error');
           }
        }
      }
    })
    
  })
})(jQuery);