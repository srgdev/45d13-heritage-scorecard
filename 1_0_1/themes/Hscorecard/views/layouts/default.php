<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en-US" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en-US" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en-US" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US" class="no-js"> <!--<![endif]-->
<head>
<meta charset="" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Heritage Action Scorecard</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<link href='http://fonts.googleapis.com/css?family=IM+Fell+Great+Primer+SC|Open+Sans+Condensed:300,700|Bree+Serif' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo cssPath(); ?>/layout.css">
<link rel="stylesheet" href="<?php echo cssPath(); ?>/effects.css">
<link rel="stylesheet" href="<?php echo templatePath(); ?>/heritageHeader/nav.css"  type="text/css">
<link href="<?php echo cssPath(); ?>/selectyze.jquery.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo cssPath(); ?>/jquery.tooltip.css" />
<link rel="stylesheet" href="<?php echo cssPath(); ?>/temp.css">
<link rel="stylesheet" href="<?php echo cssPath(); ?>/responsive.css">

<script type="text/javascript" charset="utf-8">
	var baseUrl = '<?php echo base_url().index_page() ?>';
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo jsPath(); ?>/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<!-- <script src="<?php echo jsPath(); ?>/vendor/jquery-1.8.3.min.js"></script> -->
<script type="text/javascript" src="<?php echo templatePath(); ?>/heritageHeader/header.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/plugins.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/tablesorter_savesort.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/main.js"></script> 


</head>
<?php if(!isset($body_class)) { $body_class = 'search';} ?>    
<body class="<?php echo $body_class; ?>" rel="<?php echo currentCongress() ?>">
<?php if(ENVIRONMENT == 'development'){ ?>
<div class="devAlert">
    Development Version <?php echo VERSION ?>  <a href="#" class="openLog">Change Log</a>
    
    <div class="changelog">
        <?php $this->load->view('templates/changelog') ?>
    </div>
</div>
<?php } ?>
<div id="stickyOuter">
<div id="stickyInner">

<div id="heritageHeader">
    <nav role="navigation" class="site-navigation main-navigation">
        <div class="menu-main-nav-container">
            <ul id="menu-main-nav" class="menu">
                <li id="home-link">
                    <h1 class="site-title"><a href="http://heritageaction.com">Heritage Action Scorecard </a></h1>
                </li>
                <li id="score-card" class="gradient light-blue-gradient score-card">
                    <a href="/" rel="home"><span class="nav-title">Score Card </span></a>
                    <span class="nav-desc">“Heritage Action is the scorecard for conservatives”<br><span class="author">– Washington Examiner</span></span>
                </li>
                <li class="gradient red-gradient dashboard">
                    <a href="http://heritageaction.com/congress/"><span class="nav-title">Dashboard</span></a>
                    <span class="nav-desc nav-search">
                        <input id="headerNavDashboardSearch" type="search" name="search zip" value="" placeholder="Enter your zip" class="dashboardZipSearch">
                        <div class="go gradient red-gradient dashboardZipGo"><div class="arrow-right"></div></div>
                    </span>
                </li>
                <li class="gradient orange-gradient the-forge-blog">
                    <a href="http://heritageaction.com/blog/"><span class="nav-title">The Forge Blog</span></a>
                    <span id="blog-latest-info" class="nav-desc"></span>
                </li>
                <li id="donate" class="gradient light-red-gradient donate">
                    <a href="http://heritageaction.com/donate/"><span class="nav-title">Donate</span></a>
                    <span class="nav-desc">&ldquo;I give money to Heritage Action, you should too.&rdquo;<br>
                    <span class="author">&ndash; Erick Erickson</span></span>
                </li>
                <li id="search" class="gradient blue-gradient search">
                    <a href="#" id="search-trigger"></a>
                    <span id="search-form" class="nav-desc">
                        <form method="get" id="searchform" action="http://heritageaction.com/" role="search">
                            <label for="s" class="assistive-text">Search</label>
                            <input type="text" class="field search-input" name="s" value="" id="s" placeholder="Search &hellip;">
                            <div id="search-btn" class="go gradient blue-gradient"><div class="arrow-right"></div></div>
                        </form>
                    </span>
                </li>
            </ul>
        </div>
    </nav>
<div id="output"></div>
</div> <!-- End Heritage Header -->


<div id="respHeritageHeader">
<div id="logo"><img src="<?php echo  imagesPath() ?>/logo.png" width="320" height="162"></div>
<ul id="headerNav">
    <li><a href="#">HERITAGE ACTION</a></li>
    <li><a href="#">DASHBOARD</a></li>
    <li><a href="#">THE FORGE BLOG</a></li>
    <li class="last"><a href="#">DONATE</a></li>
</ul>
</div>

<div id="topOuter" class="gradLightBlue"> <a href="<?php echo base_url(); ?>"><h1>SCORE CARD</h1></a></div>

<div id="navOuter" class="gradMedGrey">
    <div id="navInner">
        <div id="navRight">
            <div id="sessionBox" class="rounded shadowInner">
                <select name="session" id="session" class="sessionList changeSession">
                    <option value="NONE" selected="selected">Select Session</option>
                    <option value="113" <?php if(currentCongress() === '113') {?> selected="selected" <?php } ?>>113th ('13 - '14)</option>
                    <option value="112" <?php if(currentCongress() == '112') {?> selected="selected" <?php } ?>>112th ('11 - '12)</option>
                </select>
            </div>
            <?php if($this->siteauth->checkAuth()) { ?>
                <div id="userBox"><a href="<?php echo base_url(); echo index_page(); ?>watchlist/signout">Sign Out</a></div>
            <?php }else{ ?>
                <div id="userBox"><a href="<?php echo base_url(); echo index_page(); ?>watchlist/signin">Sign In</a></div>
            <?php } ?>    
        </div><!-- End navRight -->
        
        <ul id="navLeft">
            <li><div class="navPointer aboutPointer"></div><a href="<?php echo base_url(); echo index_page(); ?>about">ABOUT</a></li>
            <li><div class="navPointer searchPointer"></div><a href="<?php echo base_url(); echo index_page(); ?>">SEARCH</a></li>
            <br class="navBreak">
            <li><div class="navPointer resultsPointer"></div><a href="<?php echo base_url(); echo index_page(); ?>members">OVERALL RESULTS</a></li>
            <li class="last"><div class="navPointer watchPointer"></div><a href="<?php echo base_url(); echo index_page(); ?>watchlist">WATCHLIST</a></li>
            <br class="clear">
        </ul> <!-- End navLeft -->
    </div>
</div>

<div id="mainOuter">
    <div id="mainInner">
        <div class="dropper">
            <div class="dropInner">
                
            </div>
        </div>
        <?php echo $template['partials']['body']; ?>
        
        
    </div><!-- End mainInner -->
</div><!-- End mainOuter -->

</div> <!-- stickyInner -->
</div><!-- End stickyOuter -->

<div id="stickyFooter">
    <div id="footerInner">
        <div id="footerLeft">
            <p><a class="first" href="<?php echo base_url(); echo index_page(); ?>about">About</a><a href="<?php echo base_url(); echo index_page(); ?>">Search</a><a href="<?php echo base_url(); echo index_page(); ?>members">Overall Results</a><a class="last" href="<?php echo base_url(); echo index_page(); ?>watchlist">Watchlist</a></p>
            <p><em>214 Massachusetts Avenue NE, Suite 400, Washington, DC 20002 <br>&copy; 2013 Heritage Action for America. All Rights Reserved.</em></p>
        </div>
        <div id="footerRight">
            Visit our sister site:<br>
            <a href="http://heritageaction.com/" target="_blank"><img src="<?php echo  imagesPath() ?>/footer-logo.png" width="140" height="55"></a> 
        </div>
        <br class="clear">
    </div>
</div><!-- End stickyFooter -->




<?php if($this->configs->get('googleAnalytics') == 'true'){
    ?>
        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', '<?php echo $this->configs->get('analyticsID') ?>']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script> 
    <?php
} ?>

</body>
</html>