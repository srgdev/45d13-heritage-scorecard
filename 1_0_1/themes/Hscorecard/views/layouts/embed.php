<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo cssPath(); ?>/embedstyle.css">
    </head>
    <body>
       <a href="<?php echo base_url().index_page() ?>/members/member/<?php echo $member->congID ?>">
           <div class="wrapper">
               <div class="name">
                   <?php echo $member->title.'. '.$member->fName.' '.$member->lName; ?>
               </div>
               <div class="score">
                   <?php echo $member->score ?>%
               </div>
           </div>
       </a>
        
    </body>
</html>