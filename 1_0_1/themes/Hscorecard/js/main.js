$(document).ready(function()
{
	
	/*append arrounds*/
	$('.appendContent').appendAround();
	
	$('.tooltip').tooltipster({
		theme: '.tooltipster-shadow',
		touchDevices: false
	});
	
	/* watch list dropdown*/
	$('.watchAct').click(function(){
		var activeItem = $(this).parents('.watchItem');
		var closedHeight  = $('.watchWrapper', activeItem).height();
		var dropHeight = $('.dropdown', activeItem).height() + $('.watchPointer', activeItem).height();
		var openHeight  = closedHeight + dropHeight;
		var dropSpeed = 200;
		var fadeSpeed = 150;
		
		if(!$(activeItem).hasClass('open')){
			/*reset all dropdowns*/
			$('.watchItem').removeClass('open');
			$('.watchItem').animate({height:closedHeight}, dropSpeed)
			$('.dropdown').fadeOut();
			$('.watchItem .watchPointer').fadeOut();
			/*drop down*/
			$(activeItem).addClass('open');
			$(activeItem).animate({height:openHeight}, dropSpeed, function(){
				$('.dropdown', activeItem).fadeIn(fadeSpeed); 
				$('.watchPointer', activeItem).fadeIn(fadeSpeed);
			});
		}else{
			/*reset all dropdowns*/
			$('.watchItem').removeClass('open');
			$(activeItem).animate({height:closedHeight}, dropSpeed)
			$('.dropdown', activeItem).fadeOut();
			$('.watchItem .watchPointer').fadeOut();
		}
	});
	
	$('.watchClose').click(function(){
		var activeItem = $(this).parents('.watchItem');
		var closedHeight  = $('.watchWrapper', activeItem).height();
		var dropSpeed = 200;
		
		$('.watchItem').removeClass('open');
		$('.watchItem').animate({height:closedHeight}, dropSpeed)
		$('.dropdown').fadeOut();
		$('.watchItem .watchPointer').fadeOut();
	});
	
	
	/*activate labels on sort option sliders*/
	$('.lblAsceDesc').on('click', function(){
    	$('#sliderAsceDesc').val($(this).attr('value')).change();
 	});
	$('.lblHouseSenate').on('click', function(){
    	$('#sliderHouseSenate').val($(this).attr('value')).change();
 	});
	$('.lblRepDem').on('click', function(){
    	$('#sliderRepDem').val($(this).attr('value')).change();
 	});
	
	 $('#sortList').val('name')
	//Custom SelectBox
	$('.sessionList').Selectyze({
		theme : 'heritage'
	});	
	
	// toggle top Performers //
	$('.topPerfBtn').click(function() {
    	if ($(this).hasClass("active")) {
			//empty
		} else {
			$('.topPerfBtn').removeClass('active');
			$('.topPerfResults').slideUp()
			if ($(this).hasClass("topPerfHouseBtn")) {
				$('#topPerfHouse').slideDown();
			} else {
				$('#topPerfSenate').slideDown();
			}
			$(this).addClass('active');
		}
	});
	
	// toggle sort Options //
	$('#sortSwitch').click(function() {
    	if ($(this).hasClass("active")) {    		
			$('#dataSortOptions').slideUp();
			$(this).removeClass('active');
			$('.sortKnob').animate({margin: '0 0 0 0'}, 200);
			$(this).parent('#dataSortSwitch').parent('#dataNavRight').parent('#dataNavBar').parent('.sticky-wrapper').animate({height:'50px'}, 400)
		} else {
			$(this).parent('#dataSortSwitch').parent('#dataNavRight').parent('#dataNavBar').parent('.sticky-wrapper').animate({height:'140px'})
			$('#dataSortOptions').slideDown();
			/*Sliders (don't load until conatainer is shown or slider brakes*/	
				if ($('.switchy-container').length) {
					//don't load again
				} else {
					$('#sliderAsceDesc').switchy();
					$('#sliderHouseSenate').switchy();
					$('#sliderRepDem').switchy();
				}
			/*Sliders (don't load until conatainer is shown or slider brakes*/
			$(this).addClass('active');
			$('.sortKnob').animate({margin: '0 0 0 25px'}, 200);
		}
	});
	
	 $.fn.maphilight.defaults = {
			fill: true,
			fillColor: 'ffffff',
			fillOpacity: 0.4,
			stroke: false,
			strokeColor: 'ff0000',
			strokeOpacity: .1,
			strokeWidth: 1,
			fade: true,
			alwaysOn: false,
			neverOn: false,
			groupBy: 'href'
		}

		$('.map').maphilight();
		$("map > area").tooltip({ 
			track: true, 
			delay: 0, 
			showURL: false, 
			showBody: " - ", 
			fade: 250 
		});
		
		
	
	$(".resultsTable").tablesorter({ 
	  widgets: ["saveSort"] 
	}); 
	
	 var table = $('.resultsTable');
     var inverse = false;
	
	$('#sortList').bind("change", function(){
		attr = $(this).val();
		var row = translateRow(attr);
		if(inverse){direction = 1}else{direction = 0}
		var sorting = [[row,direction]]; 
        // sort on the first column 
        $(".resultsTable").trigger("sorton",[sorting]);
	});
	 
	 
	function translateRow(atr){
		if(atr == 'name') {return 2;}
		if(atr == 'score') {return 4;}
		if(atr == 'district') {return 1;}
		if(atr == 'state') {return 0;}
	} 
     
     // $('#sortList').change(function(){
     	// attr = $(this).val();
     	// sorter(attr);      
     // })   
     
     
    $('#sliderAsceDesc').val('DESC');
     $('#sliderAsceDesc').change(function(){
     	inverse = !inverse;
     	var attr = $('#sortList').val();
     	var row = translateRow(attr);
		if(inverse){direction = 1}else{direction = 0}
		var sorting = [[row,direction]]; 
        // sort on the first column 
        $(".resultsTable").trigger("sorton",[sorting]);
     })
     
     
	
	$('#sliderHouseSenate').val('All')
	$('#sliderHouseSenate').change(function(){
		var val = $(this).val();
		
		if(val == 'All'){
			$('.chamberTR').removeClass('chamberHide');
		}
		
		if(val == 'House'){
			$('.houseTR').removeClass('chamberHide');
			$('.senateTR').addClass('chamberHide');
		}
		
		if(val == 'Senate'){
			$('.senateTR').removeClass('chamberHide');
			$('.houseTR').addClass('chamberHide');
		}
		setChmaberBreadCrumbs(val);
	})
	
	
	function setChmaberBreadCrumbs(chamber){
		var crumb = '<span class="divider"></span>';
		if(chamber == 'House'){
			crumb = crumb + "House";
			$('.breadchamber').html(crumb);
			$('.breadchamber').show();
		}else if(chamber == 'Senate'){
			crumb = crumb + "Senate"
			$('.breadchamber').show();
			$('.breadchamber').html(crumb);
		}else{
			$('.breadchamber').hide();
		}
	}
	
	$('#sliderRepDem').val('All')
	$('#sliderRepDem').change(function(){
		var val = $(this).val();
		
		if(val == 'All'){
			$('.partyTR').removeClass('partyHide');
		}
		
		if(val == 'Rep'){
			$('.RTR').removeClass('partyHide');
			$('.DTR').addClass('partyHide');
		}
		
		if(val == 'Dem'){
			$('.DTR').removeClass('partyHide');
			$('.RTR').addClass('partyHide');
		}
		setPartyBreadCrumbs(val);
	})
	
	function setPartyBreadCrumbs(party){
		var crumb = '<span class="divider"></span>';
		if(party == 'Rep'){
			crumb = crumb + "Republican";
			$('.breadparty').html(crumb);
			$('.breadparty').show();
		}else if(party == 'Dem'){
			crumb = crumb + "Democrat"
			$('.breadparty').show();
			$('.breadparty').html(crumb);
		}else{
			$('.breadparty').hide();
		}
	}
	
	$('.tip').click(function(){
		var rel = $(this).attr('rel');
		window.location = baseUrl+"/state/state/"+rel
	})
	
	$('.resultsTable tr').click(function(){
		if($(this).attr('rel')){
			window.location = $(this).attr('rel');
		}
		
	})
	
	
	// VOTE TABLE
	
	$('.voteNav a').click(function(){
		$('#dataNav a').removeClass('active');
		$(this).addClass('active');
		
		return false;
	})
	
	$('.novote').click(function(){
		$('.NoTR').removeClass('voteHide');
		$('.YesTR').addClass('voteHide');
		$('.notTR').addClass('voteHide');
	})
	
	$('.yesvote').click(function(){
		$('.NoTR').addClass('voteHide');
		$('.YesTR').removeClass('voteHide');
		$('.notTR').addClass('voteHide');
	})
	
	$('.notvote').click(function(){
		$('.NoTR').addClass('voteHide');
		$('.YesTR').addClass('voteHide');
		$('.notTR').removeClass('voteHide');
	})
	
	$('.allvote').click(function(){
		$('.NoTR').removeClass('voteHide');
		$('.YesTR').removeClass('voteHide');
		$('.notTR').removeClass('voteHide');
	})
	
	
	//Bill Table
	
	$('.notBill').click(function(){
		$('.notBillTR').removeClass('billHide');
		$('.onBillTR').addClass('billHide');
	})
	
	$('.onBill').click(function(){
		$('.notBillTR').addClass('billHide');
		$('.onBillTR').removeClass('billHide');
	})
	
	$('.allBill').click(function(){
		$('.notBillTR').removeClass('billHide');
		$('.onBillTR').removeClass('billHide');
	})
	
	
	//Waypoints
	
	$('#dataNavBar').waypoint('sticky');
	$('.subNavbar').waypoint('sticky', {
	  offset: 53 // Apply "stuck" when element 30px from top
	});
	$('.memTitle').waypoint('sticky', {
	  offset: 53 // Apply "stuck" when element 30px from top
	});
	$('.compareTitleRow').waypoint('sticky', {
	  offset: -86 // Apply "stuck" when element 30px from top
	});
	
	
	
	// Compare Feature
	
	
	$('.memCompareAdder').click(function(){
		var id = $(this).attr('rel');
		var chamber = $(this).attr('data-chamber');
		addCompare(id, chamber)
		return false;
	})
	
	$(document).on({
	    mouseenter: function(){
	        $('.cover', this).stop().fadeIn();
	    },
	    mouseleave: function(){
	        $('.cover', this).stop().fadeOut();
	    }
	}, '.compareImgHolder');
	
	
	$(document).on('click', '.topRemove', function(){
		var id = $(this).attr('rel');
		var chamber = $(this).attr('data-chamber');
		$('.'+chamber+'Block').hide();
		$('.'+chamber+'Loading').show();
		$.ajax({
			type: 'get',
			url:  baseUrl+'/compare/remove',
			data: {'mem' : id, 'chamber' : chamber },
			dataType: 'json',
			success: function(member){
				$compare = member.data;
				$('.'+chamber+'Block').html($compare)
				$('.'+chamber+'Block').show();
				$('.'+chamber+'Loading').hide();
			}
		})
	})
	
	
	
	
	
	function addCompare(id, chamber){
		$('.'+chamber+'Block').hide();
		$('.'+chamber+'Loading').show();
		$.ajax({
			type: 'get',
			url:  baseUrl+'/compare/add',
			data: {'mem' : id, 'chamber' : chamber},
			dataType: 'json',
			success: function(member){
				if(member.returns == 'error'){
					alert(member.message);
				}else if(member.returns == 'success'){
					$compare = member.data;
					$('.'+chamber+'Block').html($compare)
				}
				if(member.count >= 4){$('.topAdder').hide()}
				$('.'+chamber+'Block').show();
				$('.'+chamber+'Loading').hide();
			}
		})
		
	}
	
	$('.compareBtn').click(function(){
		// if($('.houseBlock .compareImgHolder.topRemove').length < 2 || $('.senateBlock .compareImgHolder.topRemove').length < 2){
			// alert('You must have at least 2 Members added');
			// return false	
		// }else{
			var housemems = '';
			var senatemems = '';
			$.each($('.houseBlock .compareImgHolder.topRemove'), function(){
				housemems += $(this).attr('rel')+'_';
			})
			housemems = housemems.substring(0, housemems.length - 1);
			if(housemems == ''){housemems = 'null'}
			$.each($('.senateBlock .compareImgHolder.topRemove'), function(){
				senatemems += $(this).attr('rel')+'_';
			})
			senatemems = senatemems.substring(0, senatemems.length - 1);
			if(senatemems == ''){senatemems = 'null'}
			var cong = $('body').attr('rel');
			window.location = baseUrl+'/compare/?housemems='+housemems+'&senatemems='+senatemems+'&cong='+cong;
		
		
		return false;
	})
	
	$('.addToCompare').click(function(){
		$('.dropper').slideDown();
		var chamber = $(this).attr('data-chamber');
		$.ajax({
			type: 'get',
			url:  baseUrl+'/compare/addForm',
			data: {'chamber' : chamber},
			dataType: 'json',
			success: function(data){
				$('.dropInner').html(data.data);
			}
		})
		return false;
	})
	
	$(document).on('click', '.closeDrop', function(){
		$('.dropper').slideUp();
	});
	
	$('.topAdder').live('click',function(){
		// if ($('.memCompareAdder').length > 0) {
			// var id = $('.memCompareAdder').attr('rel');
			// var chamber = $('.memCompareAdder').attr('data-chamber');
			// addCompare(id, chamber)
		// }else{
			// window.location = '/members';
		// }
		$('.dropper').slideDown();
		var chamber = $(this).attr('data-chamber');
		$.ajax({
			type: 'get',
			url:  baseUrl+'/compare/addForm',
			data: {'chamber' : chamber},
			dataType: 'json',
			success: function(data){
				$('.dropInner').html(data.data);
			}
		})
		return false;
	})
	
	
	
	$('.changeSession').bind("change", function(){
		var atr = $(this).val();
		var newURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname;
		if($('.houseBlock .compareImgHolder.topRemove').length > 0|| $('.senateBlock .compareImgHolder.topRemove').length > 0){
			var r=confirm("Changing the selected session of congress will remove your compare selections. Do you want to continue?");
			if (r==true){
			  window.location = baseUrl+"welcome/setCongress/?cong="+atr+"&url="+newURL;
			}else{
			  var session = $('body').attr('rel');
			  
			}
				
		}else{
			window.location = baseUrl+"welcome/setCongress/?cong="+atr+"&url="+newURL;
		}
		$('.sessionList').val('112');
	});
	
	
	// How Scores are Calculated
	$('.howcalc').click(function(){
		$('.dropper').slideDown();
		$.ajax({
			type: 'get',
			url:  baseUrl+'/members/howScores',
			dataType: 'json',
			success: function(data){
				$('.dropInner').html(data.data);
			}
		})
		return false;
	})
	
	
	//member embed
	$('.embed').click(function(){
		$('.dropper').slideDown();
		var member = $(this).attr('data-member');
		$.ajax({
			type: 'get',
			url:  baseUrl+'/embed/embedMember',
			dataType: 'json',
			data: {'member' : member},
			success: function(data){
				$('.dropInner').html(data.data);
			}
		})
		return false;
	})
	
	
	 $('.openLog').click(function(){
    	$('.changelog').toggle();
    	return false;
    })

});



