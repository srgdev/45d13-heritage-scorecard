$(document).ready(function()
{
	
	var g1 = new JustGage({
	  id: "g1", 
	  value: getRandomInt(0, 100), 
	  min: 0,
	  max: 100,
	  title: "HOUSE REPUBLICANS",
	  gaugeColor: "#e4e2e2",
	  titleFontColor : "#f8f8f8",
	  levelColors: [
	  "#173454",
	  "#173454",
	  "#173454"
	]          
   });
	
   var g2 = new JustGage({
	  id: "g2", 
	  value: getRandomInt(0, 100), 
	  min: 0,
	  max: 100,
	  title: "HOUSE DEMOCRATS",
	  titleFontColor : "#f8f8f8",
      gaugeColor: "#e4e2e2",
	  levelColors: [
	  "#173454",
	  "#173454",
	  "#173454"
	] 
	});
	
	var g3 = new JustGage({
	  id: "g3", 
	  value: getRandomInt(0, 100), 
	  min: 0,
	  max: 100,
	  title: "SENATE REPUBLICANS",
	  titleFontColor : "#f8f8f8",
	  gaugeColor: "#e4e2e2",
	  levelColors: [
	  "#173454",
	  "#173454",
	  "#173454"
	] 
	});
	
	 var g4 = new JustGage({
	  id: "g4", 
	  value: getRandomInt(0, 100), 
	  min: 0,
	  max: 100,
	  title: "SENATE DEMOCRATS",
	  gaugeColor: "#e4e2e2",
	  titleFontColor : "#f8f8f8",
	  levelColors: [
	  "#173454",
	  "#173454",
	  "#173454"
	] 
	});
  
   
});