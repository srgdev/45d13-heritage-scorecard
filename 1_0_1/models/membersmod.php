<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class membersmod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->crud->use_table('Members');
        $this->congress = currentCongress();
    }
   
  public function allMembers(){
      $this->crud->use_table('Members');
      $query = $this->crud->retrieve(array('congressNum' => $this->congress), '', 0, 0, array('score' => 'DESC', 'lName' => 'ASC'));
      $this->template->set('results', $query);
  } 
  
  public function topPerformers(){
       $this->crud->use_table('Members');
       $houseQ  =  $this->db->query('SELECT *
                    FROM Members  
                    WHERE chamber = "house"
                    AND score NOT LIKE "NA"
                    AND congressNum = "'.currentCongress().'"
                    ORDER BY score DESC, lName ASC
                    LIMIT 5
            ');
       $house = $houseQ->result();
       $senateQ  =  $this->db->query('SELECT *
                    FROM Members  
                    WHERE chamber = "senate"
                    AND score NOT LIKE "NA"
                    AND congressNum = "'.currentCongress().'"
                    ORDER BY score DESC, lName ASC
                    LIMIT 5
            ');
       $senate = $senateQ->result();
       $this->template->set('topHouse', $house);
       $this->template->set('topSenate', $senate); 
  } 
  
  public function byState($state){
      $this->crud->use_table('Members');
      $query = $this->crud->retrieve(array('state' => $state, 'congressNum' => $this->congress), '', 0, 0, array('score' => 'DESC', 'lName' => 'ASC'));
      $this->template->set('stateResults', $query);
      $this->template->set('state', $state);
  }
  
  public function getMember($id){
      $this->crud->use_table('Members');
      $query = $this->crud->retrieve(array('congID' => $id, 'congressNum' => $this->congress), 'row', 0, 0, array('score' => 'DESC', 'lName' => 'ASC'));
      $this->template->set('member', $query);
      
      $average = $this->scoresmod->getChamberPartyAverage($query->chamber, $query->party);
      $this->template->set('average',$average );
      
      $keyvotes = $this->keyvotes->getMemberVotes($query->congID, $query->chamber);
      $this->template->set('votes', $keyvotes ); 
      
      $consponser = $this->billsmod->getMemberBills($query->congID, $query->chamber);
      $this->template->set('bills', $consponser );
  }
  
  public function searchZip($zip){
      $congress = currentCongress();
      $results = file_get_contents($this->config->item('sunlight_base').'legislators/locate?apikey='.$this->config->item('SUNLIGHT_LABS_API_KEY')."&zip=".$zip);
        $results = json_decode($results, true);
        $data = $results;
        $rows = array();
        $rows['results'] = "";
        $rows['zip'] = $zip;
        //$rows['call'] = $results;
        $states = array();
        $districts = array();
        $this->crud->use_table('Members');
        $i=0;
        foreach ($results['results'] as $r){
                
            $id = $r['bioguide_id'];
            // $rows['results'][$i]['member'] = $id;
            
            if(isset($r['district']) ){
                array_push($districts, $r['district']);
                array_push($states, $r['state']);
            }
            $i++;
        }
        
        //Loop each return for house district and state
        $i = 0;
        foreach($districts as $d) {
            $houseSearch = array(
                'state' => $states[0],
                'district' => $d,
                'congressNum' => $congress,
                'chamber' => 'house'
            ); 
            $this->crud->use_table('Members');
            $houseUsers = $this->crud->retrieve($houseSearch, '', 0, 0, array('score' => 'DESC', 'lName' => 'ASC'));
            foreach($houseUsers as $member){
               $memID = $member->congID;
               $rows['results'][$i] = $member;
               $i++;
           }

            //add the state to array
            // if(!in_array($d['district']['state'], $states)){array_push($states, $d['district']['state']);}
        }
        
       $stateT = array();
        //Loop states array and add the senate
        foreach($states as $state){
            if(!in_array($state, $stateT)){
                array_push($stateT, $state);
                $senateSearch = array(
                    'state' => $state,
                    'congressNum' => $congress,
                    'chamber' => 'senate'
                );
                $this->crud->use_table('Members');
                $senateUsers = $this->crud->retrieve($senateSearch, '', 0, 0, array('score' => 'DESC', 'lName' => 'ASC'));
               foreach($senateUsers as $member){
                   $memID = $member->congID;
                   $rows['results'][$i] = $member;
                   $i++;
               }
            }
       }
       
        
        return $rows;
  }


    public function searchName($term){
        $congress = currentCongress();
        $query  =  $this->db->query('SELECT *
                    FROM Members  
                    WHERE congressNum = "'.$congress.'"
                    AND lName LIKE "%'.$term.'%"
                    ORDER BY score DESC, lName ASC
            ');
        return $query->result();
    }
}