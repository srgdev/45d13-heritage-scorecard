<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class comparemod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->congress = currentCongress();
    }
    
  public function compareHouseVotes(){
      $this->houseMembers();
      $this->houseVotes();
  }
  
  
  public function compareHouseBills(){
      $this->houseMembers();
      $this->houseBills();
  }
  
  
  public function compareSenateVotes(){
      $this->senateMembers();
      $this->senateVotes();
  }
  
  
  public function compareSenateBills(){
      $this->senateMembers();
      $this->senateBills();
  }
  
  
  
  public function houseMembers(){
      $house = $_REQUEST['housemems'];
      
      $housememImgs = array();
      $housememNames = array();
      $housememScores = array();
      
      if($house != 'null'){
          $congress = $_REQUEST['cong'];
          $housemembers = explode("_", $house);
          
          
          
          foreach($housemembers as $m){
              $result = $this->crud->retrieve(array('congID' => $m, 'congressNum' => $congress), 'row', 0, 0, array('id' => 'DESC'));
              array_push($housememImgs, $result->image_path);
              $name = $result->title.'. '.$result->fName.' '.$result->lName;
              array_push($housememNames, $name);
              array_push($housememScores, $result->score);
          }
      }
      $this->template->set('housememImgs', $housememImgs);
      $this->template->set('housememNames', $housememNames);
      $this->template->set('housememScores', $housememScores);
  } 
  
  
  public function senateMembers(){
      $senate = $_REQUEST['senatemems'];
      
      $senatememImgs = array();
      $senatememNames = array();
      $senatememScores = array();
      
      if($senate != 'null'){
          $congress = $_REQUEST['cong'];
          $senatemembers = explode("_", $senate);
          
          
          
          foreach($senatemembers as $m){
              $result = $this->crud->retrieve(array('congID' => $m, 'congressNum' => $congress), 'row', 0, 0, array('id' => 'DESC'));
              array_push($senatememImgs, $result->image_path);
              $name = $result->title.'. '.$result->fName.' '.$result->lName;
              array_push($senatememNames, $name);
              array_push($senatememScores, $result->score);
          }
      }
      
      $this->template->set('senatememImgs', $senatememImgs);
      $this->template->set('senatememNames', $senatememNames);
      $this->template->set('senatememScores', $senatememScores);
  } 
  
  
  public function houseVotes(){
      $house = $_REQUEST['housemems'];
      $congress = $_REQUEST['cong'];
      $housemembers = explode("_", $house);
      
      $housevotes = array();
      $i = 0;
      $this->crud->use_table('voteTable');
      $result = $this->crud->retrieve(array('session' => $congress, 'chamber' => 'house'), '', 0, 0, array('id' => 'DESC'));
      foreach($result as $v){
          $housevotes[$i]['title'] = $v->question;
          $housevotes[$i]['position'] = $v->perferred_vote;
          $housevotes[$i]['roll_id'] = $v->roll_id;
          $j = 0;
          foreach($housemembers as $m){
             $this->crud->use_table('votes');
             $result = $this->crud->retrieve(array('vote_id' => $v->roll_id, 'member' => $m), '', 0, 0, array('id' => 'DESC'));
             foreach($result as $mv){
                 $housevotes[$i]['votes'][$j] = $mv->vote;
                 $j++;
             }  
          }
          $i++;
      }

      $this->template->set('housevotes', $housevotes);
  }
  
  
   public function senateVotes(){
      $senate = $_REQUEST['senatemems'];
      $congress = $_REQUEST['cong'];
      $senatemembers = explode("_", $senate);
      
      $senatevotes = array();
      $i = 0;
      $this->crud->use_table('voteTable');
      $result = $this->crud->retrieve(array('session' => $congress, 'chamber' => 'senate'), '', 0, 0, array('id' => 'DESC'));
      foreach($result as $v){
          $senatevotes[$i]['title'] = $v->question;
          $senatevotes[$i]['position'] = $v->perferred_vote;
          $senatevotes[$i]['roll_id'] = $v->roll_id;
          $j = 0;
          foreach($senatemembers as $m){
             $this->crud->use_table('votes');
             $result = $this->crud->retrieve(array('vote_id' => $v->roll_id, 'member' => $m), '', 0, 0, array('id' => 'DESC'));
             foreach($result as $mv){
                 $senatevotes[$i]['votes'][$j] = $mv->vote;
                 $j++;
             }  
          }
          $i++;
      }

      $this->template->set('senatevotes', $senatevotes);
  }
   
  
  public function houseBills(){
     $house = $_REQUEST['housemems'];
     $congress = $_REQUEST['cong'];
     $housemembers = explode("_", $house);
      
     $houseBills = array();
     $i = 0;
     $query = $this->db->query('SELECT *
            FROM bills  
            WHERE (bills.billType = "hr"
            OR bills.billType = "hltr")
            AND bills.session = "'.$congress.'" 
          ');
     $results = $query->result();
     foreach($results as $r){
          $houseBills[$i]['title'] = $r->short_title;
          $houseBills[$i]['position'] = $r->sponsor_position;
          $houseBills[$i]['bill_id'] = $r->bill_id;
          $this->crud->use_table('sponsors');
          // $houseBills[$i]['bill'] = $r;
          $j = 0;
          if($house != 'null'){
              foreach($housemembers as $mem){
                  $mem = $this->crud->retrieve(array('bill_id' => $r->bill_id, 'member' => $mem), 'row', 0, 0, array('id' => 'DESC'));
                  $pos = $r->sponsor_position;
                  $mempos = 'no';
                  if($pos == 'Yes' && count($mem) > 0){ $mempos = 'Yes'; $right++;}
                  if($pos == 'No' && count($mem) == 0){ $mempos = 'Yes'; $right++; }
                  $houseBills[$i]['mempos'][$j] = $mempos;
                  $j++;
              }
          }
          $i++;
      }
     
     $this->template->set('housebills', $houseBills);
  } 
  
  
  
  public function senateBills(){
     $senate = $_REQUEST['senatemems'];
     $senateBills = array();
     
    
     $congress = $_REQUEST['cong'];
     $senatemembers = explode("_", $senate);
      
     $senateBills = array();
     $i = 0;
     $query = $this->db->query('SELECT *
            FROM bills  
            WHERE (bills.billType = "s"
            OR bills.billType = "sltr")
            AND bills.session = "'.$congress.'" 
          ');
     $results = $query->result();
     foreach($results as $r){
          $senateBills[$i]['title'] = $r->short_title;
          $senateBills[$i]['position'] = $r->sponsor_position;
          $senateBills[$i]['bill_id'] = $r->bill_id;
          $this->crud->use_table('sponsors');
          // $houseBills[$i]['bill'] = $r;
          $j = 0;
          if($senate != 'null'){
              foreach($senatemembers as $mem){
                  $mem = $this->crud->retrieve(array('bill_id' => $r->bill_id, 'member' => $mem), 'row', 0, 0, array('id' => 'DESC'));
                  $pos = $r->sponsor_position;
                  $mempos = 'no';
                  if($pos == 'Yes' && count($mem) > 0){ $mempos = 'Yes'; $right++;}
                  if($pos == 'No' && count($mem) == 0){ $mempos = 'Yes'; $right++; }
                  $senateBills[$i]['mempos'][$j] = $mempos;
                  $j++;
              }
          }
          $i++;
      }
     
     $this->template->set('senatebills', $senateBills);
  } 


  
  public function returnUri(){
      $get = array();
      foreach($_GET as $key => $val){
          $get[] = $key.'='.$val;
      }
      return '?'.implode('&',$get);
  }
  
  public function returnFromUri(){
      $house = get_data('comparehouse' );
      $housemems = '';
      if(count($house) > 0){
          foreach($house as $h){
              $housemems .= $h."_";
          }
          $housemems = trim($housemems, "_");
      }else{
          $housemems = 'null';
      }
      
        
      $senate = get_data('comparesenate' );
      $senatemems = '';
      if(count($senate) > 0){
          foreach($senate as $s){
              $senatemems .= $s."_";
          }
          $senatemems = trim($senatemems, "_");
      }else{
          $senatemems = 'null';
      }
        
      return '?cong='.currentCongress().'&housemems='.$housemems.'&senatemems='.$senatemems;
  }
  
  
}