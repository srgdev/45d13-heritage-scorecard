<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class siteauth extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
    
    public function checkAuth(){
        // return true;
        if(get_data('watch_id') && get_data('loggedin_watch')){
            return true;
        }else{
            return false;
        }
    }
    
    public function createAccount($email, $password){
         $this->crud->use_table('users');
         if($this->checkemail($email)){
             $this->crud->create(array('email' => $email, 'password' => $password));
             $this->login($email, $password);
             return true;
         }else{
             return false;
         }
    }
    
    public function login($email, $password){
        $this->crud->use_table('users');
        $check =  $this->crud->retrieve(array('email' => $email, 'password' => $password), 'row', 0, 0, array('id' => 'DESC'));
        if(count($check) > 0){
            $this->session->set_userdata('loggedin_watch' , 'true');
            $this->session->set_userdata('watch_id' , $check->email);
            return true;
        }else{
            return false;
        }
    }
    
    public function logout(){
        $this->session->unset_userdata('loggedin_watch');
        $this->session->unset_userdata('watch_id');
        return true;
    }
    
    public function checkemail($email){
        $this->crud->use_table('users');
        $check =  $this->crud->retrieve(array('email' => $email), '', 0, 0, array('id' => 'DESC'));
        if(count($check) > 0){
            return false;
        }else{
            return true;
        }
    } 

    public function getWatchId(){
        return get_data('watch_id');
    }
    
}