<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class watchlistmod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
  
  
  public function getMembers(){
      $this->crud->use_table('favs');
      $user = $this->siteauth->getWatchId();
      $mems = $this->crud->retrieve(array('user' => $user), '', 0, 0, array('id' => 'DESC'));
      $members = array();
      $i = 0;
      foreach($mems as $m){
          $this->crud->use_table('Members');
          $member = $this->crud->retrieve(array('congID' => $m->member, 'congressNum' => currentCongress()), 'row', 0, 0, array('id' => 'DESC'));
          $members[$i]['member'] = $member;
          $keyvotes = $this->keyvotes->getMemberVotes($member->congID, $member->chamber, 2);
          $members[$i]['votes'] = $keyvotes;
          $consponser = $this->billsmod->getMemberBills($member->congID, $member->chamber, 2);
          $members[$i]['bills'] = $consponser;
          $this->template->set('members', $members); 
          $i ++;
      }
      return $members;
  }
  
  
  public function add($member){
      $this->crud->use_table('favs');
      $user = $this->siteauth->getWatchId();
      if($this->checkWatchList($user, $member)){
          $this->crud->create(array('user' => $user, 'member' => $member));
          return true;
      }else{
          return false;
      }
  }
  
  public function remove($member){
      $this->crud->use_table('favs');
      $user = $this->siteauth->getWatchId();
      $this->crud->delete(array('user' => $user, 'member' => $member), 0, 0, array('id' => 'DESC'));
      return true;
  }
  
  
  public function checkWatchList($user, $member){
      $this->crud->use_table('favs');
      $check =  $this->crud->retrieve(array('user' => $user, 'member' => $member), '', 0, 0, array('id' => 'DESC'));
      if(count($check) > 0) {
          return false;
      }else{
          return true;
      }
  }
  
}