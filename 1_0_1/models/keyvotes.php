<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class keyvotes extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->congress = currentCongress();
    }
  
  public function all(){
      $this->crud->use_table('voteTable');
      $query = $this->crud->retrieve(array('session' => $this->congress), '', 0, 0, array('id' => 'ASC'));
      $this->template->set('results', $query);    
  }
  
  
  public function getMemberVotes($member, $chamber, $limit = ''){
      $this->crud->use_table('voteTable');
      $order = '';
      if($limit){$limit = 'LIMIT '.$limit; $order = 'ORDER BY voteTable.id DESC';}
      $query  =  $this->db->query('SELECT *
            FROM voteTable, votes  
            WHERE voteTable.chamber = "'.$chamber.'"
            AND voteTable.session = "'.$this->congress.'"
            AND votes.vote_id = voteTable.roll_id
            AND votes.member = "'.$member.'"
            '.$order.'
            '.$limit.'            
       ');
       $results = $query->result();
       
       //This is to remove not voting and present from the total
       $query1  =  $this->db->query('SELECT *
            FROM voteTable, votes  
            WHERE voteTable.chamber = "'.$chamber.'"
            AND voteTable.session = "'.$this->congress.'"
            AND votes.vote_id = voteTable.roll_id
            AND votes.member = "'.$member.'"
            AND votes.vote != "Not Voting"
            AND votes.vote != "Present"
            '.$order.'
            '.$limit.'            
       ');
       
       $results1 = $query1->result();
       
       
       $total  = count($query1->result());
       $right = 0;
       
       foreach($results as $r){
           if($r->perferred_vote == $r->vote){
               $right++;
           }
       }
       
       $this->template->set('votesRight', $right );
       $this->template->set('votesTotal', $total );
       return $results;
  }
   
  public function getVote($vote){
      $this->crud->use_table('voteTable');
      $voteResult =  $this->crud->retrieve(array('roll_id' => $vote), 'row', 0, 0, array('id' => 'DESC'));
      $this->template->set('vote', $voteResult );
      
      $this->crud->use_table('votes');
      
      $yesVotes =  $this->crud->retrieve(array('vote_id' => $vote, 'vote' => 'Yes'), '', 0, 0, array('id' => 'DESC'));
      
      $noVotes =  $this->crud->retrieve(array('vote_id' => $vote, 'vote' => 'No'), '', 0, 0, array('id' => 'DESC'));
      
      $query  =  $this->db->query('SELECT *
            FROM Members, votes  
            WHERE votes.vote_id = "'.$vote.'"
            AND votes.member = Members.congID
            AND Members.congressNum = "'.currentCongress().'"
            ORDER BY Members.score DESC, Members.lName ASC
       ');
      
      $votes = $query->result();
      $this->template->set('votes', $votes );
       
      $yes = count($yesVotes);
      $no = count($noVotes);
      $this->template->set('yesvotes', $yes );
      $this->template->set('novotes', $no );
     
  } 
}