<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class scoresmod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->crud->use_table('Members');
        $this->congress = currentCongress();
    }
  
  public function getChamberAverage($chamber){
        $scores =  $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => $this->congress), '', 0, 0, array('id' => 'DESC'));
        return runAverage($scores);
  }
  
  public function getChamberPartyAverage($chamber, $party){
        $scores =  $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => $this->congress, 'party' => $party), '', 0, 0, array('id' => 'DESC'));
        return $this->runAverage($scores);
  }
  
  public function runAverage($scores){
        $total = count($scores);
        $score = 0;
        foreach($scores as $s){
            $score = $score + $s->score;
        }
        return round($score / $total);
  }
  
   
}