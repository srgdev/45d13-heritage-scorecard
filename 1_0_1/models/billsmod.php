<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class billsmod extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->congress = currentCongress();
    }
  
  public function all(){
      $this->crud->use_table('bills');
      $query = $this->crud->retrieve(array('session' => $this->congress), '', 0, 0, array('id' => 'ASC'));
      $this->template->set('results', $query);
  }
   
  public function getMemberBills($member, $chamber, $limit = ''){
      $order = '';
      if($limit){$limit = 'LIMIT '.$limit; $order = 'ORDER BY bills.id DESC';}
      $bills = array();
      if($chamber == 'house'){
          $query  =  $this->db->query('SELECT *
            FROM bills  
            WHERE (bills.billType = "hr"
            OR bills.billType = "hltr")
            AND bills.session = "'.$this->congress.'"
            '.$order.' 
            '.$limit.' 
          ');
      }else{
         $query  =  $this->db->query('SELECT *
            FROM bills  
            WHERE (bills.billType = "s"
            OR bills.billType = "sltr")
            AND bills.session = "'.$this->congress.'"
            '.$order.' 
            '.$limit.'  
          '); 
      }
      $results = $query->result();
      $total = count($results);
      $right = 0;
      $i = 1;
      foreach($results as $r){
          $this->crud->use_table('sponsors');
          $mem = $this->crud->retrieve(array('bill_id' => $r->bill_id, 'member' => $member), 'row', 0, 0, array('id' => 'DESC'));
          $pos = $r->sponsor_position;
          $mempos = 'no';
          $bills[$i]['bill'] = $r;
          if($pos == 'Yes' && count($mem) > 0){ $mempos = 'Yes'; $right++;}
          if($pos == 'No' && count($mem) == 0){ $mempos = 'Yes'; $right++; }
          $bills[$i]['mempos'] = $mempos;
          $i++;
      }
      $this->template->set('billsTotal', $total );
      $this->template->set('billsRight', $right );
      return $bills;
  }

  public function getBill($bill){
      $this->crud->use_table('bills');
      $bills = $this->crud->retrieve(array('bill_id' => $bill), 'row', 0, 0, array('id' => 'DESC'));
      $this->template->set('bill', $bills );
      
      $this->crud->use_table('sponsors');
      $sponsors = $this->crud->retrieve(array('bill_id' => $bill), '', 0, 0, array('id' => 'DESC'));
      $total = count($sponsors);
      $this->template->set('sponsorTotal', $total );
      
      $chamber = getBillChamber($bills->billType);
      $this->crud->use_table('Members');
      $chamberMembers = $this->crud->retrieve(array('chamber' => $chamber, 'congressNum' => currentCongress()), '', 0, 0, array('id' => 'DESC'));
      $members = array();
      $i = 0;
      foreach($chamberMembers as $m){
          $members[$i]['member'] = $m;
          $this->crud->use_table('sponsors');
          $billCheck = $this->crud->retrieve(array('bill_id' => $bill, 'member' => $m->congID), 'row', 0, 0, array('id' => 'DESC'));
          if(count($billCheck) > 0){
              $members[$i]['billcheck'] = 'yes';
          }else{
              $members[$i]['billcheck'] = 'no';
          }
          $i++;
      }
      
      $this->template->set('members', $members );
      
  }
  
}